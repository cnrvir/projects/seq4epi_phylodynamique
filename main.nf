//We are using DSL2
nextflow.enable.dsl=2

//Import the different workflows
include { GENERAL_VIRUS } from './workflows/general-virus.nf'
include { SARS_COV_2 } from './workflows/sars-cov-2.nf'
include { RSV } from './workflows/rsv.nf'
include { FLU } from './workflows/flu.nf'

//Main
workflow {

	if(params.type.equals("general-virus")){
		
	GENERAL_VIRUS()
	
	}

	if(params.type.equals("sars-cov-2")){
		
	SARS_COV_2()
	
	}
	
	if(params.type.equals("rsv")){
		
	RSV()
	
	}
	
	if(params.type.equals("flu")){
		
	FLU()
	
	}
	
}
