
//Prepare CovFlow analysis 

process GetNumberOfMonths {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path (datefile)
    val (tmrca)

    output:
    path ("nb_of_dimensions.txt")

    script:
    """
        GetNumberOfMonths.py --tmrca $tmrca --datefile $datefile | tr -d '\\n' > nb_of_dimensions.txt

   """

}

process CovFlow {

    cpus 10 
    memory '20 GB'

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    conda "covflow.yaml"

    input:
    path (treefile)
    path (dates)
    path (maxheight)
    path (dimensions)

    output:
    path ("*bdsky.xml")
    path ("*coalsky.xml")

    script:
    """

        cp -r ${baseDir}/assets/* ./
        snakemake --use-conda --config treefile=$treefile dates=$dates maxheight=$maxheight dimensionfile=$dimensions --cores 10
   
    """

}

//Run BEAST

process RunBeastBDSky {
    publishDir "${params.results}/02a_BDSKY",  mode: 'copy'

    label 'beast262bdsky'

    input:
    path (xml)

    output:
    path ("*.log")

    script:
    """

        beast -threads ${task.cpus} $xml

    """
}

process RunBeastCoalSky {
    publishDir "${params.results}/02b_CoalSky",  mode: 'copy'

    label 'beast262bdsky'

    input:
    path (xml)

    output:
    path ("*.log")

    script:
    """

        beast -threads ${task.cpus} $xml

    """
}

//Create BEAST graphs

process GetGraphsBDsky {

    publishDir "${params.results}/03_Beast_graphs",  mode: 'copy'
    
    label 'rbdskytools'

    input:
    path (log_file)
    path (date_file)

    output:
    path ("graphs*")

    script:
    """
    
        cp -r ${baseDir}/bin/plot_bdsky_log.R ./
        Rscript plot_bdsky_log.R --flog $log_file --fdates $date_file --outputprefix graphs

    """

}