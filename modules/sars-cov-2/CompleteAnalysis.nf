process PhylodeepAnalysis {

    publishDir "${params.results}/02_Phylodeep",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path (treefile)
    tuple val(windowname), path (sampling_proportion)

    output:
    path ("*phylodeep_analysis*")
    path ("best_phylodeep_model_results.tsv")
    path ("BD_phylodeep_model_results.tsv")
    path ("BDEI_phylodeep_model_results.tsv")

    script:
    """

        PhylodeepAnalysis.py --treefile $treefile --sampling_proba $sampling_proportion --output_dir ./ --output_prefix phylodeep_analysis

        best_model=\$(awk 'NR==1{for(i=1;i<=NF;i++) h[i]=\$i} NR==2{max=\$1; col=h[1]; for(i=2;i<=NF;i++) if(\$i>max){max=\$i; col=h[i]}} END{print col}' phylodeep_analysis_model_selection.tsv)
        best_model=\${best_model#Probability_}
        
        param_file="phylodeep_analysis_\${best_model}_FULL_parameter_estimation.tsv"
        output_file="best_phylodeep_model_results.tsv"
        > "\$output_file"
        awk 'NR==1 {for(i=1; i<=NF; i++) header[i] = \$i} 
        NR==2 {for(i=1; i<=NF; i++) print header[i] "\\t" \$i}' "\$param_file" > "\$output_file"
        awk 'NR==1 {for(i=1; i<=NF; i++) header[i] = \$i}
        NR==2 {for(i=1; i<=NF; i++) print header[i] "\\t" \$i}' phylodeep_analysis_BD_FULL_parameter_estimation.tsv > BD_phylodeep_model_results.tsv
        awk 'NR==1 {for(i=1; i<=NF; i++) header[i] = \$i}
        NR==2 {for(i=1; i<=NF; i++) print header[i] "\\t" \$i}' phylodeep_analysis_BDEI_FULL_parameter_estimation.tsv > BDEI_phylodeep_model_results.tsv   
    """

}

//Perform ML analyses 

process PerformBDMLAnalysis {

    publishDir "${params.results}/02a_BDML_estimation/${type}",  mode: 'copy'
    
    label 'bdevo'

    input:
    tuple val(window), path(treefile), path(sampling_proportion)
    path (bd_priors)

    output:
    tuple val(window), path ("BD_analysis_${window}.tsv")


    script:
    """
        R_naught=\$(grep "R_naught" $bd_priors | cut -f2 | tr -d '\\n') 
        infectious_period=\$(grep "Infectious_period" $bd_priors | cut -f2 | tr -d '\\n') 

        bd_infer --nwk $treefile --ci --psi \$infectious_period --log BD_analysis_${window}.tsv

    """
//         bd_infer --nwk $treefile --ci --p \$(cat $sampling_proportion) --lower_bounds 0 0.15 0 --upper_bounds 1 0.25 1 --log BD_analysis_${window}.tsv

}

process PerformBDEIMLAnalysis {

    publishDir "${params.results}/02b_BDEIML_estimation/${type}",  mode: 'copy'
    
    label 'bdeievo'

    input:
    tuple val(window), path(treefile), path(sampling_proportion)
    path (bdei_priors)

    output:
    tuple val(window), path ("BDEI_analysis_${window}.tsv")

    script:
    """
        R_naught=\$(grep "R_naught" $bdei_priors | cut -f2 | tr -d '\\n') 
        infectious_period=\$(grep "Infectious_period" $bdei_priors | cut -f2 | tr -d '\\n') 
        incubation_period=\$(grep "Incubation_period" $bdei_priors | cut -f2 | tr -d '\\n') 


        bdei_infer -t ${task.cpus} --nwk $treefile --psi \$infectious_period --mu \$incubation_period --u_policy max --CI_repetitions 500 --upper_bounds 1 1 1 1  --log BDEI_analysis_${window}.tsv
    """
//        bdei_infer --nwk $treefile --p \$(cat $sampling_proportion) --u_policy max --CI_repetitions 1000 --upper_bounds 1 1 1 1 --log BDEI_analysis_${window}.tsv

}

//Perform BEAST analysis 


process CovFlow {

    cpus 10 
    memory '20 GB'

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    conda "covflow.yaml"

    input:
    path (treefile)
    path (dates)
    path (maxheight)
    path (dimensions)
    path (beast_priors)

    output:
    path ("*bdsky.xml")
    path ("*coalsky.xml")

    script:
    """
        R_naught=\$(grep "R_naught" $beast_priors | cut -f2 | tr -d '\\n') 
        infectious_period=\$(grep "Infectious_period" $beast_priors | cut -f2 | tr -d '\\n') 
        incubation_period=\$(grep "Incubation_period" $beast_priors | cut -f2 | tr -d '\\n') 

        cp -r ${baseDir}/assets/* ./
        snakemake --use-conda --config treefile=$treefile dates=$dates maxheight=$maxheight dimensionfile=$dimensions --cores 10
   
    """

}


