process PhylodeepAnalysis {

    publishDir "${params.results}/02_Phylodeep",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path (treefile)
    tuple val(windowname), path (sampling_proportion)

    output:
    path ("phylodeep_analysis*")

    script:
    """

        PhylodeepAnalysis.py --treefile $treefile --sampling_proba $sampling_proportion --output_dir ./ --output_prefix phylodeep_analysis
   
    """

}