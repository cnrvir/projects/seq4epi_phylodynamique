// #### Phylodeep analysis ####

process PhylodeepAnalysis {

    publishDir "${params.results}/02_Phylodeep",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path (treefile)
    tuple val(windowname), path (sampling_proportion)

    output:
    path ("phylodeep_analysis*")

    script:
    """

        PhylodeepAnalysis.py --treefile $treefile --sampling_proba $sampling_proportion --output_dir ./ --output_prefix phylodeep_analysis

    """

}

// #### ML Analysis ####

process GenerateTimeWindows {
    
    label 'pythonevol'

    input:
    path (min_max_date)
    val window_size
    val window_step

    output:
    path "time_window_*"

    script:
    """
        MINDATE=\$(cut -f 1 $min_max_date)
        MAXDATE=\$(cut -f 2 $min_max_date)

        GenerateTimeWindows.py -b \$MINDATE -e \$MAXDATE -m ${window_size} -s ${window_step} > tmp
        split -l 1 tmp time_window_
        rm tmp
    """
}


//Perform analyses 

process PerformBDMLAnalysis {

    publishDir "${params.results}/02a_BDML_estimation",  mode: 'copy'
    
    label 'bdevo'

    input:
    tuple val(window), path(treefile), path(sampling_proportion)


    output:
    tuple val(window), path("BD_analysis_${window}.tsv")


    script:
    """
        
        bd_infer --nwk $treefile --ci --psi 0.142  --lower_bounds 0 0 0 --upper_bounds 1 1 1 --log BD_analysis_${window}.tsv

    """
//         bd_infer --nwk $treefile --ci --p \$(cat $sampling_proportion) --lower_bounds 0 0.15 0 --upper_bounds 1 0.25 1 --log BD_analysis_${window}.tsv

}

process PerformBDEIMLAnalysis {

    publishDir "${params.results}/02b_BDEIML_estimation",  mode: 'copy'
    
    label 'bdeievo'

    input:
    tuple val(window), path(treefile), path(sampling_proportion)

    output:
    tuple val(window), path ("BDEI_analysis_${window}.tsv")

    script:
    """

        bdei_infer -t ${task.cpus} --nwk $treefile --psi 0.284 --mu 0.284 --u_policy max --CI_repetitions 500 --upper_bounds 1 1 1 1  --log BDEI_analysis_${window}.tsv
    
    """
//        bdei_infer --nwk $treefile --p \$(cat $sampling_proportion) --u_policy max --CI_repetitions 1000 --upper_bounds 1 1 1 1 --log BDEI_analysis_${window}.tsv

}

//Reformat data 

process ReformatBDMLOutput {

    publishDir "${params.results}/02a_BDML_estimation",  mode: 'copy'

    label 'pythonevol'

    input:
    tuple val(windowname), path(bdmlfile), path(windowfile)

    output:
    path "bdml_output_${windowname}.txt"

    script:
    """

    ReformatBDMLResults.py --bdmlfile $bdmlfile --datefile $windowfile --output_file bdml_output_${windowname}.txt

    """
}

process ReformatBDEIMLOutput {

    publishDir "${params.results}/02b_BDEIML_estimation",  mode: 'copy'

    label 'pythonevol'

    input:
    tuple val(windowname), path(bdeimlfile), path(windowfile)

    output:
    path "bdeiml_output_${windowname}.txt"

    script:
    """

    ReformatBDEIMLResults.py --bdeimlfile $bdeimlfile --datefile $windowfile --output_file bdeiml_output_${windowname}.txt

    """
}

//Create graphs from BDML and BDEIML analyses

process GetGraphsBDML {

    publishDir "${params.results}/03a_BDML_graphs",  mode: 'copy'
    
    label 'rbdskytools'

    input:
    path(bdmlfile)

    output:
    path ("*_plot.pdf")

    script:
    """
    
        cp -r ${baseDir}/bin/GetGraphsBDML.R ./
        Rscript GetGraphsBDML.R $bdmlfile

    """

}

process GetGraphsBDEIML {

    publishDir "${params.results}/03b_BDML_graphs",  mode: 'copy'
    
    label 'rbdskytools'

    input:
    path(bdeimlfile)

    output:
    path ("*_plot.pdf")

    script:
    """
    
        cp -r ${baseDir}/bin/GetGraphsBDEIML.R ./
        Rscript GetGraphsBDEIML.R $bdeimlfile

    """

}
