//Extract and Filter

process PrepareDatasetDateAndCountry {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path (fasta)
    path (metadata)
    val (country)
    val (sample_min_date)
    val (sample_max_date)

    output:
    path ("sequences_filtered.fasta")
    path ("metadata_filtered.tsv")

    script:
    """

        FilterDatasetByDateAndCountry.py -f $fasta -m $metadata -i $sample_min_date -d $sample_max_date -c $country -t ${task.cpus} -o metadata_filtered.tsv -u sequences_filtered.fasta

    """

}

process FilterNext {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'nextstrainbase'

    input:
    path (metadata)
    val (country)
    val (sample_min_date)
    val (sample_max_date)
    val (nb_max)

    output:
    path ("${metadata.baseName}_filternext.tsv")

    script:
    """

        augur filter --metadata $metadata --exclude-where 'country!=$country' --min-date $sample_min_date --max-date $sample_max_date --group-by division year month --subsample-max-sequences $nb_max --output-metadata ${metadata.baseName}_filternext.tsv

   """

}

process FilterRandom {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'nextstrainbase'

    input:
    path (metadata)
    val (nb_max)

    output:
    path ("${metadata.baseName}_filternext.tsv")

    script:
    """

        head -n 1 $metadata > ${metadata.baseName}_filternext.tsv
        tail -n+2 $metadata | cut -f1 | shuf -n $nb_max > tmp.txt
        grep -f tmp.txt $metadata >> ${metadata.baseName}_filternext.tsv

   """

}

// Add other datasets 

process MergeRefQuerydataset {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'

    label 'pythonevol'

    input:
    path (fasta)
    path (fasta_ref)
    path (metadata)
    path (metadata_ref)

    output:
    path ("${fasta.baseName}_w_ref.fasta")
    path ("${metadata.baseName}_w_ref.tsv")

    script:
    """

		cat $fasta_ref > ${fasta.baseName}_w_ref.fasta
		echo -e '' >> ${fasta.baseName}_w_ref.fasta
		cat $fasta >> ${fasta.baseName}_w_ref.fasta
        sed -i '/^\$/d' ${fasta.baseName}_w_ref.fasta

		cat $metadata_ref > ${metadata.baseName}_w_ref.tsv
		echo -e '' >> ${metadata.baseName}_w_ref.tsv
		tail -n+2 $metadata >> ${metadata.baseName}_w_ref.tsv
        sed -i '/^\$/d' ${metadata.baseName}_w_ref.tsv
        
    """

}

process AddWuhan {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path (fasta)
    path (fasta_wuhan)
    path (metadata)
    path (metadata_wuhan)

    output:
    path ("${fasta.baseName}_w_ref.fasta")
    path ("${metadata.baseName}_w_ref.tsv")


    script:
    """
		cat $fasta_wuhan > ${fasta.baseName}_w_ref.fasta
		echo -e '' >> ${fasta.baseName}_w_ref.fasta
		cat $fasta >> ${fasta.baseName}_w_ref.fasta
        sed -i '/^\$/d' ${fasta.baseName}_w_ref.fasta

		   
		cat $metadata_wuhan > ${metadata.baseName}_w_ref.tsv
		echo -e '' >> ${metadata.baseName}_w_ref.tsv
		tail -n+2 $metadata >> ${metadata.baseName}_w_ref.tsv
        sed -i '/^\$/d' ${metadata.baseName}_w_ref.tsv

   """

}

//Manipulate newly created dataset

process RemoveDuplicates {  

    label 'seqkit'

    input:
    path (fasta)
    path (metadata)

    output:
    path ("${fasta.baseName}_rmdup.fasta")
    path ("${metadata.baseName}_rmdup.tsv")

    script:
    """

        awk -F"\\t" '!_[\$1]++' $metadata > ${metadata.baseName}_rmdup.tsv
        seqkit rmdup -n $fasta > ${fasta.baseName}_rmdup.fasta

    """

}

process GrepSequences {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'seqkit'

    input:
    path (fasta)
    path (metadata)

    output:
    path ("${fasta.baseName}_filternext.fasta")

    script:
    """

        cut -f1 $metadata > names.txt
        seqkit grep -f names.txt $fasta > ${fasta.baseName}_filternext.fasta

   """

}

process RemoveForbiddenCharSeq {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path (sequences)

    output:
    path ("${sequences.baseName}_nfc.fasta")

    script:
    """

        cat $sequences | sed "s/([^()]*)//g" | sed "s/'//g" | sed "s/?/-/g" | sed "s/  / /g" > ${sequences.baseName}_nfc.fasta    
    
    """

}


process RemoveForbiddenCharMetadata {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path (metadata)

    output:
    path ("${metadata.baseName}_nfc.fasta")

    script:
    """
    
        cat $metadata | sed "s/([^()]*)//g" | sed "s/'//g" | sed "s/?/-/g" | sed "s/  / /g" > ${metadata.baseName}_nfc.fasta    
    
    """

}

process GetDates {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path (metadata)

    output:
    path ("${metadata.baseName}_dates.tsv")

    script:
    """

        cut -f1,6 $metadata > ${metadata.baseName}_dates.tsv

   """

}

process FillMissingInfosDates {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path(datefile)

    output:
    path("${datefile.baseName}_corrected.tsv")


    script:
    """

        FillMissingInfosDates.py --date_file $datefile --output_file ${datefile.baseName}_corrected.tsv

   """

}


// Align and prepare sequences 

process NextAlign {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'nextstrainbase'

    input:
    path (fasta)
    path (fastaref)

    output:
    path ("${fasta.baseName}_aligned.fasta")

    script:
    """
        nextalign run --jobs ${task.cpus} --input-ref $fastaref --output-fasta ${fasta.baseName}_aligned.fasta $fasta

    """

}

process MaskAlignment {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'

    label 'goalign'

    input:
    path (aln)
    path (mask_sites_file)
    val (mask_begin)
    val (mask_end)

    output:
    path ("${aln.baseName}_masked.fasta")

    script:
    """
	goalign mask -s 0 -l $mask_begin -i $aln \
    	    | goalign mask -s 29703 -l $mask_end \
    	    | goalign mask --pos \$(cat $mask_sites_file) \
	    > ${aln.baseName}_masked.fasta
        
    """
}

