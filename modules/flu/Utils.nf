//Time related processes

process GetDecimalYear {
    
    label 'pythonevol'

    input:
    val (input_date)

    output:
    stdout

    script:
    """
        ConverDateToDecimalYear.py -d $input_date | tr -d '\\n'

    """

}

process ConvertTimeWindowToFraction {
 
    label 'pythonevol'

    input:
    val mindate
    val maxdate

    output:
    path "timewindow_${mindate}_${maxdate}.txt"

    script:

    """
        TimeWindowToFraction.py --min_date $mindate --max_date $maxdate > timewindow_${mindate}_${maxdate}.txt
    """
}


//Sampling Proportion estimation

// process EstimateSamplingProportion { //Under quarantine till we find a way to get a nb_cases_file

//     publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
//     label 'pythonevol'

//     input:
//     tuple val(type), val(windowname), path(label_file), path (timewindow)
//     path (cases_file)
//     val (expected_sampling_proportion)

//     output:
//     tuple val(type), val(timewindow.baseName), path("sampling_proportion_${timewindow.baseName}.txt")

//     script:

//     """
//         nb_labels=\$(cat $label_file | sed '/^\$/d' | wc -l | tr -d '\\n')

//         MINDATE=\$(cut -f 1 $timewindow)
//         MAXDATE=\$(cut -f 2 $timewindow)

//         EstimateSamplingProportion.py --nb_tree \$nb_labels --cases_file $cases_file --expected_p $expected_sampling_proportion --min_date \$MINDATE --max_date \$MAXDATE --output_p sampling_proportion_${timewindow.baseName}.txt
//     """

// }

process EstimateSamplingProportion {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'pythonevol'

    input:
    tuple val(type), val(windowname), path(label_file), path (timewindow)
    val (expected_sampling_proportion)

    output:
    tuple val(type), val(timewindow.baseName), path("sampling_proportion_${timewindow.baseName}.txt")

    script:

    """
        echo -e $expected_sampling_proportion > sampling_proportion_${timewindow.baseName}.txt
    """

}

// process EstimateSamplingProportionPhylodeep { //Under quarantine till we find a way to get a nb_cases_file

//     publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
//     label 'pythonevol'

//     input:
//     path (timewindow)
//     tuple val(type), path (treefile)
//     path (cases_file)
//     val (expected_sampling_proportion)

//     output:
//     tuple val(type), val(timewindow.baseName), path("sampling_proportion.txt")

//     script:

//     """
        
//         mkdir results 

//         nb_labels=\$(cat $label_file | sed '/^\$/d' | wc -l | tr -d '\\n')
        
//         MINDATE=\$(cut -f 1 $timewindow)
//         MAXDATE=\$(cut -f 2 $timewindow)

//         EstimateSamplingProportion.py --nb_tree \$nb_labels --cases_file $cases_file --expected_p $expected_sampling_proportion --min_date \$MINDATE --max_date \$MAXDATE --output_p sampling_proportion.txt

//     """

// }


process EstimateSamplingProportionPhylodeep {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path (timewindow)
    tuple val(type), path (label_file)
    val (expected_sampling_proportion)

    output:
    tuple val(type), val(timewindow.baseName), path("sampling_proportion.txt")

    script:

    """

        echo -e $expected_sampling_proportion > sampling_proportion.txt

    """

}

process GetMinMaxDate {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'pythonevol'

    input:
    tuple val(type), path (datefile)

    output:
    tuple val(type), path ("min_and_max_dates.txt")

    script:

    """
        
         GetMinAndMaxDates.py --input_file $datefile --output_file min_and_max_dates.txt


    """

}

