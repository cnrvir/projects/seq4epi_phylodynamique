//Prepare timeframes 

process GenerateTimeWindows {
    
    label 'pythonevol'

    input:
    tuple val (type), path (min_max_date)
    val window_size
    val window_step

    output:
    tuple val (type), path ("time_window_*")

    script:
    """

        MINDATE=\$(cut -f 1 $min_max_date)
        MAXDATE=\$(cut -f 2 $min_max_date)

        GenerateTimeWindows.py -b \$MINDATE -e \$MAXDATE -m ${window_size} -s ${window_step} > tmp
        split -l 1 tmp time_window_
        rm tmp

    """
}


//Perform analyses 

process PerformBDMLAnalysis {

    publishDir "${params.results}/02a_BDML_estimation/${type}",  mode: 'copy'
    
    label 'bdevo'

    input:
    tuple val(type), val(window), path(treefile), path(sampling_proportion)


    output:
    tuple val(type), val(window), path("BD_analysis_${window}.tsv")


    script:
    """
        
        bd_infer --nwk $treefile --ci --psi 0.142 --log BD_analysis_${window}.tsv

    """
//         bd_infer --nwk $treefile --ci --p \$(cat $sampling_proportion) --lower_bounds 0 0.15 0 --upper_bounds 1 0.25 1 --log BD_analysis_${window}.tsv

}

process PerformBDEIMLAnalysis {

    publishDir "${params.results}/02b_BDEIML_estimation/${type}",  mode: 'copy'
    
    label 'bdeievo'

    input:
    tuple val(type), val(window), path(treefile), path(sampling_proportion)

    output:
    tuple val(type), val(window), path ("BDEI_analysis_${window}.tsv")

    script:
    """

        bdei_infer -t ${task.cpus} --nwk $treefile --psi 0.284 --mu 0.284 --u_policy max --CI_repetitions 500 --log BDEI_analysis_${window}.tsv
    """
//        bdei_infer --nwk $treefile --p \$(cat $sampling_proportion) --u_policy max --CI_repetitions 1000 --upper_bounds 1 1 1 1 --log BDEI_analysis_${window}.tsv

}

//Reformat data 

process ReformatBDMLOutput {

    publishDir "${params.results}/02a_BDML_estimation/${type}",  mode: 'copy'

    label 'pythonevol'

    input:
    tuple val(type), val(windowname), path(bdmlfile), path(windowfile)

    output:
    tuple val(type), path ("bdml_output_${windowname}.txt")

    script:
    """

    ReformatBDMLResults.py --bdmlfile $bdmlfile --datefile $windowfile --output_file bdml_output_${windowname}.txt

    """
}

process ReformatBDEIMLOutput {

    publishDir "${params.results}/02b_BDEIML_estimation/${type}",  mode: 'copy'

    label 'pythonevol'

    input:
    tuple val(type), val(windowname), path(bdeimlfile), path(windowfile)

    output:
    tuple val(type), path ("bdeiml_output_${windowname}.txt")

    script:
    """

    ReformatBDEIMLResults.py --bdeimlfile $bdeimlfile --datefile $windowfile --output_file bdeiml_output_${windowname}.txt

    """
}

// Merge data following type 

//Manage files

process MergeBDMLFiles {
    
    publishDir "${params.results}/02a_BDML_estimation/${type}",  mode: 'copy'

    input:
    tuple val(type), path(bdml_files)

    output:
    tuple val(type), path ("bdml_output_${type}.tsv" )

    script:
    """
    first=true
    
    for file in ${bdml_files}; do
        if [ \$first = true ]
        then
            cat \$file > bdml_output_${type}.tsv
            first=false
        else
            tail -n +2 \$file >> bdml_output_${type}.tsv
        fi
    done

    """

}

process MergeBDEIMLFiles {
    
    publishDir "${params.results}/02b_BDEIML_estimation/${type}",  mode: 'copy'

    input:
    tuple val(type), path(bdeiml_files)

    output:
    tuple val(type), path ("bdeiml_output_${type}.tsv" )

    script:
    """
    first=true
    
    for file in ${bdeiml_files}; do
        if [ \$first = true ]
        then
            cat \$file > bdeiml_output_${type}.tsv
            first=false
        else
            tail -n +2 \$file >> bdeiml_output_${type}.tsv
        fi
    done

    """

}

//Create graphs from BDML and BDEIML analyses

process GetGraphsBDML {

    publishDir "${params.results}/03a_BDML_estimation/${type}",  mode: 'copy'
    
    label 'rbdskytools'

    input:
    tuple val(type), path(bdmlfile)

    output:
    tuple val(type), path ("*_plot.pdf")

    script:
    """
    
        cp -r ${baseDir}/bin/GetGraphsBDML.R ./
        Rscript GetGraphsBDML.R $bdmlfile

    """

}

process GetGraphsBDEIML {

    publishDir "${params.results}/03b_BDEIML_estimation/${type}",  mode: 'copy'
    
    label 'rbdskytools'

    input:
    tuple val(type), path(bdeimlfile)

    output:
    tuple val(type), path ("*_plot.pdf")

    script:
    """
    
        cp -r ${baseDir}/bin/GetGraphsBDEIML.R ./
        Rscript GetGraphsBDEIML.R $bdeimlfile

    """

}
