//INFER TREE

process InferTree {

    publishDir "${params.results}/01_TreeManip/${type}",  mode: 'copy'
    
    label 'nextstrainbase'

    input:
    tuple val(type), path (alnfile), path(metadata), path (dates)

    output:
    tuple val(type), path("${alnfile.baseName}.treefile"), path (dates)

    script:
    """
        
        nb_seq=\$(grep ">" $alnfile | wc -l | tr -d '\\n')
        if [ \$nb_seq -ge 1000 ]
        then
            FastTreeDblMP -gtr -nt < $alnfile > ${alnfile.baseName}.treefile
        else 
            iqtree -ninit 2 -n 2 -me 0.05 -nt ${task.cpus} -s $alnfile -m GTR -ninit 10 -n 4 -pre ${alnfile.baseName}
        fi
        cat ${alnfile.baseName}.treefile | sed 's/_/\\//g' > tmp.txt
        mv tmp.txt ${alnfile.baseName}.treefile

    """

}

process TreetimeAnalysis { // Deprecated

    publishDir "${params.results}/01_TreeManip/${type}",  mode: 'copy'
    
    label 'nextstrainbase'

    input:
    path (treefile)
    path (alnfile)
    path (datefile)

    output:
    path ("results/*")
    path ("results/timetree.nexus")
    path ("results/outliers.tsv")

    script:
    """

        mkdir results 
        treetime --tree $treefile --aln $alnfile --dates $datefile --clock-rate 0.0008 --clock-std-dev 0.0004 --reconstruct-tip-states --stochastic-resolve --reroot 'Wuhan/Hu-1/2019' --outdir results
        if [ ! -f results/outliers.tsv ]
        then
            touch results/outliers.tsv
        fi

    """

}

process LSD2Analysis {

    publishDir "${params.results}/01_TreeManip/${type}",  mode: 'copy'
    
    label 'lsd2'

    input:
    tuple val(type), path(treefile), path (datefile)

    output:
    tuple val(type), path ("lsd2_analysis.date.nexus")
    tuple val(type), stdout

    script:
    """
    sed '1d' $datefile > tmp.txt
    nb_indiv=\$(cat tmp.txt | wc -l | tr -d '\\n')
    echo \$nb_indiv > date_file_for_lsd2.tsv
    cat tmp.txt >> date_file_for_lsd2.tsv

    lsd2_analysis=\$(lsd2 -i $treefile -d date_file_for_lsd2.tsv -u 0.00273972603 -U 0.00273972603 -l 0 -e 2 -D 1 -o lsd2_analysis)
    tMRCA=\$(echo  \$lsd2_analysis | grep "tMRCA" | sed "s/.*tMRCA \\([^ ]*\\).*/\\1/")
    
    echo \$tMRCA | tr -d "\\n"

    """

}

// Manipulate trees 

process ResolvePolytomies {

    publishDir "${params.results}/01_TreeManip/${type}",  mode: 'copy'
    
    label 'gotree'

    input:
    tuple val(type), path (treefile)

    output:
    tuple val(type), path ("${treefile.baseName}_polytomies_solved.nwk")

    script:
    """
        gotree resolve -i $treefile > tmp.txt
        gotree brlen setmin -i tmp.txt -l 0.00273972603 > ${treefile.baseName}_polytomies_solved.nwk
    """

}

process RemoveAnnotations {

    publishDir "${params.results}/01_TreeManip/${type}",  mode: 'copy'
    
    label 'gotree'

    input:
    tuple val(type), path (treefile)

    output:
    tuple val(type), path ("${treefile.baseName}_wo_annotations.nwk")

    script:
    """
        gotree comment clear -i $treefile > ${treefile.baseName}_wo_annotations.nwk
    
    """

}

process ReformatTree {

    publishDir "${params.results}/01_TreeManip/${type}",  mode: 'copy'
    
    label 'gotree'

    input:
    tuple val(type), path (treefile)

    output:
    tuple val(type), path ("${treefile.baseName}_reformated.nexus")

    script:
    """
        gotree reformat nexus -i $treefile -f newick > ${treefile.baseName}_reformated.nexus
    """
}

process CutTreeDate {

    publishDir "${params.results}/01_TreeManip/${type}",  mode: 'copy'
    
    label 'pythonevol'

    input:
    tuple val(type), path (treefile), path (timewindow)

    output:
    tuple val(type), val(timewindow.baseName), path("${treefile.baseName}_date_cut_${timewindow.baseName}.nwk"), path (timewindow)

    script:
    """
        MINDATE=\$(cut -f 1 $timewindow)
        MAXDATE=\$(cut -f 2 $timewindow)
        CutTreeByDate.py --tree $treefile --min_date \$MINDATE --max_date \$MAXDATE --forest ${treefile.baseName}_date_cut_${timewindow.baseName}.nwk

    """

}

process RescaleTreeToDays {

    publishDir "${params.results}/01_TreeManip/${type}",  mode: 'copy'
    
    label 'pythonevol'

    input:
    tuple val(type), val(window), path(treefile), path (windofile)

    output:
    tuple val(type), val(window), path("${treefile.BaseName}_rescaled.nwk")

    script:
    """
        RescaleTreeToDays.py --input_forest $treefile --output_forest ${treefile.BaseName}_rescaled.nwk
    """

}

process RemoveTips {

    publishDir "${params.results}/01_TreeManip/${type}",  mode: 'copy'
    
    label 'gotree'

    input:
    tuple val(type), path (treefile)
    path (reference)

    output:
    tuple val(type), path ("${treefile.BaseName}_tips_removed.nwk")

    script:
    """
        cut -f1 $reference/${type}_reference.txt > ignored_tips_list.txt
        gotree prune --format nexus -i $treefile -f ignored_tips_list.txt >> ${treefile.BaseName}_tips_removed.nwk

    """

}

process RerootTree {

    publishDir "${params.results}/01_TreeManip/${type}",  mode: 'copy'
    
    label 'gotree'

    input:
    tuple val(type), path (treefile), path (dates)
    path (reference)

    output:
    tuple val(type), path ("${treefile.baseName}_rerooted.nwk"), path (dates)

    script:
    """

        gotree reroot outgroup -i $treefile -l $reference/"${type}"_reference.txt -o ${treefile.baseName}_rerooted.nwk
    
    """

}

// Extract infos from tree 

process GetLabelsFromForest {
    
    label 'pythonevol'

    input:
    tuple val(type), val(windowname), path(treefile), path (timewindow)

    output:
    tuple val(type), val(timewindow.baseName), path("labels_${timewindow.baseName}.txt"), path (timewindow)

    script:

    """
        GetLabelsOfTree.py --input_tree $treefile --output_file labels_${timewindow.baseName}.txt
    """

}

process GetLabelsFromTree {
    
    label 'pythonevol'

    input:
    tuple val(type), path (treefile)

    output:
    tuple val(type), path("labels.txt")

    script:

    """
        GetLabelsOfTree.py --input_tree $treefile --output_file labels.txt
    """

}

process GetTreeHeight {
    
    label 'gotree'

    input:
    tuple val (type), path (treefile)

    output:
    tuple val (type), path ("maxheight.txt")

    script:
    """

        gotree stats tips -i $treefile | cut -f6 | sort -n | tail -1 | tr -d '\\n' > maxheight.txt
    
    """

}

process GetDatesFromLabels {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'pythonevol'

    input:
    tuple val(type), path (labelfile), path (sequences), path(metadata), path(datefile)

    output:
    tuple val(type), path ("${datefile.baseName}_dates_wo_outliers.tsv")

    script:
    """
        head -n 1 $datefile > ${datefile.baseName}_dates_wo_outliers.tsv
        grep -f $labelfile $datefile >> ${datefile.baseName}_dates_wo_outliers.tsv

   """

}


