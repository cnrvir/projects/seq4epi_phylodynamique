
//Prepare CovFlow analysis 

process GetNumberOfMonths {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'pythonevol'

    input:
    tuple val(type), path (datefile), val (tmrca)

    output:
    tuple val(type), path ("${type}_nb_of_dimensions.txt")

    script:
    """
        GetNumberOfMonths.py --tmrca $tmrca --datefile $datefile | tr -d '\\n' > ${type}_nb_of_dimensions.txt

   """

}

process CovFlow {

    cpus 10 
    memory '20 GB'

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    conda "covflow.yaml"

    input:
    tuple val (type), path (treefile), path (dates), path (maxheight), path (dimensions)

    output:
    tuple val (type), path ("*bdsky.xml")
    tuple val (type), path ("*coalsky.xml")

    script:
    """

        cp -r ${baseDir}/assets/* ./
        snakemake --use-conda --config treefile=$treefile dates=$dates maxheight=$maxheight dimensionfile=$dimensions --cores 10
   
    """

}

//Run BEAST

process RunBeastBDSky {
    publishDir "${params.results}/02a_BDSKY/${type}",  mode: 'copy'

    label 'beast262bdsky'

    input:
    tuple val (type), path (xml)

    output:
    tuple val (type), path ("*.log")

    script:
    """

        beast -threads ${task.cpus} $xml

    """
}

process RunBeastCoalSky {
    publishDir "${params.results}/02b_CoalSky/${type}",  mode: 'copy'

    label 'beast262bdsky'

    input:
    tuple val (type), path (xml)

    output:
    tuple val (type), path ("*.log")

    script:
    """

        beast -threads ${task.cpus} $xml

    """
}

//Create BEAST graphs

process GetGraphsBDsky {

    publishDir "${params.results}/03_Beast_graphs/${type}",  mode: 'copy'
    
    label 'rbdskytools'

    input:
    tuple val (type), path (log_file), path (date_file)

    output:
    path ("graphs*")

    script:
    """
    
        cp -r ${baseDir}/bin/plot_bdsky_log.R ./
        Rscript plot_bdsky_log.R --flog $log_file --fdates $date_file --outputprefix graphs

    """

}