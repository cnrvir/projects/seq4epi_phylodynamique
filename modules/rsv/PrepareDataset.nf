//Extract and Filter

process ParseGisaidFile {

    publishDir "${params.results}/00_PreparedDataset",  mode: 'copy'
    
    label 'pythonevol'

    input:
    path (gisaid_file)
    path (region_file)

    output:
    tuple val("RSVA"), path ("sequences_RSVA.fasta"), path ("metadata_RSVA.tsv")
    tuple val("RSVB"), path ("sequences_RSVB.fasta"), path ("metadata_RSVB.tsv")

    script:
    """

        ParseGisaidFile.py -g $gisaid_file -r $region_file -o sequences_RSVA.fasta -u metadata_RSVA.tsv -s sequences_RSVB.fasta -t metadata_RSVB.tsv

    """

}

process FilterNext {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'nextstrainbase'

    input:
    tuple val(type), path(sequences), path (metadata)
    val (country)
    val (sample_min_date)
    val (sample_max_date)
    val (nb_max)

    output:
    tuple val (type), path(sequences), path ("${metadata.baseName}_filternext.tsv")

    script:
    """

        augur filter --metadata $metadata --exclude-where 'country!=$country' --min-date $sample_min_date --max-date $sample_max_date --group-by division year month --subsample-max-sequences $nb_max --output-metadata ${metadata.baseName}_filternext.tsv

   """

}

process FilterRandom {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'nextstrainbase'

    input:
    tuple val(type), path(sequences), path (metadata)
    val (nb_max)

    output:
    tuple val (type), path(sequences), path ("${metadata.baseName}_filternext.tsv")

    script:
    """

        head -n 1 $metadata > ${metadata.baseName}_filternext.tsv
        tail -n+2 $metadata | cut -f1 | shuf -n $nb_max > tmp.txt
        grep -f tmp.txt $metadata >> ${metadata.baseName}_filternext.tsv

   """

}

// Add other datasets 

process MergeRefQuerydataset {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'

    label 'pythonevol'

    input:
    tuple val(type), path(sequences), path (metadata)
    path (reference)

    output:
    tuple val(type), path("${sequences.baseName}_w_ref.fasta"), path ("${metadata.baseName}_w_ref.tsv")


    script:
    """

		cat $reference/${type}_ref_dataset.fasta > ${sequences.baseName}_w_ref.fasta
		echo -e '' >> ${sequences.baseName}_w_ref.fasta
		cat $sequences >> ${sequences.baseName}_w_ref.fasta
        sed -i '/^\$/d' ${sequences.baseName}_w_ref.fasta

		cat $reference/${type}_ref_dataset.tsv > ${metadata.baseName}_w_ref.tsv
		echo -e '' >> ${metadata.baseName}_w_ref.tsv
		tail -n+2 $metadata >> ${metadata.baseName}_w_ref.tsv
        sed -i '/^\$/d' ${metadata.baseName}_w_ref.tsv
        
    """

}


//Manipulate newly created dataset

process RemoveDuplicates {  

    label 'seqkit'

    input:
    tuple val(type), path (sequences), path(metadata)

    output:
    tuple val(type), path ("${sequences.baseName}_rmdup.fasta"), path("${metadata.baseName}_rmdup.tsv")

    script:
    """

        awk -F"\\t" '!_[\$1]++' $metadata > ${metadata.baseName}_rmdup.tsv
        seqkit rmdup -n $sequences > ${sequences.baseName}_rmdup.fasta

    """

}

process RemoveForbiddenChar {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'pythonevol'

    input:
    tuple val (type), path (sequences), path (metadata)

    output:
    tuple val(type), path ("${sequences.baseName}_nfc.fasta"), path ("${metadata.baseName}_nfc.tsv")

    script:
    """

        cat $sequences | sed "s/([^()]*)//g" | sed "s/\\'//g" | sed "s/?/-/g" | sed "s/  / /g" > ${sequences.baseName}_nfc.fasta
        cat $metadata | sed "s/([^()]*)//g" | sed "s/\\'//g" | sed "s/?/-/g" | sed "s/  / /g" > ${metadata.baseName}_nfc.tsv
    
    
    """

}

process GrepSequences {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'seqkit'

    input:
    tuple val(type), path (sequences), path(metadata)

    output:
    tuple val(type), path ("${sequences.baseName}_filternext.fasta"), path(metadata)

    script:
    """

        cut -f1 $metadata > names.txt
        seqkit grep -f names.txt $sequences > ${sequences.baseName}_filternext.fasta

   """

}


process GetDates {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'pythonevol'

    input:
    tuple val(type), path (sequences), path(metadata)

    output:
    tuple val(type), path (sequences), path(metadata), path("${metadata.baseName}_dates.tsv")


    script:
    """

        cut -f1,5 $metadata > ${metadata.baseName}_dates.tsv

   """

}

process FillMissingInfosDates {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'pythonevol'

    input:
    tuple val(type), path (sequences), path(metadata), path(datefile)

    output:
    tuple val(type), path (sequences), path(metadata), path("${datefile.baseName}_corrected.tsv")


    script:
    """

        FillMissingInfosDates.py --date_file $datefile --output_file ${datefile.baseName}_corrected.tsv

   """

}

// Align and prepare sequences 

process NextAlign {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'nextstrainbase'

    input:
    tuple val(type), path (sequences), path(metadata), path(dates)
    path (reference)


    output:
    tuple val(type), path ("${sequences.baseName}_aligned.fasta"), path(metadata), path(dates)

    script:
    """
        nextalign run --jobs ${task.cpus} --input-ref $reference/"${type}_reference.fasta" --output-fasta ${sequences.baseName}_aligned.fasta $sequences

    """

}

process AddReference {

    publishDir "${params.results}/00_PreparedDataset/${type}",  mode: 'copy'
    
    label 'pythonevol'

    input:
    tuple val(type), path (sequences), path(metadata)
    path (reference)


    output:
    tuple val(type), path ("${sequences.baseName}_w_ref.fasta"), path("${metadata.baseName}_w_ref.tsv")



    script:
    """
		cat $reference/"${type}_reference.fasta" > ${sequences.baseName}_w_ref.fasta
		echo -e '' >> ${sequences.baseName}_w_ref.fasta
		cat $sequences >> ${sequences.baseName}_w_ref.fasta
        sed -i '/^\$/d' ${sequences.baseName}_w_ref.fasta

		   
		cat $reference/"${type}_reference.tsv" > ${metadata.baseName}_w_ref.tsv
		echo -e '' >> ${metadata.baseName}_w_ref.tsv
		tail -n+2 $metadata >> ${metadata.baseName}_w_ref.tsv
        sed -i '/^\$/d' ${metadata.baseName}_w_ref.tsv

   """

}
