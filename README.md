
<div style="text-align:center">
<img src="./images/starphylo_logo_tmp.png" height="500" width="500"/> 
</div>

# Starphylo

## Introduction

Starphylo is a nextflow pipeline for phylodynamic analysis of respiratory viruses genome sequences. The main goal of this pipeline is to calculate epidemiological parameters following an "almost" real-time philosophy.

Right now, the code performs analyses on the following viruses / subtypes:

* SARS-CoV-2
* Influenza-A: H1N1pdm
* Influenza-A: H3N2
* Influenza-B: Victoria
* RSV-A
* RSV-B

The pipeline consists of four main tasks: 

* Creation of a reference dataset to ensure a good representation of the epidemic as a whole.
* Phylodynamics analysis using a modified version of [CovFlow](https://gitlab.in2p3.fr/ete/CoV-flow). CovFlow generates two XML files ready to use to run a Birth-Death Skyline (or [BDSKY](https://github.com/BEAST2-Dev/bdsky)) and a Coalescent Skyline analysis (or [CoalSKY](https://taming-the-beast.org/tutorials/Skyline-plots/)) on [Beast2](https://www.beast2.org/). Theses analyses can infer epidemiological parameters such as R0 and "BecomeUninfectious" rate (i.e, removal rate). These parameters can be estimated for different time frames (e.g, every month of a year).
* Maximum-Likelihood estimation of epidemiological parameters using [bd(pn)](https://github.com/evolbioinfo/bdpn) and [bdei](https://github.com/evolbioinfo/bdei) softwares. With this solution, one can also estimates these epidemiological parameters on different time frames. To do so, the phylogenetic tree is cut as a "forest". See the cut [code](https://github.com/evolbioinfo/bdpn/blob/main/hiv_b_uk/py/cut_tree.py) and [explanation](https://github.com/evolbioinfo/gotree/blob/dev/docs/commands/cut.md).
* A deep-learning solution, that can estimate epidemiological parameters on a phylogeny. This step is performed thanks to the [PhyloDeep](https://github.com/evolbioinfo/phylodeep) software. It is advised to use this step on a "general" dataset (i.e., that represents the full epidemic).

Roughly, the last three steps are composed of the following steps: 

* filtering of data to create a "focused" dataset (e.g a specific period of time, a specific lineage, ...).
* adding of the newly created dataset to the reference dataset to create a full dataset.
* alignment and masking of the alignment
* Inference of a tree and of a timetree
* Manipulation of the tree/timetree (if needed) and creation of xml files (for the Beast analysis)
* Inference of epidemiological parameters using informations contained in the timetree. 

The pipeline uses the workflow management system [NextFlow](https://www.nextflow.io/).  

<div style="text-align:center">
<img src="./images/starphylo_workflow_dag.svg"height="1000" width="1000"/>
</div>


### Input files

Input files must come from the [GISAID](https://gisaid.org/) website. 

#### SARS-CoV-2

For SARS-CoV-2, it is advised to download the full metadata and sequences from the website (download tab). They must be at the "tar.xz" format: 

```
sequences_DDMMYYYY.tar.xz
metadata_DDMMYYYY.tar.xz
```

#### Influenza viruses 

For Influenza, a conventional query dataset (for now, only HA sequences) that should look like this: 

```
h1n1pdm.fasta
h1n1pdm.xls
h3n2.fasta
h3n2.xls
vic.fasta
vic.xls
```

Please note that it's mandatory for influenza viruses to arbor, at minimum, the name of the subtype in the filenames. Thus, you need to rename the input files after downloading them (`gisaid_epiflu[...]`).

#### RSV

For RSV, a conventional query dataset that should look like this: 

```
gisaid_rsv_YYYY_MM_DD.fasta
```

The GISAID RSV fasta files habe headers containing the metadata of samples. It's not necessary to give a metadata file as input.

#### Notes

Please note that for the Beast and Maximum-Likelihood, a supplementary reference dataset can be provided. This dataset can be interpretated as a "context" for the samples you want to focus on. This contextual dataset, that can be given to the workflow as a supplementary input, must follow a specific nomenclature and format. You can create one with the step described below.


## How to launch an Starphylo analysis

### Virus-specific analysis

To date, Starphylo works with three different "categories" of viruses :

* SARS-CoV-2 (namely, `sars-cov-2` in the pipeline)
* Influenza viruses (namely, `flu` in the pipeline)
* RSV (namely, `rsv` in the pipeline)

You can select which virus you're working on in the workflow with the `--type` parameter. 


```
newtflow run main.nf --type sars-cov-2
```

We saw previously that the input dataset differ following the considered virus. Thus, the input parameters also differ in the workflow.

#### SARS-CoV-2 

The pipeline takes as input a (`--fasta`) `<path/to/gisaid_fasta.tar.xz>` sequence file and a (`--metadata`) `<path/to/gisaid_metadata.tar.xz>` metadata file. Both of these files must come from a "download" step of [GISAID](https://gisaid.org/). 

```
newtflow run main.nf --type sars-cov-2
                     --fasta <path/to/gisaid_fasta.tar.xz> 
                     --metadata <path/to/gisaid_metadata.tar.xz>
                     [...]
```

Please note that whatever your analysis, the **Wuhan/Hu-1/2019** and **Wuhan/WH01/2019** references will be automatically and temporarly added to the dataset. They will be used to root the tree and then be removed.

#### Influenza viruses

The pipeline takes as input a folder (`--input_folder`) containing the fasta and metadata files. The files must correspond to at least one of the following subtype: `h1n1pdm`, `h3n2` or `vic`. The files must come from a "download" step of [GISAID](https://gisaid.org/). You should not mix the datasets altogether. Instead, the fasta/metadata couples should be renamed so they contain the keywords related to their subtype (e.g. `h1n1pdm.fasta` and `h1n1pdm.xls`)

```
newtflow run main.nf --type flu
                     --input_folder <path/to/folder/> 
                     [...]
```

Please note that whatever your analysis, the **A/California/07/2009** (H1N1pdm), **A/Wisconsin/67/2005** (H3N2) and **B/Brisbane/60/2008** (Victoria) references will be automatically and temporarly added to the dataset. They will be used to root the tree and then be removed.

#### RSV

The pipeline takes as input a fasta file (`--gisaid_file`). The file must come from a "download" step of [GISAID](https://gisaid.org/). The GISAID RSV fasta files habe headers containing the metadata of samples. It's not necessary to give a metadata file as input. Here, you can mix (or not) RSVA and RSVB files. If not mixed, you must perform the analysis separately (for each RSV subtype). It is thus advised to mix RSVA and RSVB samples if you plan to analyze the two of them.

```
newtflow run main.nf --type rsv
                     --gisaid_file <path/to/gisaid_file.fasta> 
                     [...]
```

Please note that whatever your analysis, the **England/397/2017** (RSV-A) and **Australia/VIC-RCH056/2019** (RSV-B) references will be automatically and temporarly added to the dataset. They will be used to root the tree and then be removed.

### Disclaimer

Before going any further in this README, please note that we are going to use the `--type sars-cov-2` to display the functionalities of Starphylo. If you want to perform analyses on Influenza viruses or RSV, you should modify the commands so they reflect the input files described above.

### Creation of the reference dataset

If you want to perform a BEAST or ML analysis, you may need a reference dataset that represents well the epidemic from its beginning to, at least, the latest date of sampling you focus on. This dataset should represent well the epidemic spatio-temporally. To do so, we mainly use the augur [filter](https://docs.nextstrain.org/projects/augur/en/stable/usage/cli/filter.html) function of nextstrain, with a selection following a "week, country, division" focus. 

The pipeline takes as input a `<path/to/gisaid_fasta.tar.xz>` sequence file and a `<path/to/gisaid_metadata.tar.xz>` metadata file. Both of these files must come from a "download" step of [GISAID](https://gisaid.org/). 

To launch this pipeline, you must do as following:

```
nextflow run main.nf --type sars-cov-2
                     --mode create_ref_dataset 
                     --results <path/to/results_folder> 
                     --nb_max <int> 
                     --sample_min_date <YYYY-MM-DD> 
                     --sample_max_date <YYYY-MM-DD> 
                     --country <string> 
                     --fasta <path/to/gisaid_fasta.tar.xz> 
                     --metadata <path/to/gisaid_metadata.tar.xz>
```

Description of the arguments: 

* --nb_max: the maximum number of samples in the reference dataset. Please note that the reference dataset may not have the 
* --country: the country focused on for the subsampling (for example, "France")
* --sample_min_date: the earliest date of your subsampling. We advise to use 2019-01-01 (i.e, before the beginning of the epidemic)
* --sample_max_date: the latest date of your subsampling. We advise to use today date if you want to represent fully the epidemic, Or you can pick the latest date of interest.

Several step of filtering and of deduplication of data will be performed, resulting on three final files that can be found on the <path/to/results/00_PreparedDataset> folder: the `sequences_filtered_rmdup_filternext_w_ref_nfc.fasta` sequence file and the `metadata_rmdup_filternext_w_ref.tsv` metadata file. 

### Performing a Bayesian analysis

The Bayesian analysis consists of the estimation of epidemiological parameters through a "Birth-Death Skyline" (BDSKY) and a "Coalescent Skyline" (CoalSKY) models. Here, thanks to the "Skyline" approach, we aim to estimate the epidemiological parameters on a time grid comprising of n cuts. With Starphylo, the number of cuts is automatically determined by multiplying the height of the timetree by 12, so that n represents the number of months between the origin and the last date of the tree. 
The pipeline will first filter the raw data to create a "focused" dataset" following different variables (e.g, focus on a specific country and / or on a period of time). It is possible to merge automatically this "focused dataset" with a "reference" one (for example, one created with the `--mode create_ref_dataset`). Starphylo performs then a timetree inference on the selected dataset. A modified version of CovFlow will be used to generate XML files readable by BEAST2. Finally, a bayesian inference of epidemiological parameters will be performed using BEAST2.

The pipeline takes as input a `<path/to/gisaid_fasta.tar.xz>` sequence file and a `<path/to/gisaid_metadata.tar.xz>` metadata file. Both of these files must come from a "download" step of [GISAID](https://gisaid.org/). Another set of files required are the reference dataset (must be uncompressed!): a `<path/to/ref_fasta.fasta>` sequence file and a `<path/to/ref_metadata.tsv>` metadata file. Please note that the metadata file must be splitted in four columns instead of one for the location column (region / country / division / location --> region | country | division | location).


To launch this pipeline, you must do as following:


```
nextflow run main.nf --type sars-cov-2
                     --mode beast 
                     --results <path/to/results_folder> 
                     --nb_max <int> 
                     --sample_min_date <YYYY-MM-DD> 
                     --sample_max_date <YYYY-MM-DD> 
                     --country <string> 
                     --fasta <path/to/gisaid_fasta.tar.xz>
                     --metadata <path/to/gisaid_metadata.tar.xz>
                     --ref_fasta <path/to/ref_fasta.fasta>
                     --ref_metadata <path/to/ref_metadata.tsv>
                     --random_sampling <yes|no>
                     --use_context <yes|no>
```

Description of the arguments: 

* --nb_max: the maximum number of samples in the reference dataset. Please note that the reference dataset may not have the 
* --country: the country focused on for the subsampling (for example, "France")
* --sample_min_date: the earliest date of your subsampling. We advise to use 2019-01-01 (i.e, before the beginning of the epidemic)
* --sample_max_date: the latest date of your subsampling. We advise to use today date if you want to represent fully the epidemic, Or you can pick the latest date of interest.
* --random_sampling: If positive, the selection of samples will be random. If not, Starphylo will use 'augur filter' to select samples following location and date.
* --use_context: If positive, the selected dataset will be added to an already existing dataset (for example, one created with the `--mode create_ref_dataset` step). Otherwise, the analysis will be performed on the selected dataset.


### Performing a Maximum Likelihood analysis

The Maximum Likelihood analysis consists of the estimation of epidemiological parameters through a "Birth-Death" model (bdpn) and Birth-Death-Exposed-Infectious (bdei) model. Here, we aim to estimate the epidemiological parameters on a time frame. To do so, the pipeline will first filter the dataset to create a "focused" dataset (e.g, on a period of time such as 2022-09-01 // 2023-09-01). Then, this "focused dataset" will be merged with a reference dataset (for example, one created with the `--mode create_ref_dataset`). Then, a timetree will be inferred using iqtree and treetime softwares. the timetree will undergo a certain number of steps of preparation (trimming of outliers, suppression of polytomies and 0-length branches). The final step is the creation of a "forest" for the desired time-frame, using the a [cut](https://github.com/evolbioinfo/gotree/blob/dev/docs/commands/cut.md) function. Finally, epidemiological parameters will be inferred on the forest. 


The pipeline takes as input a `<path/to/gisaid_fasta.tar.xz>` sequence file and a `<path/to/gisaid_metadata.tar.xz>` metadate file. Both of these files must come from a "download" step of [GISAID](https://gisaid.org/). Another set of files required are the reference dataset (must be uncompressed!): a `<path/to/ref_fasta.fasta>` sequence file, a `<path/to/ref_metadata.tsv>` `<path/to/ref_dates.tsv>`. Please note that the metadate file must be splitted in four columns instead of one for the location column (region / country / division / location --> region | country | division | location).


To launch this pipeline, you must do as following:


```
nextflow run main.nf --type sars-cov-2
                     --mode ml
                     --results <path/to/results_folder> 
                     --nb_max <int> 
                     --sample_min_date <YYYY-MM-DD> 
                     --sample_max_date <YYYY-MM-DD>  
                     --window_size <float>
                     --window_step <float>
                     --country <string> 
                     --fasta <path/to/gisaid_fasta.tar.xz> 
                     --metadata <path/to/gisaid_metadata.tar.xz> 
                     --ref_fasta <path/to/ref_fasta.fasta> 
                     --ref_metadata <path/to/ref_metadata.tsv> 
                     --random_sampling <yes|no>
                     --use_context <yes|no>
```

Description of the arguments: 

* --nb_max: the maximum number of samples in the reference dataset. Please note that the reference dataset may not have the 
* --country: the country focused on for the subsampling (for example, "France")
* --sample_min_date: the earliest date of your subsampling. We advise to use 2019-01-01 (i.e, before the beginning of the epidemic)
* --sample_max_date: the latest date of your subsampling. We advise to use today date if you want to represent fully the epidemic, Or you can pick the latest date of interest.
* --random_sampling: If positive, the selection of samples will be random. If not, Starphylo will use 'augur filter' to select samples following location and date.
* --use_context: If positive, the selected dataset will be added to an already existing dataset (for example, one created with the `--mode create_ref_dataset` step). Otherwise, the analysis will be performed on the selected dataset.
* --window_size: The size of each window (month unit). 1 is equivalent to one month, and 0.25 to 1/4 (roughly one week).
* --window_step: The step of each window (month unit). 1 is equivalent to one month, and 0.25 to 1/4 (roughly one week).

Deprecated arguments (these arguments may be kept in the future): 
* --min_date: the beginning of the time frame of interest. It could be the same as "--sample_min_date", but can also differ (it should be contained between "--sample_min_date" and "--sample_max_date" tho). Please note that you can relaunch this code as much as you want on the same "--sample_date" (since it's the same sampling, it will be the exact same tree). Modyfying the "--min_date" parameters will modify the forest on which you'll perform the estimation of epidemiological parameters.
* --max_date: the end of the time frame of interest. It could be the same as "--sample_max_date", but can also differ (it should be contained between "--sample_min_date" and "--sample_max_date" tho). Please note that you can relaunch this code as much as you want on the same "--sample_date" (since it's the same sampling, it will be the exact same tree). Modyfying the "--max_date" parameters will modify the forest on which you'll perform the estimation of epidemiological parameters.

#### Small caution 

Among the parameters used by the Maximum Likelihood approach to infer the epidemiological parameters, it is mandatory to fix one of them. It is usually advised to fix the "sampling proportion one" but it may be tricky in our case. We need to investigate on rather the usage of the "removal rate" parameter.

### Performing a PhyloDeep analysis

The Phylodeep analysis consists of the estimation of epidemiological parameters through a "Birth-Death" model (BD), a Birth-Death-Exposed-Infectious (BDEI) and a Birt-Death Super-Spreader (BDSS) models. Phylodeep is based on deep learning: it used thousand of simulated trees to train these three models to best estimate the epidemiological parameters. To do so, the pipeline will first filter the dataset to create a "focused" dataset (e.g, on a period of time such as 2022-09-01 // 2023-09-01). Then, a timetree will be inferred using iqtree and treetime softwares. the timetree will undergo a certain number of steps of preparation (trimming of outliers, suppression of polytomies and 0-length branches). The tree will be given as an input to phylodeep. Phylodeep will estimate the epidemiological parameters using the three models (these three models will be compared to see the one best-fitting the dataset). 

Please note that it is advised that PhyloDeep should be used on a tree that represents the full epidemic. 

To launch this pipeline, you must do as following:


```
nextflow run main.nf --type sars-cov-2
                     --mode phylodeep
                     --results <path/to/results_folder> 
                     --nb_max <int> 
                     --sample_min_date <YYYY-MM-DD> 
                     --sample_max_date <YYYY-MM-DD> 
                     --country <string> 
                     --fasta <path/to/gisaid_fasta.tar.xz> 
                     --metadata <path/to/gisaid_metadata.tar.xz> 
                     --ref_fasta <path/to/ref_fasta.fasta>
                     --ref_metadata <path/to/ref_metadata.tsv>
                     --random_sampling <yes|no>
                     --use_context <yes|no>
```

Description of the arguments: 

* --nb_max: the maximum number of samples in the reference dataset. Please note that the reference dataset may not have the 
* --country: the country focused on for the subsampling (for example, "France")
* --sample_min_date: the earliest date of your subsampling. We advise to use 2019-01-01 (i.e, before the beginning of the epidemic)
* --sample_max_date: the latest date of your subsampling. We advise to use today date if you want to represent fully the epidemic, Or you can pick the latest date of interest.
* --random_sampling: If positive, the selection of samples will be random. If not, Starphylo will use 'augur filter' to select samples following location and date.
* --use_context: If positive, the selected dataset will be added to an already existing dataset (for example, one created with the `--mode create_ref_dataset` step). Otherwise, the analysis will be performed on the selected dataset. It is strongly advised to perform the Phylodeep analysis.

#### Small caution 

Among the parameters used by the Phylodeep approach to infer the epidemiological parameters, it is mandatory to fix one of them. It is usually advised to fix the "sampling proportion one" but it may be tricky in our case. We need to investigate on rather the usage of the "removal rate" parameter (if possible with this tool).