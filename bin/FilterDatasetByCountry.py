#! /usr/bin/env python3
# coding: utf-8

import sys, getopt
import csv
import io
import logging
import os
import re
import gzip
import lzma
import subprocess
from Bio import SeqIO

# This script will extract informations from the GISAID meta data
# It will convert it to augur workflow format and keep only omicron 
# sequence information
# It will finally select the omicron & contextual sequences from the fasta xz archive
def main(argv):
	cpus=1
	try:
		opts, args = getopt.getopt(argv,"hm:f:c:t:o:u:")
	except getopt.GetoptError:
		print('FilterDatasetByCountry.py -m <gisaid meta tsv> -f <gisaid fasta file> -c <country> -t <cpus> -o <output filtered tsv name>  -u <output filtered fasta name>')
		sys.exit(1)
	for opt, arg in opts:
		if opt == '-h':
			print('FilterDatasetByCountry.py -m <gisaid meta tsv> -f <gisaid fasta file> -c <country> -t <cpus> -o <output filtered tsv name>  -u <output filtered fasta name>')
			sys.exit(0)

		elif opt in ("-f"):
			inputfasta = arg

		elif opt in ("-m"):
			inputmeta = arg
		
		elif opt in ("-c"): 
			country_value = arg

		elif opt in ("-t"): 
			cpus = arg

		elif opt in ("-o"):
			metadataoutfile = arg
		
		elif opt in ("-u"):
			fastaoutfile = arg

	gisaidlist=dict()
	with open(metadataoutfile, 'w') as outfile:
		logging.info(f'Parsing metadata file {inputmeta}')
		# We parse the metadata file for getting the gisaid name of the epiisl ids
		proc = subprocess.Popen(["sh","-c", f"tar -x -O -I 'xz -T{cpus}' -f {inputmeta} metadata.tsv"], stdout=subprocess.PIPE)
		tsv_file = csv.reader(io.TextIOWrapper(proc.stdout), delimiter="\t")
		
		firstline = "strain\tLast vaccinated\tPassage details/history\tType\tAccession ID\tdate\tregion\tcountry\tdivision\tlocation\tAdditional location information\tSequence length\thost\tage\tgender\tclade\tPango lineage\tPango version\tVariant\tAA Substitutions\tdate_submitted\tIs reference?\tIs complete?\tIs high coverage?\tIs low coverage?\tN-Content\tGC-Content"
		# for line in tsv_file : 
		# 	firstline = line
		# 	break
		
		print(firstline,file=outfile)

		next(tsv_file,None)
		for line in tsv_file:
			strain = str(line[0])
			epi_isl = str(line[4])

			region = "?"
			country = "?"
			division = "?"
			location = "?"
			locationcol = line[6]
			del line[6]
			locationsplit = locationcol.split("/")
			if len(locationsplit) >= 1 : 
				region = locationsplit[0].strip()
			if len(locationsplit) >= 2 : 
				country = locationsplit[1].strip()
			if len(locationsplit) >= 3 : 
				division = locationsplit[2].strip()
			if len(locationsplit) == 4 : 
				location = locationsplit[3].strip()

			line.insert(6,location)
			line.insert(6,division)
			line.insert(6,country)
			line.insert(6,region)

			if country == country_value : 
				if strain not in gisaidlist : 
					gisaidlist[strain] = epi_isl			
				print('\t'.join(map(str,line)),file=outfile)
	

	
#	proc = subprocess.Popen(["sh","-c", f"tar c -I 'xz -9 -T{cpus}' -f {metadataoutfile}.tar.xz -C $(readlink -f {metadataoutfile}) $(basename {metadataoutfile})  --remove-files"], stdout=subprocess.PIPE)
	

	with open(fastaoutfile,'wt') as outf:
		logging.info(f'Parsing fasta file {inputfasta}')
		proc = subprocess.Popen(["sh","-c", f"tar -x -O -I 'xz -T{cpus}' -f {inputfasta} sequences.fasta"], stdout=subprocess.PIPE)
		for record in SeqIO.parse(io.TextIOWrapper(proc.stdout), "fasta"):
			id=record.description.split("|")[0]
			if id in gisaidlist:
				outf.write(f'>{id}\n')
				outf.write(f'{record.seq}\n')

#	proc = subprocess.Popen(["sh","-c", f"tar c -I 'xz -9 -T{cpus}' -f {fastaoutfile}.tar.xz -C $(readlink -f {fastaoutfile}) $(basename {fastaoutfile}) --remove-files"], stdout=subprocess.PIPE)


if __name__ == "__main__":
	logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
	main(sys.argv[1:])
