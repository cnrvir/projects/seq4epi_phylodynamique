#!/usr/bin/env python3

from phylodeep import BD, BDEI, BDSS, FULL
from phylodeep.checkdeep import checkdeep
from phylodeep.modeldeep import modeldeep
from phylodeep.paramdeep import paramdeep
import argparse
import pandas as pd

from ete3 import Tree



def perform_phylodeep(treefile, sampling_proba, output_dir, output_prefix) : 

	ete3tree = Tree(treefile, format=1)
	ntips = len(ete3tree.get_leaves())

	if sampling_proba < 0.01 : #Unfortunately, we need to do this since modeldeep function cannot accept any value below 0.01
		sampling_proba = 0.01
	

	# a priori check for models BD, BDEI, BDSS
	checkdeep(treefile, model=BD, outputfile_png= output_dir + "/" + output_prefix + "_BD_a_priori_check.png")
	checkdeep(treefile, model=BDEI, outputfile_png= output_dir + "/" + output_prefix + "_BDEI_a_priori_check.png")
	if ntips > 200 : 
		checkdeep(treefile, model=BDSS, outputfile_png= output_dir + "/" + output_prefix + "_BDSS_a_priori_check.png")


	# model selection
	model_BDEI_vs_BD_vs_BDSS = modeldeep(treefile, sampling_proba, vector_representation=FULL)
	with open(output_dir + "/" + output_prefix + "_model_selection.tsv", 'w') as outfile : 
		model_BDEI_vs_BD_vs_BDSS.to_csv(outfile, sep="\t", index=False)


	# the selected model is BDSS

	# parameter inference
	param_BD = paramdeep(treefile, sampling_proba, model=BD, vector_representation=FULL, ci_computation=True)
	param_BDEI = paramdeep(treefile, sampling_proba, model=BDEI, vector_representation=FULL, ci_computation=True)
	if ntips > 200 : 
		param_BDSS = paramdeep(treefile, sampling_proba, model=BDSS, vector_representation=FULL, ci_computation=True)

	with open(output_dir + "/" + output_prefix + "_BD_FULL_parameter_estimation.tsv", 'w') as outfile : 
		param_BD.to_csv(outfile, sep="\t", index=False)

	with open(output_dir + "/" + output_prefix + "_BDEI_FULL_parameter_estimation.tsv", 'w') as outfile : 
		param_BDEI.to_csv(outfile, sep="\t", index=False)
	
	if ntips > 200 : 

		with open(output_dir + "/" + output_prefix + "_BDSS_FULL_parameter_estimation.tsv", 'w') as outfile : 
			param_BDSS.to_csv(outfile, sep="\t", index=False)

if __name__ == "__main__": ##Launching of the code
	parser = argparse.ArgumentParser(description="Performe a classic phylodeep analysis") ## initialize parser
	parser.add_argument('--treefile', '-t', required=True,help="Your input treefile",type=str) #The input csv file
	parser.add_argument('--sampling_proba_file', '-s', required=True,help="Your sampling probability",type=str) #The input csv file
	parser.add_argument('--output_dir', '-o', required=False,help="The output folder",type=str, default="./") #The output_dir
	parser.add_argument('--output_prefix', '-p', required=False,help="The output prefix",type=str, default="seq4epi_analysis") #The output_dir

	args = parser.parse_args() #Gather the args

	treefile = args.treefile

	with open(args.sampling_proba_file, 'r') as file:
			sampling_proba = float(file.read().strip())
			
	output_dir = args.output_dir

	output_prefix = args.output_prefix


	perform_phylodeep(treefile, sampling_proba, output_dir, output_prefix)

