#! /usr/bin/env python3
# coding: utf-8

import sys, getopt
import csv
import io
import logging
import os
import re
import gzip
import lzma
import subprocess
from datetime import datetime
from datetime import date
from datetime import timedelta
from Bio import SeqIO


def year_fraction(date_to_modify):

	start = date(date_to_modify.year, 1, 1).toordinal()
	year_length = date(date_to_modify.year+1, 1, 1).toordinal() - start
	return date_to_modify.year + float(date_to_modify.toordinal() - start) / year_length


# This script will extract informations from the GISAID meta data
# It will convert it to augur workflow format and keep only omicron 
# sequence information
# It will finally select the omicron & contextual sequences from the fasta xz archive
def main(argv):
	cpus=1
	try:
		opts, args = getopt.getopt(argv,"hd:")
	except getopt.GetoptError:
		print('ConvertDateToDecimalYear.py -d <YYYY-MM-DD>')
		sys.exit(1)
	for opt, arg in opts:
		if opt == '-h':
			print('ConvertDateToDecimalYear.py -d <YYYY-MM-DD>')
			sys.exit(0)

		elif opt in ("-d"):
			input_date = arg
	
	print (year_fraction(datetime.strptime(input_date, "%Y-%m-%d").date()))
		




#	proc = subprocess.Popen(["sh","-c", f"tar c -I 'xz -9 -T{cpus}' -f {fastaoutfile}.tar.xz -C $(readlink -f {fastaoutfile}) $(basename {fastaoutfile}) --remove-files"], stdout=subprocess.PIPE)


if __name__ == "__main__":
	logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
	main(sys.argv[1:])
