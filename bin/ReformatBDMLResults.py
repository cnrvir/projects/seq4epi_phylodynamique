#!/usr/bin/env python3


import pandas as pd
import argparse
from datetime import datetime
from datetime import date
from datetime import timedelta

def fraction_2_date(date_fraction):

        year = int(date_fraction)
        frac = date_fraction - float(int(date_fraction))
        year_length = (date(year+1, 1, 1) - date(year,1,1)).days
        return (date(year,1,1) + timedelta(days=int(frac*year_length))).strftime("%Y-%m-%d")


def reformat_bdml(bdmlfile, datefile, output_file):

    bdmldic = pd.read_csv(bdmlfile, sep=",")
    datedic = pd.read_csv(datefile, sep="\t", header=None)

    reformatted_data_dic = {}

    nb_rows = len(bdmldic)

    if nb_rows == 3 : 

        for col in bdmldic.columns[1:]:
            # For each column, create new columns for value, CI_min, and CI_max
            reformatted_data_dic[f'{col}_value'] = bdmldic.loc[bdmldic.index[0], col]
            reformatted_data_dic[f'{col}_CI_low'] = bdmldic.loc[bdmldic.index[1], col]
            reformatted_data_dic[f'{col}_CI_high'] = bdmldic.loc[bdmldic.index[2], col]

    elif nb_rows == 1 : 
        for col in bdmldic.columns[1:]:
            # For each column, create new columns for value
            reformatted_data_dic[f'{col}_value'] = bdmldic.loc[bdmldic.index[0], col]
        
    reformatted_data_dic['date'] = fraction_2_date(float(datedic[0][0]))

    reformatted_data_pd = pd.DataFrame([reformatted_data_dic])

    reformatted_data_pd.columns = reformatted_data_pd.columns.str.replace(' ', '_')

    # Save the final DataFrame to the specified output CSV file
    reformatted_data_pd.to_csv(output_file, sep="\t", index=False)
    
if __name__ == "__main__":
    # Initialize argument parser
    parser = argparse.ArgumentParser(description="Reformat CSV files with confidence intervals.")
    
    # Argument for input files (multiple files can be given)
    parser.add_argument("--bdmlfile", "-t", required=True, help='CSV files to be reformatted.')
    
    # Argument for the list of dates (comma-separated, should match the number of files)
    parser.add_argument('--datefile', "-d", required=True, help='Comma-separated list of dates corresponding to the CSV files.')
    
    # Argument for specifying the output file name
    parser.add_argument('--output_file', "-o", required=True, help='Name of the output CSV file.')

    # Parse the arguments
    args = parser.parse_args()
        
    # Call the main function with files, dates, and output file name
    reformat_bdml(args.bdmlfile, args.datefile, args.output_file)