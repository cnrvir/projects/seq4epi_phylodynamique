#! /usr/bin/env python3
# coding: utf-8

import sys, getopt
import csv
import io
import logging
import os
import re
import pandas as pd
from datetime import datetime
from datetime import date
from datetime import timedelta

def fraction_2_date(date_fraction):

        year = int(date_fraction)
        frac = date_fraction - float(int(date_fraction))
        year_length = (date(year+1, 1, 1) - date(year,1,1)).days
        return (date(year,1,1) + timedelta(days=int(frac*year_length))).strftime("%Y-%m-%d")


if '__main__' == __name__:
	import argparse

	parser = argparse.ArgumentParser()

	parser.add_argument('--nb_tree', type=int)
	parser.add_argument('--cases_file', type=str)
	parser.add_argument('--expected_p', type=float, default=0.3)
	parser.add_argument('--output_p', type=str)
	parser.add_argument('--min_date', type=float)
	parser.add_argument('--max_date', type=float)

	params = parser.parse_args()

	nb_tree = params.nb_tree
	cases_file = params.cases_file
	expected_p = params.expected_p
	output_p = params.output_p
	min_date = pd.to_datetime(fraction_2_date(params.min_date), format="%Y-%m-%d").date()
	max_date = pd.to_datetime(fraction_2_date(params.max_date), format="%Y-%m-%d").date()


	cases_df = pd.read_table(cases_file)

	cases_df['date'] = pd.to_datetime(cases_df['date'], format="%Y-%m-%d").dt.date

	cases_df_filtered_dates = cases_df.loc[(cases_df['date'] >= min_date) & (cases_df['date'] <= max_date)]

	nb_cases = int(cases_df_filtered_dates['cases'].sum())

	with open(output_p, 'w') as output :
#		output.write(str(0.5))
		if nb_cases > 0.0 and nb_tree > 0.0 : 
			if (nb_tree/nb_cases) <= 1 : 
				if ((nb_tree/nb_cases) <= 0.000001) : 
					output.write('{:f}'.format(0.000001))
				else : 
					output.write('{:f}'.format(round((nb_tree/nb_cases), 6)))
			else : 
				output.write(str(expected_p))
		else : 
			output.write(str(expected_p))
