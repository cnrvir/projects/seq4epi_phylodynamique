#!/usr/bin/env python3

import argparse
import pandas as pd
from datetime import datetime
from datetime import date
from datetime import timedelta


def find_min_max_date(file_path):
	# Read the TSV file into a DataFrame
	data = pd.read_csv(file_path, sep="\t")
	
	# Check if the 'date' column exists
	if 'date' not in data.columns:
		raise ValueError("The input file must have a 'date' column.")
	
	# Convert the 'date' column to datetime
	data['date'] = pd.to_datetime(data['date'], errors='coerce')
	
	# Drop rows with invalid dates
	data = data.dropna(subset=['date'])
	
	# Calculate the minimum and maximum dates
	max_date = data['date'].max()
	min_date = data['date'].min()
	
	return min_date.strftime("%Y-%m-%d"), max_date.strftime("%Y-%m-%d")

def main():
	# Set up argument parsing
	parser = argparse.ArgumentParser(description="Find the minimum and maximum dates in a TSV file.")
	parser.add_argument("--input_file", "-i", help="Path to the TSV file.")
	parser.add_argument("--output_file", "-o", help="Path to the output date file.")
	
	args = parser.parse_args()
	
	input_file = args.input_file
	output_file = args.output_file
	
	# Process the file
	try:
		min_date, max_date = find_min_max_date(input_file)

		with open(output_file, 'w') as output : 
			output.write (str(min_date) + "\t" + str(max_date))
			   
	except Exception as e:
		print(f"Error: {e}")

if __name__ == "__main__":
	main()