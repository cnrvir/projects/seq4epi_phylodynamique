#!/usr/bin/env python3

### Pretty basic python code to delete sequences with a sample_prop on N quantity

### Imports ###
import os, argparse, re
import pandas as pd

from datetime import datetime

def uniformize_metadata(tsv_dataframe, output_file): ## Function to find to check proportion of N in a sequence and to compare it to a user sample_prop
	
	for i in tsv_dataframe.index : 

		tsv_dataframe.at[i, "region"] = str(tsv_dataframe.at[i, "region"]).strip()
		tsv_dataframe.at[i, "strain"] = str(tsv_dataframe.at[i, "strain"]).replace(" ", "_")
		tsv_dataframe.at[i, "region"] = str(tsv_dataframe.at[i, "region"]).strip()
		tsv_dataframe.at[i, "country"] = str(tsv_dataframe.at[i, "country"]).strip()
		tsv_dataframe.at[i, "division"] = str(tsv_dataframe.at[i, "division"]).strip()
		tsv_dataframe.at[i, "location"] = str(tsv_dataframe.at[i, "location"]).strip()
		tsv_dataframe.at[i, "date"] = str(tsv_dataframe.at[i, "date"]).split()[0]
		tsv_dataframe.at[i, "date_submitted"] = str(tsv_dataframe.at[i, "date_submitted"]).split()[0]

	tsv_dataframe.to_csv(output_file, sep="\t", index = False)
				
		
if __name__ == "__main__": ##Launching of the code
	parser = argparse.ArgumentParser(description="Create a nextstrain-ready tsv file from a xls file (gisaid format)") ## initialize parser
	parser.add_argument('--tsv_file', '-m', required=True,help="Your input tsv file",type=str) #The input xls file
	parser.add_argument('--output_file', '-o', required=False,help="The output_file (default = same dir than tsv file)",type=str) #The output_file

	args = parser.parse_args() #Gather the args

	tsv_file = args.tsv_file # gather the input argument
	output_file = args.output_file

	tsv_name = os.path.basename(tsv_file).strip(".tsv") #Gather the name of the input file. CAUTION : here we supposed there is only one occurence of ".tsv" in your input filename.

	tsv_dataframe= pd.read_csv(tsv_file, sep="\t")



	if os.path.isfile(output_file) : 
		exit("error, you already have a tsv output file with the same name as your input file (keep your data safe !)")

	
	uniformize_metadata(tsv_dataframe, output_file) #launch the code !


