#! /usr/bin/env python3
# coding: utf-8

import sys, getopt
import csv
import io
import logging
import os
import re
import gzip
import lzma
import subprocess
import argparse
from datetime import datetime
from datetime import date
from datetime import timedelta


def year_fraction(date_to_modify):

	start = date(date_to_modify.year, 1, 1).toordinal()
	year_length = date(date_to_modify.year+1, 1, 1).toordinal() - start
	return date_to_modify.year + float(date_to_modify.toordinal() - start) / year_length


# This script will extract informations from the GISAID meta data
# It will convert it to augur workflow format and keep only omicron 
# sequence information
# It will finally select the omicron & contextual sequences from the fasta xz archive
def main(argv):
	parser = argparse.ArgumentParser()

	parser.add_argument('--min_date', type=str)
	parser.add_argument('--max_date', type=str)

	params = parser.parse_args()

	min_date = year_fraction(datetime.strptime(params.min_date, "%Y-%m-%d").date())
	max_date = year_fraction(datetime.strptime(params.max_date, "%Y-%m-%d").date())

	print(f"{min_date}\t{max_date}")

if __name__ == "__main__":
	logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
	main(sys.argv[1:])
