#!/usr/bin/env python3


from pastml.tree import read_forest




if '__main__' == __name__:
	import argparse

	parser = argparse.ArgumentParser()

	parser.add_argument('--input_forest', type=str)
	parser.add_argument('--output_forest', type=str)

	params = parser.parse_args()

	input_forest = params.input_forest
	output_forest = params.output_forest

	roots = read_forest(input_forest)
	
	print (roots)

	for tree in roots:
		for n in tree.traverse():
			n.dist *= 365

	with open(output_forest, 'w+') as f:
		f.write('\n'.join([root.write(format_root_node=True, format=3) for root in roots]))
	
