#!/usr/bin/env python3

### Pretty basic python code to delete sequences with a sample_prop on N quantity

### Imports ###
import os, argparse, re
import pandas as pd

from datetime import datetime


tsv_firstline = "strain\tvirus\taccession\tdate\tdate_submitted\tregion\tcountry\tdivision\tlocation\toriginating_lab\tsubmitting_lab\tage\tgender"

def create_nextstrain_tsv(xls_dataframe, output_file): ## Function to find to check proportion of N in a sequence and to compare it to a user sample_prop
		
		with open(output_file , 'a') as output_tsv_file : 

			output_tsv_file.write(tsv_firstline)

			for line in range(len(xls_dataframe["Isolate_Id"])) :

				strain = str(xls_dataframe["Isolate_Name"][line])
				if strain == "" or strain == "nan" : 
					strain = "?"

				virus = "flu"

				accession = str(xls_dataframe["Isolate_Id"][line])
				if accession == "" or accession == "nan" : 
					accession = "?"

				date = str(xls_dataframe["Collection_Date"][line])
				if date == "" or date == "nan" : 
					date = "?"
					print ("HUGE WARNING : NO DATE ?")
					continue

				date_submitted = str(xls_dataframe["Submission_Date"][line])
				if date_submitted == "" or date_submitted == "nan" : 
					date_submitted = datetime.today().strftime('%Y-%m-%d')


				location_all = ""
				location_splitted = str(xls_dataframe["Location"][line]).split("/")
				try:
					region = location_splitted[0]
					location_all += region + "\t"
				except:
					location_all += "?\t"
				
				try:
					country = location_splitted[1]
					location_all += country + "\t"  					
				except:
					location_all += "?\t"

				try:
					division = location_splitted[2]
					location_all += division + "\t"
				except:
					location_all += "?\t"

				try:
					location = location_splitted[3]
					location_all += location
				except:
					location_all += "?"

				originating_lab = str(xls_dataframe["Originating_Lab"][line])
				if originating_lab == "" or originating_lab == "nan" : 
					originating_lab = "?"

				submitting_lab = str(xls_dataframe["Submitting_Lab"][line])
				if submitting_lab == "" or submitting_lab == "nan" : 
					submitting_lab = "?"

				age = xls_dataframe["Host_Age"][line]
				if str(age) == "nan" or age == "" : 
					age = "?"
				else : 
					age = str(int(age))	


				#subclade = str(xls_dataframe["Subclade"][line])
				#if subclade == "" or subclade == "nan" : 
				#	subclade = "?"
                                
				age_unit = str(xls_dataframe["Host_Age_Unit"][line])
				if age == "?" :
					age_unit = ""
				if (age_unit == "" or age_unit == "nan") and age != "?" :
					age_unit = "Y"

				age = age + age_unit

				gender = str(xls_dataframe["Host_Gender"][line])
				if gender == "" or gender == "nan" : 
					gender = "?"

				output_tsv_file.write("\n" + strain + "\t" + virus + "\t" + accession + "\t" + date + "\t" + date_submitted + "\t" + location_all + "\t" + originating_lab + "\t" + submitting_lab + "\t" + age + "\t" + gender) 

		
if __name__ == "__main__": ##Launching of the code
	parser = argparse.ArgumentParser(description="Create a nextstrain-ready tsv file from a xls file (gisaid format)") ## initialize parser
	parser.add_argument('--xls_file', '-x', required=True,help="Your input xls file",type=str) #The input xls file
	parser.add_argument('--output_file', '-o', required=False,help="The output file",type=str) #The output_dir

	args = parser.parse_args() #Gather the args

	xls_file = args.xls_file # gather the input argument

	output_file = args.output_file # gather the input argument

	xls_dataframe= pd.read_excel(xls_file, 'Tabelle1', index_col=None)
	
	create_nextstrain_tsv(xls_dataframe, output_file) #launch the code !


