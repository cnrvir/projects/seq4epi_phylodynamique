#!/usr/bin/env python3

import argparse
import pandas as pd
import re
from datetime import datetime
from datetime import date
from datetime import timedelta


def fraction_2_date(date_fraction):

        year = int(date_fraction)
        frac = date_fraction - float(int(date_fraction))
        year_length = (date(year+1, 1, 1) - date(year,1,1)).days
        return (date(year,1,1) + timedelta(days=int(frac*year_length))).strftime("%Y-%m-%d")



def reformat_file(bdeimlfile, datefile, output_file):
    # Read the tab-separated file into a DataFrame
    bdeiml_pd = pd.read_csv(bdeimlfile, sep='\t')

    datedic = pd.read_csv(datefile, sep="\t", header=None)

    # Process columns to split confidence intervals
    for col in bdeiml_pd.columns:
        if '_CI' in col:
            base_col = col.replace('_CI', '')
            if base_col in bdeiml_pd.columns:
                # Extract low and high values from the CI column
                ci_split = bdeiml_pd[col].str.extract(r'\(([^,]+),\s*([^\)]+)\)')
                bdeiml_pd[f'{base_col}_CI_low'] = ci_split[0].astype(float)
                bdeiml_pd[f'{base_col}_CI_high'] = ci_split[1].astype(float)

    # Drop original CI columns
    bdeiml_pd = bdeiml_pd[[col for col in bdeiml_pd.columns if not col.endswith('_CI')]]

    # Add the date column
    bdeiml_pd['date'] = fraction_2_date(float(datedic[0][0]))

    bdeiml_pd.columns = bdeiml_pd.columns.str.replace(' ', '_')

    # Save the reformatted DataFrame to the output file
    bdeiml_pd.to_csv(output_file, sep='\t', index=False)

def main():
    # Initialize argument parser
    parser = argparse.ArgumentParser(description="Reformat CSV files with confidence intervals.")
    
    # Argument for input files (multiple files can be given)
    parser.add_argument("--bdeimlfile", "-t", required=True, help='CSV files to be reformatted.')
    
    # Argument for the list of dates (comma-separated, should match the number of files)
    parser.add_argument('--datefile', "-d", required=True, help='Comma-separated list of dates corresponding to the CSV files.')
    
    # Argument for specifying the output file name
    parser.add_argument('--output_file', "-o", required=True, help='Name of the output CSV file.')

    # Parse the arguments
    args = parser.parse_args()

    reformat_file(args.bdeimlfile, args.datefile, args.output_file)

if __name__ == "__main__":
    main()