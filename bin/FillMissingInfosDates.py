#!/usr/bin/env python3

import pandas as pd
import argparse

def update_dates(date_file, output_file):
	date_df = pd.read_csv(date_file, sep='\t')

	def complete_date(date):
		parts = date.split("-")
		if len(parts) == 1:  
			return f"{date}-06-15"
		elif len(parts) == 2:  
			return f"{date}-15"
		return date  

	date_df['date'] = date_df['date'].apply(complete_date)

	date_df.to_csv(output_file, sep='\t', index=False)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Update incomplete dates in a TSV file.")
	parser.add_argument("--date_file", "-i", help="Path to the input date file.")
	parser.add_argument("--output_file", "-o", help="Path to the output date file.")
	args = parser.parse_args()
	
	date_file = args.date_file
	output_file = args.output_file

	update_dates(args.date_file, output_file)
