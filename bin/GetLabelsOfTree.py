#!/usr/bin/env python3


from pastml.tree import read_forest




if '__main__' == __name__:
	import argparse

	parser = argparse.ArgumentParser()

	parser.add_argument('--input_tree', type=str)
	parser.add_argument('--output_file', type=str)

	params = parser.parse_args()

	input_tree = params.input_tree
	output_file = params.output_file

	roots = read_forest(input_tree)
	leaves = []
	for tree in roots:
		for n in tree.traverse():
			if n.is_leaf() : 
				leaves.append(str(n).replace("\n--", ""))

	with open(output_file, 'w') as output : 
		output.write("\n".join(str(leaf) for leaf in leaves))