#! /usr/bin/env python3
# coding: utf-8

import sys, getopt
import csv
import io
import logging
import os
import re
import gzip
import lzma
import subprocess
from datetime import datetime
from datetime import date
from datetime import timedelta


def year_fraction(date_to_modify):

	start = date(date_to_modify.year, 1, 1).toordinal()
	year_length = date(date_to_modify.year+1, 1, 1).toordinal() - start
	return date_to_modify.year + float(date_to_modify.toordinal() - start) / year_length


# This script will extract informations from the GISAID meta data
# It will convert it to augur workflow format and keep only omicron 
# sequence information
# It will finally select the omicron & contextual sequences from the fasta xz archive
def main(argv):
	cpus=1
	min_date="1900-01-01"
	max_date="1901-01-01"
	window_size=4.0
	window_step=1.0
	try:
		opts, args = getopt.getopt(argv,"hb:e:m:s:")
	except getopt.GetoptError:
		print('GenerateTimeWindows.py -b <YYYY-MM-DD> -e <YYY-MM-DD> -m <size of window (in month)> -s <window step (in months)>')
		sys.exit(1)
	for opt, arg in opts:
		if opt == '-h':
			print('GenerateTimeWindows.py -b <YYYY-MM-DD> -e <YYYY-MM-DD> -m <size of window (in month)> -s <window step (in months)>')
			sys.exit(0)
		elif opt in ("-b"):
			min_date = arg
		elif opt in ("-e"):
			max_date = arg
		elif opt in ("-m"):
			window_size = arg
		elif opt in ("-s"):
			window_step = arg

	min_date_frac=year_fraction(datetime.strptime(min_date, "%Y-%m-%d").date())
	max_date_frac=year_fraction(datetime.strptime(max_date, "%Y-%m-%d").date())
	window_size_frac = float(window_size) * 30.0/365.0
	step_frac = float(window_step) * 30.0/365.0
	cur_window_start = min_date_frac
	print(f"{cur_window_start}\t{cur_window_start+window_size_frac}")
	while cur_window_start+window_size_frac <= max_date_frac:
		cur_window_start = cur_window_start + step_frac
		print(f"{cur_window_start}\t{cur_window_start+window_size_frac}")

if __name__ == "__main__":
	logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
	main(sys.argv[1:])
