library(ggplot2)
library(dplyr)
library(lubridate)
#library(ggpubr)

args <- commandArgs(TRUE)

datafile <- args[1]

data <- read.table(file = datafile, header = T, sep = "\t", stringsAsFactors = F)

data$date = as.Date(data$date)


pdf("R0_plot.pdf", width = 12, height= 5)
R0_plot=ggplot(data, aes(x=date, y=R_naught)) + 
      geom_point(size=1) +
      geom_line() +
      ylim(-1,6)+
      theme_bw() +
      scale_x_date(date_breaks = "1 year", date_minor_breaks = "1 month",date_labels = "%Y")+
      theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) + 
      xlab("Date") +
      ylab("R0 value")
print(R0_plot)
dev.off()

pdf("transmission_rate_plot.pdf", width = 12, height= 5)
transmission_rate_plot=ggplot(data, aes(x=date, y=la)) + 
  geom_point(size=1) +
  geom_line() +
  ylim(0,1)+
  geom_ribbon(aes(ymin=la_CI_low, ymax=la_CI_high), linetype=2, alpha=0.1) +
  theme_bw() +
  scale_x_date(date_breaks = "1 year", date_minor_breaks = "1 month",date_labels = "%Y")+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) + 
  xlab("Date") +
  ylab("Transmission rate value")
print(transmission_rate_plot)
dev.off()

pdf("becoming_infectious_rate_plot.pdf", width = 12, height= 5)
becoming_infectious_rate_plot=ggplot(data, aes(x=date, y=mu)) + 
  geom_point(size=1) +
  geom_line() +
  ylim(0,1)+
  geom_ribbon(aes(ymin=mu_CI_low, ymax=mu_CI_high), linetype=2, alpha=0.1) +
  theme_bw() +
  scale_x_date(date_breaks = "1 year", date_minor_breaks = "1 month",date_labels = "%Y")+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) + 
  xlab("Date") +
  ylab("Becoming infectious rate value")
print(becoming_infectious_rate_plot)
dev.off()

pdf("removal_rate_plot.pdf", width = 12, height= 5)
removal_rate_plot=ggplot(data, aes(x=date, y=psi)) + 
  geom_point(size=1) +
  geom_line() +
  ylim(0,1)+
  geom_errorbar(aes(ymin = psi_CI_low, ymax = psi_CI_high)) +
  theme_bw() +
  scale_x_date(date_breaks = "1 year", date_minor_breaks = "1 month",date_labels = "%Y")+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) + 
  xlab("Date") +
  ylab("Removal rate value")
print(removal_rate_plot)
dev.off()

pdf("sampling_proportion_plot.pdf", width = 12, height= 5)
sampling_proportion_plot=ggplot(data, aes(x=date, y=p)) + 
  geom_point(size=1) +
  geom_line() +
  ylim(0,1)+
  geom_ribbon(aes(ymin=p_CI_low, ymax=p_CI_high), linetype=2, alpha=0.1) +
  theme_bw() +
  scale_x_date(date_breaks = "1 year", date_minor_breaks = "1 month",date_labels = "%Y")+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) + 
  xlab("Date") +
  ylab("Sampling proportion value")
print(sampling_proportion_plot)
dev.off()

pdf("incubation_period_plot.pdf", width = 12, height= 5)
incubation_period_plot=ggplot(data, aes(x=date, y=incubation_period)) + 
  geom_point(size=1) +
  geom_line() +
  ylim(0,200)+
  theme_bw() +
  scale_x_date(date_breaks = "1 year", date_minor_breaks = "1 month",date_labels = "%Y")+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) + 
  xlab("Date") +
  ylab("Incubation period value")
print(incubation_period_plot)
dev.off()

pdf("infectious_time_plot.pdf", width = 12, height= 5)
infectious_time_plot=ggplot(data, aes(x=date, y=infectious_time)) + 
  geom_point(size=1) +
  geom_line() +
  ylim(-1,6)+
  theme_bw() +
  scale_x_date(date_breaks = "1 year", date_minor_breaks = "1 month",date_labels = "%Y")+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) + 
  xlab("Date") +
  ylab("Infectious time value")
print(infectious_time_plot)
dev.off()

#pdf("epidemic_parameters.pdf", width = 12, height= 50)
#epidemic_parameters = ggarrange(R0_plot, sampling_proportion_plot,transmission_rate_plot, becoming_infectious_rate_plot, incubation_period_plot,  ncol = 1, nrow = 5)
#print(epidemic_parameters)
#dev.off()

