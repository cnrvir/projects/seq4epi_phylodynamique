#!/usr/bin/env python3

### Pretty basic python code to delete sequences with a sample_prop on N quantity

### Imports ###
import os, argparse, re
import pandas as pd

from datetime import datetime
from Bio import Align
from Bio import GenBank
from Bio import SeqIO



def parse_genbank(gisaid_data, region_dataframe, output_fasta_a, output_metadata_a, output_fasta_b, output_metadata_b): ## Function to find to check proportion of N in a sequence and to compare it to a user sample_prop

	tsv_firstline = "strain\tvirus\taccession\tgenbank_accession_rev\tdate\tregion\tcountry\tdivision\tlocation\thost\tdate_submitted\tsra_accession\tabbr_authors\treverse\tauthors\tinstitution"
	date_patterns = ["%Y", "%Y-%m-%d", '%d-%b-%Y', "'%d %b %Y'"]


	with open(output_metadata_a, "w") as outputm : 
		outputm.write(tsv_firstline)
	with open(output_metadata_b, "w") as outputm : 
		outputm.write(tsv_firstline)


	gisaid_data = SeqIO.parse(gisaid_data, "fasta")

	dic_a = {}
	dic_b = {}

	for record in gisaid_data : 

		record_id = record.id
		record_sequence = record.seq

		record_id_list = record_id.split("/")

		virus = "RSV" + record_id_list[1]
		country = record_id_list[2].replace ("_", " ")

		if country in list(region_dataframe.loc[:, 'country']) : 
			region = str(region_dataframe.loc[region_dataframe['country'] == country, 'region'].iloc[0])
		else : 
			region = "?"

		record_id_name = record_id.split("|")

		strain = record_id_name[0]
		accession = record_id_name[1]
		date = record_id_name[-1]

		for pattern in date_patterns : 
			try : 
				date = datetime.strptime(date, pattern).date().strftime('%Y-%m-%d')
			except : 
				pass

		genbank_accession_rev = "?"

		date_submitted = datetime.today().strftime('%Y-%m-%d')
		division = "?"
		location="?"
		host="Homo sapiens"
		sra_accession="?"
		abbr_authors="?"
		reverse="?"
		authors="?"
		institution="GISAID"

		if virus == "RSVA": 
			if strain not in dic_a : 
				dic_a[strain] = {}
				dic_a[strain]["sequence"] = record_sequence
				dic_a[strain]["strain"] = strain
				dic_a[strain]["virus"] = virus
				dic_a[strain]["accession"] = accession
				dic_a[strain]["genbank_accession_rev"] = genbank_accession_rev
				dic_a[strain]["date"] = date
				dic_a[strain]["region"] = region
				dic_a[strain]["country"] = country
				dic_a[strain]["division"] = division
				dic_a[strain]["location"] = location
				dic_a[strain]["host"] = host
				dic_a[strain]["date_submitted"] = date_submitted
				dic_a[strain]["sra_accession"] = sra_accession
				dic_a[strain]["abbr_authors"] = abbr_authors
				dic_a[strain]["reverse"] = reverse
				dic_a[strain]["authors"] = authors
				dic_a[strain]["institution"] = institution

		if virus == "RSVB": 
			if strain not in dic_b : 
				dic_b[strain] = {}
				dic_b[strain]["sequence"] = record_sequence
				dic_b[strain]["strain"] = strain
				dic_b[strain]["virus"] = virus
				dic_b[strain]["accession"] = accession
				dic_b[strain]["genbank_accession_rev"] = genbank_accession_rev
				dic_b[strain]["date"] = date
				dic_b[strain]["region"] = region
				dic_b[strain]["country"] = country
				dic_b[strain]["division"] = division
				dic_b[strain]["location"] = location
				dic_b[strain]["host"] = host
				dic_b[strain]["date_submitted"] = date_submitted
				dic_b[strain]["sra_accession"] = sra_accession
				dic_b[strain]["abbr_authors"] = abbr_authors
				dic_b[strain]["reverse"] = reverse
				dic_b[strain]["authors"] = authors
				dic_b[strain]["institution"] = institution

	with open(output_fasta_a, 'w') as output: 

		for strain in dic_a: 
			output.write(">" + str(dic_a[strain]["strain"]) + "\n" + str(dic_a[strain]["sequence"]) + "\n")
			
	with open(output_fasta_b, 'w') as output: 
			
		for strain in dic_b: 
			output.write(">" + str(dic_b[strain]["strain"]) + "\n" + str(dic_b[strain]["sequence"]) + "\n")
			
	with open(output_metadata_a, 'w') as output: 

		output.write(tsv_firstline)
		for strain in dic_a:
			output.write("\n" + str(dic_a[strain]["strain"]) + "\t" + str(dic_a[strain]["virus"]) + "\t" + str(dic_a[strain]["accession"]) + "\t" + str(dic_a[strain]["genbank_accession_rev"]) + "\t" + str(dic_a[strain]["date"]) + "\t" + str(dic_a[strain]["region"]) + "\t" + str(dic_a[strain]["country"]) + "\t" + str(dic_a[strain]["division"]) + "\t" + str(dic_a[strain]["location"]) + "\t" + str(dic_a[strain]["host"]) + "\t" + str(dic_a[strain]["date_submitted"]) + "\t" + str(dic_a[strain]["sra_accession"]) + "\t" + str(dic_a[strain]["abbr_authors"]) + "\t" + str(dic_a[strain]["reverse"]) + "\t" + str(dic_a[strain]["authors"]) + "\t" + str(dic_a[strain]["institution"]))
			
	with open(output_metadata_b, 'w') as output: 
				
		output.write(tsv_firstline)
		for strain in dic_b:
			output.write("\n" + str(dic_b[strain]["strain"]) + "\t" + str(dic_b[strain]["virus"]) + "\t" + str(dic_b[strain]["accession"]) + "\t" + str(dic_b[strain]["genbank_accession_rev"]) + "\t" + str(dic_b[strain]["date"]) + "\t" + str(dic_b[strain]["region"]) + "\t" + str(dic_b[strain]["country"]) + "\t" + str(dic_b[strain]["division"]) + "\t" + str(dic_b[strain]["location"]) + "\t" + str(dic_b[strain]["host"]) + "\t" + str(dic_b[strain]["date_submitted"]) + "\t" + str(dic_b[strain]["sra_accession"]) + "\t" + str(dic_b[strain]["abbr_authors"]) + "\t" + str(dic_b[strain]["reverse"]) + "\t" + str(dic_b[strain]["authors"]) + "\t" + str(dic_b[strain]["institution"]))


if __name__ == "__main__": ##Launching of the code
	parser = argparse.ArgumentParser(description="Create a nextstrain-ready tsv file from a genbank file (gisaid format)") ## initialize parser
	parser.add_argument('--gisaid_file', '-g', required=True,help="Your input genbank file",type=str) #The input genbank file
	parser.add_argument('--region_file', '-r', required=True,help="Region file",type=str) #The input genbank file
	parser.add_argument('--output_fasta_a', '-o', required=False,help="The output sequences",type=str) #The output_dir
	parser.add_argument('--output_metadata_a', '-u', required=False,help="The output metadata",type=str) #The output_dir
	parser.add_argument('--output_fasta_b', '-s', required=False,help="The output sequences",type=str) #The output_dir
	parser.add_argument('--output_metadata_b', '-t', required=False,help="The output metadata",type=str) #The output_dir
	args = parser.parse_args() #Gather the args

	gisaid_data = open(args.gisaid_file, "r") # gather the input argument

	region_dataframe = pd.read_table(open(args.region_file, "r"), sep="\t")

	output_fasta_a = args.output_fasta_a # gather the output argument
	output_metadata_a = args.output_metadata_a # gather the output argument
	output_fasta_b = args.output_fasta_b # gather the output argument
	output_metadata_b = args.output_metadata_b # gather the output argument

	parse_genbank(gisaid_data, region_dataframe, output_fasta_a, output_metadata_a, output_fasta_b, output_metadata_b) #launch the code !


