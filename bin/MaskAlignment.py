#! /usr/bin/env python3
# coding: utf-8

import argparse
import Bio
import Bio.SeqIO
from Bio.Seq import Seq

def mask_terminal_gaps(seq):
    len_seq = len(seq)
    trim_seq = seq.lstrip('-')
    len_left_gaps = len_seq - len(trim_seq)
    trim_seq = trim_seq.rstrip('-')
    len_right_gaps = len_seq - len_left_gaps - len(trim_seq)
    seq = "N"*len_left_gaps + trim_seq + "N"*len_right_gaps
    return seq


def mask_sites(seq,sites):
    '''sites : string; sites whitespace separated'''
    sites = [int(float(site)) for site in sites]
    sites.sort()
    tmp = 0
    new_seq = ""
    for site in sites:
        site = int(site)
        new_seq += seq[tmp:site-1]+"N"
        tmp = site
    new_seq += seq[tmp:]
    return new_seq


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Mask initial bases from alignment FASTA",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("--alignment", required=True, help="FASTA file of alignment")
    parser.add_argument("--mask-terminal-gaps", action='store_true', help="fill all terminal gaps with N as they likely represent missing data")
    parser.add_argument("--mask-from-beginning", type = int, required=True, help="number of bases to mask from start")
    parser.add_argument("--mask-from-end", type = int, help="number of bases to mask from end")
    parser.add_argument("--mask-sites", nargs='+', help="list of sites to mask")
    parser.add_argument("--output", required=True, help="FASTA file of output alignment")
    args = parser.parse_args()

    begin_length = 0
    if args.mask_from_beginning:
        begin_length = args.mask_from_beginning
    end_length = 0
    if args.mask_from_end:
        end_length = args.mask_from_end


    with open(args.output, 'w') as outfile:
        for record in Bio.SeqIO.parse(args.alignment, 'fasta'):
            seq = str(record.seq)
            if args.mask_terminal_gaps:
                seq = mask_terminal_gaps(seq)

            start = "N" * begin_length
            middle = seq[begin_length:-end_length]
            end = "N" * end_length
            seq = start + middle + end
            if args.mask_sites:
                sites_to_mask = args.mask_sites
                new_seq = mask_sites(seq,sites_to_mask)
            record.seq = Seq(new_seq)
            Bio.SeqIO.write(record, outfile, 'fasta')
