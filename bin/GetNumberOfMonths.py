#! /usr/bin/env python3
# coding: utf-8

import pandas as pd
from datetime import datetime
import argparse

def calculate_month_difference(tmrca, datefile):
    # Convert numerical date to datetime object
    year = int(tmrca)
    fractional_year = tmrca - year
    month = int(fractional_year * 12) + 1  # Convert fractional year to month
    input_date = datetime(year, month, 1)
    
    # Load data and parse dates in the second column
    dataframe_dates = pd.read_csv(datefile, sep='\t')
    dataframe_dates["date"] = pd.to_datetime(dataframe_dates["date"])  # Ensure dates are parsed correctly
    
    # Get the latest date from the second column
    latest_date = dataframe_dates["date"].max()
    
    # Calculate the difference in years and months manually
    year_diff = input_date.year - latest_date.year
    month_diff = input_date.month - latest_date.month
    nb_of_months = abs(year_diff * 12 + month_diff)
    
    # Ensure minimum difference is 3
    nb_of_months = max(nb_of_months, 3)
    
    return nb_of_months

def main():
    # Setup argument parser
    parser = argparse.ArgumentParser(description="Calculate month difference between a numerical date and latest date in a file")
    parser.add_argument("--tmrca", "-t", type=float, help="Numerical date in format YYYY.FFF (e.g., 2019.541)", required=True)
    parser.add_argument("--datefile", "-d", type=str, help="Path to the tab-separated file containing dates in the second column", required=True)
    
    args = parser.parse_args()
    
    # Calculate the months difference
    nb_of_months = calculate_month_difference(args.tmrca, args.datefile)
    print(nb_of_months)

if __name__ == "__main__":
    main()
