include {PrepareDatasetDateAndCountry} from '../../modules/general-virus/PrepareDataset.nf'
include {FilterRandom} from '../../modules/general-virus/PrepareDataset.nf'
include {FilterNext} from '../../modules/general-virus/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesFirst} from '../../modules/general-virus/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesSecond} from '../../modules/general-virus/PrepareDataset.nf'
include {RemoveForbiddenCharSeq} from '../../modules/general-virus/PrepareDataset.nf'
include {RemoveForbiddenCharMetadata} from '../../modules/general-virus/PrepareDataset.nf'
include {GrepSequences} from '../../modules/general-virus/PrepareDataset.nf'
include {AddWuhan} from '../../modules/general-virus/PrepareDataset.nf'

workflow CREATE_REF_DATASET{

    take:
        fasta
        metadata
        country
        nb_max
        sample_min_date
        sample_max_date
        random_sampling
        fasta_wuhan
        metadata_wuhan

    main: 
        prepared_dataset = PrepareDatasetDateAndCountry(fasta, metadata, country, sample_min_date, sample_max_date)
        prepared_sequences = prepared_dataset[0]
        prepared_metadata = prepared_dataset[1]

        rmdup_dataset = RemoveDuplicatesFirst(prepared_sequences, prepared_metadata)
        rmdup_sequences = rmdup_dataset[0]
        rmdup_metadata = rmdup_dataset[1]

        if( random_sampling.equals("yes") || random_sampling.equals("Yes") || random_sampling.equals("YES") || random_sampling.equals("Y") || random_sampling.equals("y") ) {
            filtered_metadata = FilterRandom(rmdup_metadata, nb_max)
        }
        else {
            filtered_metadata = FilterNext(rmdup_metadata, country, sample_min_date, sample_max_date, nb_max)
        }
        
        filtered_fasta = GrepSequences(rmdup_sequences, filtered_metadata)

        dataset_w_wuhan = AddWuhan(filtered_fasta, fasta_wuhan, filtered_metadata, metadata_wuhan)
        dataset_ready = RemoveDuplicatesSecond(RemoveForbiddenCharSeq(dataset_w_wuhan[0]), RemoveForbiddenCharMetadata(dataset_w_wuhan[1]))

    emit: 
        dataset_ready[0]
        dataset_ready[1]

}