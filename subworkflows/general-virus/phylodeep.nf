//Utils
include {EstimateSamplingProportionPhylodeep} from '../../modules/general-virus/Utils.nf'
include {ConvertTimeWindowToFraction} from '../../modules/general-virus/Utils.nf'

//Create and manipulate tree
include {CutTreeDate} from '../../modules/general-virus/TreeManip.nf'
include {RescaleTreeToDays} from '../../modules/general-virus/TreeManip.nf'
include {GetLabelsFromTree} from '../../modules/general-virus/TreeManip.nf'
include {ReformatTree} from '../../modules/general-virus/TreeManip.nf'

//Perform Deep Learning Analysis
include {PhylodeepAnalysis} from '../../modules/general-virus/PhylodeepAnalysis.nf'


workflow PHYLODEEP_ANALYSIS{

    take:
        treefile_resolved
        sample_min_date
        sample_max_date
        nb_cases_file
        expected_sampling_proportion

    main: 

        treefile_reformated = ReformatTree(treefile_resolved)
        tree_labels = GetLabelsFromTree(treefile_reformated)

        timewindow = ConvertTimeWindowToFraction(sample_min_date,sample_max_date)
        sampling_proportion_analysis = EstimateSamplingProportionPhylodeep(timewindow, tree_labels, nb_cases_file, expected_sampling_proportion) // we use the tree_label as the metadata here since only the number of lines (e.g, the number of individuals) is of interest

        phylodeep_results = PhylodeepAnalysis(treefile_resolved, sampling_proportion_analysis)

    emit: 
        treefile_reformated
        timewindow
        sampling_proportion_analysis
        phylodeep_results
}
