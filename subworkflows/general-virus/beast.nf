//Create and manipulate tree
include {RemoveAnnotations} from '../../modules/general-virus/TreeManip.nf'
include {GetLabelsFromTree} from '../../modules/general-virus/TreeManip.nf'
include {GetDatesFromLabels} from '../../modules/general-virus/TreeManip.nf'
include {GetTreeHeight} from '../../modules/general-virus/TreeManip.nf'

//Perform Bayesian Analysis
include {CovFlow} from '../../modules/general-virus/Beast.nf'
include {GetNumberOfMonths} from '../../modules/general-virus/Beast.nf'
include {RunBeastBDSky} from '../../modules/general-virus/Beast.nf'
include {RunBeastCoalSky} from '../../modules/general-virus/Beast.nf'
include {GetGraphsBDsky} from '../../modules/general-virus/Beast.nf'

workflow BEAST_ANALYSIS{

    take: 
        treefile_resolved
        tMRCA
        dates_ready
        
    main: 
        tree_height = GetTreeHeight (treefile_resolved)
        treefile_wo_annotations = RemoveAnnotations(treefile_resolved)
        tree_labels = GetLabelsFromTree(treefile_wo_annotations) // This step is performed to get the labels of the tree and grep them, in the next function, in the datefile. It is performed like this because lsd2 delete outliers and stdout the outliers without putting them in a file.
        dates_ready_wo_outliers = GetDatesFromLabels(tree_labels, dates_ready)
        
        nb_of_dimensions = GetNumberOfMonths(dates_ready_wo_outliers, tMRCA) //Here, we create n windows (dimensions) following the number of months between the supposed origin and the latest date in our samples.

        covflow_files = CovFlow(treefile_wo_annotations, dates_ready_wo_outliers, tree_height, nb_of_dimensions)

        bdsky_file = covflow_files[0]
        coalsky_file = covflow_files[1]
        bdsky_results = RunBeastBDSky(bdsky_file)
        coalsky_results = RunBeastCoalSky (coalsky_file)
        graphs_bdsky = GetGraphsBDsky(bdsky_results, dates_ready_wo_outliers)

    emit: 
        bdsky_file
        coalsky_file
        bdsky_results
        coalsky_results
        graphs_bdsky
        
}