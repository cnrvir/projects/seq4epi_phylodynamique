//Utils
include {GetMinMaxDate} from '../../modules/flu/Utils.nf'
include {EstimateSamplingProportion} from '../../modules/flu/Utils.nf'


//Create and manipulate tree
include {RemoveAnnotations} from '../../modules/flu/TreeManip.nf'
include {GetLabelsFromTree} from '../../modules/flu/TreeManip.nf'
include {GetLabelsFromForest} from '../../modules/flu/TreeManip.nf'
include {GetDatesFromLabels} from '../../modules/flu/TreeManip.nf'
include {ReformatTree} from '../../modules/flu/TreeManip.nf'
include {CutTreeDate} from '../../modules/flu/TreeManip.nf'
include {RescaleTreeToDays} from '../../modules/flu/TreeManip.nf'

//Perform ML Analysis
include {GenerateTimeWindows } from '../../modules/flu/MLAnalysis'
include {PerformBDMLAnalysis} from '../../modules/flu/MLAnalysis.nf'
include {PerformBDEIMLAnalysis} from '../../modules/flu/MLAnalysis.nf'
include {ReformatBDMLOutput} from '../../modules/flu/MLAnalysis.nf'
include {MergeBDMLFiles} from '../../modules/flu/MLAnalysis.nf'
include {GetGraphsBDML} from '../../modules/flu/MLAnalysis.nf'
include {ReformatBDEIMLOutput} from '../../modules/flu/MLAnalysis.nf'
include {MergeBDEIMLFiles} from '../../modules/flu/MLAnalysis.nf'
include {GetGraphsBDEIML} from '../../modules/flu/MLAnalysis.nf'


//ML parameters
params.window_step="1"
params.window_size="4"

workflow ML_ANALYSIS{


    take: 
        treefile_resolved
        dataset_ready_w_dates
        expected_sampling_proportion

    main: 
        window_size = params.window_size
        window_step = params.window_step

        tree_labels = GetLabelsFromTree(RemoveAnnotations(treefile_resolved))
        dates_wo_outliers = GetDatesFromLabels(tree_labels.combine(dataset_ready_w_dates, by:0))
        min_and_max_dates = GetMinMaxDate(dates_wo_outliers)
        time_windows = GenerateTimeWindows(min_and_max_dates, window_size, window_step).transpose()

        treefile_reformated = ReformatTree(treefile_resolved)
        tree_cut = CutTreeDate(treefile_reformated.combine(time_windows, by: 0))
        tree_cut_filter = tree_cut.filter{ it -> it[2].size() > 0 }
        tree_rescaled = RescaleTreeToDays(tree_cut_filter)
        forest_cut_labels = GetLabelsFromForest(tree_cut_filter)

      //  sampling_proportion_analysis = EstimateSamplingProportion(forest_cut_labels, nb_cases_file, expected_sampling_proportion)
        sampling_proportion_analysis = EstimateSamplingProportion(forest_cut_labels, expected_sampling_proportion) // Right now, the calculation of sampling proportion has been stopped since we don't have any nb_cases_file for flu

        bdml_analysis = PerformBDMLAnalysis(tree_rescaled.join(sampling_proportion_analysis, by: [0,1]))
        bdeiml_analysis = PerformBDEIMLAnalysis(tree_rescaled.join(sampling_proportion_analysis, by: [0,1]))

        time_windows_name = time_windows.map{type, it->[type, it.baseName, it]}
        
        reformated_bdml = MergeBDMLFiles(ReformatBDMLOutput(bdml_analysis.join(time_windows_name, by: [0,1])).groupTuple()) // reformated_bdml = bdml_analysis.join(time_windows_name, by: [0,1]).groupTuple().each { type, group -> ReformatBDMLOutput(group.collect { [type] + it }).collectFile(name: "bdml_output_${type}.tsv", storeDir: "${params.results}/02a_BDML_estimation/${type}", keepHeader: true)}
        reformated_bdeiml = MergeBDEIMLFiles(ReformatBDEIMLOutput(bdeiml_analysis.join(time_windows_name, by: [0,1])).groupTuple()) // reformated_bdml = bdml_analysis.join(time_windows_name, by: [0,1]).groupTuple().each { type, group -> ReformatBDMLOutput(group.collect { [type] + it }).collectFile(name: "bdml_output_${type}.tsv", storeDir: "${params.results}/02a_BDML_estimation/${type}", keepHeader: true)}

        bdml_graphs = GetGraphsBDML(reformated_bdml)
        bdeiml_graphs = GetGraphsBDEIML(reformated_bdeiml)

    emit: 
        reformated_bdml
        reformated_bdeiml
        bdeiml_graphs
        bdeiml_graphs

}