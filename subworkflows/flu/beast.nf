//Create and manipulate tree
include {RemoveAnnotations} from '../../modules/flu/TreeManip.nf'
include {GetLabelsFromTree} from '../../modules/flu/TreeManip.nf'
include {GetDatesFromLabels} from '../../modules/flu/TreeManip.nf'
include {GetTreeHeight} from '../../modules/flu/TreeManip.nf'

//Perform Bayesian Analysis
include {CovFlow} from '../../modules/flu/Beast.nf'
include {GetNumberOfMonths} from '../../modules/flu/Beast.nf'
include {RunBeastBDSky} from '../../modules/flu/Beast.nf'
include {RunBeastCoalSky} from '../../modules/flu/Beast.nf'
include {GetGraphsBDsky} from '../../modules/flu/Beast.nf'

workflow BEAST_ANALYSIS{

    take: 
        treefile_resolved
        tMRCA
        dataset_ready_w_dates
        
    main: 
        tree_height = GetTreeHeight (treefile_resolved)
        treefile_wo_annotations = RemoveAnnotations(treefile_resolved)
        tree_labels = GetLabelsFromTree(treefile_wo_annotations)
        dates_wo_outliers = GetDatesFromLabels(tree_labels.combine(dataset_ready_w_dates, by:0))
        
        nb_of_dimensions = GetNumberOfMonths(dates_wo_outliers.combine(tMRCA, by:0)) //Here, we create n windows (dimensions) following the number of months between the supposed origin and the latest date in our samples.

        covflow_files = CovFlow(treefile_wo_annotations.combine(dates_wo_outliers, by:0).combine(tree_height, by:0).combine(nb_of_dimensions, by:0))

        bdsky_file = covflow_files[0]
        coalsky_file = covflow_files[1]
        bdsky_results = RunBeastBDSky(bdsky_file)
        coalsky_results = RunBeastCoalSky (coalsky_file)
        graphs_bdsky = GetGraphsBDsky(bdsky_results.combine(dates_wo_outliers, by: 0))

    emit: 
        bdsky_file
        coalsky_file
        bdsky_results
        coalsky_results
        graphs_bdsky
        
}