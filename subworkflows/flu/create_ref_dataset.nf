include {ParseMetadata} from '../../modules/flu/PrepareDataset.nf'
include {ParseSequences} from '../../modules/flu/PrepareDataset.nf'
include {RemoveForbiddenChar} from '../../modules/flu/PrepareDataset.nf'
include {FilterRandom} from '../../modules/flu/PrepareDataset.nf'
include {FilterNext} from '../../modules/flu/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesFirst} from '../../modules/flu/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesSecond} from '../../modules/flu/PrepareDataset.nf'
include {GrepSequences} from '../../modules/flu/PrepareDataset.nf'
include {AddReference} from '../../modules/flu/PrepareDataset.nf'

workflow CREATE_REF_DATASET{

    take:
        input_files
        country
        nb_max
        sample_min_date
        sample_max_date
        random_sampling
        flu_refs

    main: 

        dataset = RemoveDuplicatesFirst(ParseSequences(ParseMetadata(input_files)))

        if (random_sampling.equals("yes") || random_sampling.equals("Yes") || random_sampling.equals("YES") || random_sampling.equals("Y") || random_sampling.equals("y")) {
            filtered_dataset = FilterRandom(dataset, nb_max)
        }
        else {
            filtered_dataset = FilterNext(dataset, country, sample_min_date, sample_max_date, nb_max)
        }
        
        filtered_dataset = GrepSequences(filtered_dataset)
        dataset_w_refs = AddReference(filtered_dataset, flu_refs)   
        dataset_ready = RemoveDuplicatesSecond(RemoveForbiddenChar(dataset_w_refs))

    emit: 
        dataset_ready
        
}