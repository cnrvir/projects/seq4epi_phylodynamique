//Utils
include {EstimateSamplingProportionPhylodeep} from '../../modules/rsv/Utils.nf'
include {ConvertTimeWindowToFraction} from '../../modules/rsv/Utils.nf'

//Create and manipulate tree
include {CutTreeDate} from '../../modules/rsv/TreeManip.nf'
include {RescaleTreeToDays} from '../../modules/rsv/TreeManip.nf'
include {GetLabelsFromTree} from '../../modules/rsv/TreeManip.nf'
include {ReformatTree} from '../../modules/rsv/TreeManip.nf'

//Perform Deep Learning Analysis
include {PhylodeepAnalysis} from '../../modules/rsv/PhylodeepAnalysis.nf'


workflow PHYLODEEP_ANALYSIS{

    take:
        treefile_resolved
        sample_min_date
        sample_max_date
        expected_sampling_proportion    
        //Find a way to have an expected proportion analysis ?
    
    main: 
        treefile_reformated = ReformatTree(treefile_resolved)
        tree_labels = GetLabelsFromTree(treefile_reformated)

        timewindow = ConvertTimeWindowToFraction(sample_min_date,sample_max_date)
        sampling_proportion_analysis = EstimateSamplingProportionPhylodeep(timewindow, tree_labels, expected_sampling_proportion) // we use the tree_label as the metadata here since only the number of lines (e.g, the number of individuals) is of interest
       // sampling_proportion_analysis = EstimateSamplingProportionPhylodeep(timewindow, tree_labels, nb_cases_file, expected_sampling_proportion) // we use the tree_label as the metadata here since only the number of lines (e.g, the number of individuals) is of interest
        

        phylodeep_results = PhylodeepAnalysis(treefile_resolved.combine(sampling_proportion_analysis, by: 0)) // Right now, expected sampling proportion, can't calculate sampling prop

    emit: 
        treefile_reformated
        timewindow
        expected_sampling_proportion //To replace with sampling_proportion_analysis
        phylodeep_results

}
