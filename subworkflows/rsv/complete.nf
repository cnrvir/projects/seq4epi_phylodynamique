//Utils
include {GetMinMaxDate} from '../../modules/rsv/Utils.nf'
include {EstimateSamplingProportionPhylodeep} from '../../modules/rsv/Utils.nf'
include {EstimateSamplingProportion} from '../../modules/rsv/Utils.nf'
include {ConvertTimeWindowToFraction} from '../../modules/rsv/Utils.nf'

//Create and manipulate tree
include {CutTreeDate} from '../../modules/rsv/TreeManip.nf'
include {RescaleTreeToDays} from '../../modules/rsv/TreeManip.nf'
include {GetLabelsFromTree} from '../../modules/rsv/TreeManip.nf'
include {ReformatTree} from '../../modules/rsv/TreeManip.nf'
include {RemoveAnnotations} from '../../modules/rsv/TreeManip.nf'
include {GetDatesFromLabels} from '../../modules/rsv/TreeManip.nf'
include {GetTreeHeight} from '../../modules/rsv/TreeManip.nf'
include {GetLabelsFromForest} from '../../modules/rsv/TreeManip.nf'

//Perform Deep Learning Analysis
include {PhylodeepAnalysis} from '../../modules/rsv/CompleteAnalysis.nf'

//Perform ML Analysis
include {GenerateTimeWindows } from '../../modules/rsv/MLAnalysis'
include {PerformBDMLAnalysis} from '../../modules/rsv/CompleteAnalysis.nf'
include {PerformBDEIMLAnalysis} from '../../modules/rsv/CompleteAnalysis.nf'
include {ReformatBDMLOutput} from '../../modules/rsv/MLAnalysis.nf'
include {MergeBDMLFiles} from '../../modules/rsv/MLAnalysis.nf'
include {GetGraphsBDML} from '../../modules/rsv/MLAnalysis.nf'
include {ReformatBDEIMLOutput} from '../../modules/rsv/MLAnalysis.nf'
include {MergeBDEIMLFiles} from '../../modules/rsv/MLAnalysis.nf'
include {GetGraphsBDEIML} from '../../modules/rsv/MLAnalysis.nf'

//Perform Bayesian Analysis
include {CovFlow} from '../../modules/rsv/CompleteAnalysis.nf'
include {GetNumberOfMonths} from '../../modules/rsv/Beast.nf'
include {RunBeastBDSky} from '../../modules/rsv/Beast.nf'
include {RunBeastCoalSky} from '../../modules/rsv/Beast.nf'
include {GetGraphsBDsky} from '../../modules/rsv/Beast.nf'

//ML parameters
params.window_step="1"
params.window_size="4"

workflow COMPLETE_ANALYSIS{

    take:
        treefile_resolved
        tMRCA
        dataset_ready_w_dates
        sample_min_date
        sample_max_date
        expected_sampling_proportion
        //Find a way to have an expected proportion analysis ?
    
    main: 
        // Global operations on tree (and other). Some analyses are shared inbetween Beast, ML and phylodeep
        treefile_reformated = ReformatTree(treefile_resolved)
        tree_labels = GetLabelsFromTree(treefile_reformated)
        dates_wo_outliers = GetDatesFromLabels(tree_labels.combine(dataset_ready_w_dates, by:0))
        min_and_max_dates = GetMinMaxDate(dates_wo_outliers)

        // Phylodeep part of the analysis
        time_window_phylodeep = ConvertTimeWindowToFraction(sample_min_date,sample_max_date)
        sampling_proportion_analysis_phylodeep = EstimateSamplingProportionPhylodeep(time_window_phylodeep, tree_labels, expected_sampling_proportion) // we use the tree_label as the metadata here since only the number of lines (e.g, the number of individuals) is of interest
       // sampling_proportion_analysis = EstimateSamplingProportionPhylodeep(timewindow, tree_labels, nb_cases_file, expected_sampling_proportion) // we use the tree_label as the metadata here since only the number of lines (e.g, the number of individuals) is of interest

        phylodeep_analysis = PhylodeepAnalysis(treefile_resolved.combine(sampling_proportion_analysis_phylodeep, by: 0)) // Right now, expected sampling proportion, can't calculate sampling prop
        phylodeep_results = phylodeep_analysis[0]
        
        //Results of phylodeep that will be used by ML and Beast analyses
        params_for_beast = phylodeep_analysis[1]
        params_for_bdml = phylodeep_analysis[2]
        params_for_bdeiml = phylodeep_analysis[3]


        //ML analysis specific tasks

        //parameters
        window_size = params.window_size
        window_step = params.window_step
        time_windows_ML = GenerateTimeWindows(min_and_max_dates, window_size, window_step).transpose()


        tree_cut = CutTreeDate(treefile_reformated.combine(time_windows_ML, by: 0))
        tree_cut_filter = tree_cut.filter{ it -> it[2].size() > 0 }
        tree_rescaled_ML = RescaleTreeToDays(tree_cut_filter)
        forest_cut_labels = GetLabelsFromForest(tree_cut_filter)
        sampling_proportion_analysis_ML = EstimateSamplingProportion(forest_cut_labels, expected_sampling_proportion) // Right now, the calculation of sampling proportion has been stopped since we don't have any nb_cases_file for rsv

        bdml_analysis = PerformBDMLAnalysis(tree_rescaled_ML.join(sampling_proportion_analysis_ML, by: [0,1]).combine(params_for_bdml, by: 0))
        bdeiml_analysis = PerformBDEIMLAnalysis(tree_rescaled_ML.join(sampling_proportion_analysis_ML, by: [0,1]).combine(params_for_bdeiml, by: 0))

        time_windows_name_ML = time_windows_ML.map{type, it->[type, it.baseName, it]}
        
        reformated_bdml = MergeBDMLFiles(ReformatBDMLOutput(bdml_analysis.join(time_windows_name_ML, by: [0,1])).groupTuple()) // reformated_bdml = bdml_analysis.join(time_windows_name, by: [0,1]).groupTuple().each { type, group -> ReformatBDMLOutput(group.collect { [type] + it }).collectFile(name: "bdml_output_${type}.tsv", storeDir: "${params.results}/02a_BDML_estimation/${type}", keepHeader: true)}
        reformated_bdeiml = MergeBDEIMLFiles(ReformatBDEIMLOutput(bdeiml_analysis.join(time_windows_name_ML, by: [0,1])).groupTuple()) // reformated_bdml = bdml_analysis.join(time_windows_name, by: [0,1]).groupTuple().each { type, group -> ReformatBDMLOutput(group.collect { [type] + it }).collectFile(name: "bdml_output_${type}.tsv", storeDir: "${params.results}/02a_BDML_estimation/${type}", keepHeader: true)}

        bdml_graphs = GetGraphsBDML(reformated_bdml)
        bdeiml_graphs = GetGraphsBDEIML(reformated_bdeiml)

        //BEAST analysis specific tasks
        tree_height = GetTreeHeight (treefile_resolved)
        treefile_wo_annotations = RemoveAnnotations(treefile_resolved)
        nb_of_dimensions = GetNumberOfMonths(dates_wo_outliers.combine(tMRCA, by:0)) //Here, we create n windows (dimensions) following the number of months between the supposed origin and the latest date in our samples.
        covflow_files = CovFlow(treefile_wo_annotations.combine(dates_wo_outliers, by:0).combine(tree_height, by:0).combine(nb_of_dimensions, by:0).combine(params_for_beast, by: 0))
        bdsky_file = covflow_files[0]
        coalsky_file = covflow_files[1]
        bdsky_results = RunBeastBDSky(bdsky_file)
        coalsky_results = RunBeastCoalSky (coalsky_file)
        graphs_bdsky = GetGraphsBDsky(bdsky_results.combine(dates_wo_outliers, by: 0))

    emit: 
        treefile_reformated
        time_window_phylodeep
        expected_sampling_proportion //To replace with sampling_proportion_analysis
        phylodeep_results
        reformated_bdml
        reformated_bdeiml
        bdeiml_graphs
        bdeiml_graphs
        bdsky_file
        coalsky_file
        bdsky_results
        coalsky_results
        graphs_bdsky
}
