include {ParseGisaidFile} from '../../modules/rsv/PrepareDataset.nf'
include {FilterRandom} from '../../modules/rsv/PrepareDataset.nf'
include {FilterNext} from '../../modules/rsv/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesFirst} from '../../modules/rsv/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesSecond} from '../../modules/rsv/PrepareDataset.nf'
include {RemoveForbiddenChar} from '../../modules/rsv/PrepareDataset.nf'
include {GrepSequences} from '../../modules/rsv/PrepareDataset.nf'
include {AddReference} from '../../modules/rsv/PrepareDataset.nf'

workflow CREATE_REF_DATASET{

    take:
        gisaid_file
        region_file
        country
        nb_max
        sample_min_date
        sample_max_date
        random_sampling
        rsv_refs

    main: 


        parsed_dataset = ParseGisaidFile(gisaid_file, region_file)
        rsva_dataset = parsed_dataset[0]
        rsvb_dataset = parsed_dataset[1]

        dataset = RemoveDuplicatesFirst(rsva_dataset.mix(rsvb_dataset))

        if (random_sampling.equals("yes") || random_sampling.equals("Yes") || random_sampling.equals("YES") || random_sampling.equals("Y") || random_sampling.equals("y")) {
            filtered_dataset = FilterRandom(dataset, nb_max)
        }
        else {
            filtered_dataset = FilterNext(dataset, country, sample_min_date, sample_max_date, nb_max)
        }
        
        filtered_dataset = GrepSequences(filtered_dataset)
        dataset_w_refs = AddReference(filtered_dataset, rsv_refs)
        dataset_ready = RemoveDuplicatesSecond(RemoveForbiddenChar(dataset_w_refs))

    emit: 
    
        dataset_ready

}