//Utils
include {GetMinMaxDate} from '../../modules/sars-cov-2/Utils.nf'
include {EstimateSamplingProportionPhylodeep} from '../../modules/sars-cov-2/Utils.nf'
include {EstimateSamplingProportion} from '../../modules/sars-cov-2/Utils.nf'
include {ConvertTimeWindowToFraction} from '../../modules/sars-cov-2/Utils.nf'

//Create and manipulate tree
include {CutTreeDate} from '../../modules/sars-cov-2/TreeManip.nf'
include {RescaleTreeToDays} from '../../modules/sars-cov-2/TreeManip.nf'
include {GetLabelsFromTree} from '../../modules/sars-cov-2/TreeManip.nf'
include {ReformatTree} from '../../modules/sars-cov-2/TreeManip.nf'
include {RemoveAnnotations} from '../../modules/sars-cov-2/TreeManip.nf'
include {GetDatesFromLabels} from '../../modules/sars-cov-2/TreeManip.nf'
include {GetTreeHeight} from '../../modules/sars-cov-2/TreeManip.nf'
include {GetLabelsFromForest} from '../../modules/sars-cov-2/TreeManip.nf'

//Perform Deep Learning Analysis
include {PhylodeepAnalysis} from '../../modules/sars-cov-2/CompleteAnalysis.nf'

//Perform ML Analysis
include {GenerateTimeWindows } from '../../modules/sars-cov-2/MLAnalysis'
include {PerformBDMLAnalysis} from '../../modules/sars-cov-2/CompleteAnalysis.nf'
include {PerformBDEIMLAnalysis} from '../../modules/sars-cov-2/CompleteAnalysis.nf'
include {ReformatBDMLOutput} from '../../modules/sars-cov-2/MLAnalysis.nf'
include {GetGraphsBDML} from '../../modules/sars-cov-2/MLAnalysis.nf'
include {ReformatBDEIMLOutput} from '../../modules/sars-cov-2/MLAnalysis.nf'
include {GetGraphsBDEIML} from '../../modules/sars-cov-2/MLAnalysis.nf'

//Perform Bayesian Analysis
include {CovFlow} from '../../modules/sars-cov-2/CompleteAnalysis.nf'
include {GetNumberOfMonths} from '../../modules/sars-cov-2/Beast.nf'
include {RunBeastBDSky} from '../../modules/sars-cov-2/Beast.nf'
include {RunBeastCoalSky} from '../../modules/sars-cov-2/Beast.nf'
include {GetGraphsBDsky} from '../../modules/sars-cov-2/Beast.nf'

//ML parameters
params.window_step="1"
params.window_size="4"

workflow COMPLETE_ANALYSIS{

    take:
        treefile_resolved
        tMRCA
        dates_ready
        sample_min_date
        sample_max_date
        nb_cases_file
        expected_sampling_proportion
        //Find a way to have an expected proportion analysis ?
    
    main: 
        
        // Global operations on tree (and other). Some analyses are shared inbetween Beast, ML and phylodeep
        treefile_reformated = ReformatTree(treefile_resolved)
        tree_labels = GetLabelsFromTree(treefile_reformated)
        dates_wo_outliers = GetDatesFromLabels(tree_labels, dates_ready)
        min_and_max_dates = GetMinMaxDate(dates_wo_outliers)

        // Phylodeep part of the analysis
        time_window_phylodeep = ConvertTimeWindowToFraction(sample_min_date,sample_max_date)
        sampling_proportion_analysis_phylodeep = EstimateSamplingProportionPhylodeep(time_window_phylodeep, tree_labels, nb_cases_file, expected_sampling_proportion) // we use the tree_label as the metadata here since only the number of lines (e.g, the number of individuals) is of interest
       // sampling_proportion_analysis = EstimateSamplingProportionPhylodeep(timewindow, tree_labels, nb_cases_file, expected_sampling_proportion) // we use the tree_label as the metadata here since only the number of lines (e.g, the number of individuals) is of interest

        phylodeep_analysis = PhylodeepAnalysis(treefile_resolved, sampling_proportion_analysis_phylodeep) // Right now, expected sampling proportion, can't calculate sampling prop
        phylodeep_results = phylodeep_analysis[0]
        
        //Results of phylodeep that will be used by ML and Beast analyses
        params_for_beast = phylodeep_analysis[1]
        params_for_bdml = phylodeep_analysis[2]
        params_for_bdeiml = phylodeep_analysis[3]


        //ML analysis specific tasks

        //parameters
        window_size = params.window_size
        window_step = params.window_step
        time_windows_ML = GenerateTimeWindows(min_and_max_dates, window_size, window_step).flatten()


        tree_cut = CutTreeDate(treefile_reformated.first(), time_windows_ML)
        tree_cut_filter = tree_cut.filter{ it -> it[1].size() > 0 }
        tree_rescaled_ML = RescaleTreeToDays(tree_cut_filter)
        forest_cut_labels = GetLabelsFromForest(tree_cut_filter)
        sampling_proportion_analysis_ML = EstimateSamplingProportion(forest_cut_labels, nb_cases_file, expected_sampling_proportion) // Right now, the calculation of sampling proportion has been stopped since we don't have any nb_cases_file for sars-cov-2

        bdml_analysis = PerformBDMLAnalysis(tree_rescaled_ML.join(sampling_proportion_analysis_ML), params_for_bdml)
        bdeiml_analysis = PerformBDEIMLAnalysis(tree_rescaled_ML.join(sampling_proportion_analysis_ML), params_for_bdeiml)

        time_windows_name_ML = time_windows_ML.map{it->[it.baseName, it]}
        
        reformated_bdml = ReformatBDMLOutput(bdml_analysis.join(time_windows_name_ML)).collectFile(name: 'bdml_output.tsv', storeDir: "${params.results}/02a_BDML_estimation", keepHeader: true) // reformated_bdml = bdml_analysis.join(time_windows_name, by: [0,1]).groupTuple().each { type, group -> ReformatBDMLOutput(group.collect { [type] + it }).collectFile(name: "bdml_output_${type}.tsv", storeDir: "${params.results}/02a_BDML_estimation/${type}", keepHeader: true)}
        reformated_bdeiml = ReformatBDEIMLOutput(bdeiml_analysis.join(time_windows_name_ML)).collectFile(name: 'bdeiml_output.tsv', storeDir: "${params.results}/02b_BDEIML_estimation", keepHeader: true) // reformated_bdml = bdml_analysis.join(time_windows_name, by: [0,1]).groupTuple().each { type, group -> ReformatBDMLOutput(group.collect { [type] + it }).collectFile(name: "bdml_output_${type}.tsv", storeDir: "${params.results}/02a_BDML_estimation/${type}", keepHeader: true)}

        bdml_graphs = GetGraphsBDML(reformated_bdml)
        bdeiml_graphs = GetGraphsBDEIML(reformated_bdeiml)

        //BEAST analysis specific tasks
        tree_height = GetTreeHeight (treefile_resolved)
        treefile_wo_annotations = RemoveAnnotations(treefile_resolved)
        nb_of_dimensions = GetNumberOfMonths(dates_wo_outliers, tMRCA) //Here, we create n windows (dimensions) following the number of months between the supposed origin and the latest date in our samples.
        covflow_files = CovFlow(treefile_wo_annotations, dates_wo_outliers, tree_height, nb_of_dimensions, params_for_beast)
        bdsky_file = covflow_files[0]
        coalsky_file = covflow_files[1]
        bdsky_results = RunBeastBDSky(bdsky_file)
        coalsky_results = RunBeastCoalSky (coalsky_file)
        graphs_bdsky = GetGraphsBDsky(bdsky_results, dates_wo_outliers)

    emit: 
        treefile_reformated
        time_window_phylodeep
        expected_sampling_proportion //To replace with sampling_proportion_analysis
        phylodeep_results
        reformated_bdml
        reformated_bdeiml
        bdeiml_graphs
        bdeiml_graphs
        bdsky_file
        coalsky_file
        bdsky_results
        coalsky_results
        graphs_bdsky

}
