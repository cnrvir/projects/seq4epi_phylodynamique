//Utils
include {GetMinMaxDate} from '../../modules/sars-cov-2/Utils.nf'
include {EstimateSamplingProportion} from '../../modules/sars-cov-2/Utils.nf'


//Create and manipulate tree
include {RemoveAnnotations} from '../../modules/sars-cov-2/TreeManip.nf'
include {GetLabelsFromTree} from '../../modules/sars-cov-2/TreeManip.nf'
include {GetLabelsFromForest} from '../../modules/sars-cov-2/TreeManip.nf'
include {GetDatesFromLabels} from '../../modules/sars-cov-2/TreeManip.nf'
include {ReformatTree} from '../../modules/sars-cov-2/TreeManip.nf'
include {CutTreeDate} from '../../modules/sars-cov-2/TreeManip.nf'
include {RescaleTreeToDays} from '../../modules/sars-cov-2/TreeManip.nf'

//Perform ML Analysis
include {GenerateTimeWindows } from '../../modules/sars-cov-2/MLAnalysis'
include {PerformBDMLAnalysis} from '../../modules/sars-cov-2/MLAnalysis.nf'
include {PerformBDEIMLAnalysis} from '../../modules/sars-cov-2/MLAnalysis.nf'
include {ReformatBDMLOutput} from '../../modules/sars-cov-2/MLAnalysis.nf'
include {GetGraphsBDML} from '../../modules/sars-cov-2/MLAnalysis.nf'
include {ReformatBDEIMLOutput} from '../../modules/sars-cov-2/MLAnalysis.nf'
include {GetGraphsBDEIML} from '../../modules/sars-cov-2/MLAnalysis.nf'


//ML parameters
params.window_step="1"
params.window_size="4"

workflow ML_ANALYSIS{

    take: 
        treefile_resolved
        dates_ready
        nb_cases_file
        expected_sampling_proportion

    main: 
        window_size = params.window_size
        window_step = params.window_step
        tree_labels = GetLabelsFromTree(RemoveAnnotations(treefile_resolved))
        dates_wo_outliers = GetDatesFromLabels(tree_labels, dates_ready)
        min_and_max_dates = GetMinMaxDate (dates_wo_outliers)
        time_windows = GenerateTimeWindows(min_and_max_dates, window_size, window_step).flatten() 

        treefile_reformated = ReformatTree(treefile_resolved)
        tree_cut = CutTreeDate(treefile_reformated.first(), time_windows)
        tree_cut_filter = tree_cut.filter{ it -> it[1].size() > 0 }
        tree_rescaled = RescaleTreeToDays(tree_cut_filter)
        forest_cut_labels = GetLabelsFromForest(tree_cut_filter)

        sampling_proportion_analysis = EstimateSamplingProportion(forest_cut_labels, nb_cases_file, expected_sampling_proportion)

        bdml_analysis = PerformBDMLAnalysis(tree_rescaled.join(sampling_proportion_analysis))
        bdeiml_analysis = PerformBDEIMLAnalysis(tree_rescaled.join(sampling_proportion_analysis))

        time_windows_name = time_windows.map{it->[it.baseName, it]}
        
        reformated_bdml = ReformatBDMLOutput(bdml_analysis.join(time_windows_name)).collectFile(name: 'bdml_output.tsv', storeDir: "${params.results}/02a_BDML_estimation", keepHeader: true)
        reformated_bdeiml = ReformatBDEIMLOutput(bdeiml_analysis.join(time_windows_name)).collectFile(name: 'bdeiml_output.tsv', storeDir: "${params.results}/02b_BDEIML_estimation", keepHeader: true)

        bdml_graphs = GetGraphsBDML(reformated_bdml)
        bdeiml_graphs = GetGraphsBDEIML(reformated_bdeiml)

    emit: 
        reformated_bdml
        reformated_bdeiml
        bdeiml_graphs
        bdeiml_graphs

}