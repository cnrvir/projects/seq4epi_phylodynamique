
//SUBWORKFLOWS

include { COMPLETE_ANALYSIS } from '../subworkflows/flu/complete.nf'
include { BEAST_ANALYSIS } from '../subworkflows/flu/beast.nf'
include { ML_ANALYSIS } from '../subworkflows/flu/ml.nf'
include { PHYLODEEP_ANALYSIS } from '../subworkflows/flu/phylodeep.nf'
include { CREATE_REF_DATASET } from '../subworkflows/flu/create_ref_dataset.nf'

//MODULES 

//Prepare dataset
include {ParseMetadata} from '../modules/flu/PrepareDataset.nf'
include {ParseSequences} from '../modules/flu/PrepareDataset.nf'
include {RemoveForbiddenChar} from '../modules/flu/PrepareDataset.nf'
include {FilterRandom} from '../modules/flu/PrepareDataset.nf'
include {FilterNext} from '../modules/flu/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesFirst} from '../modules/flu/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesSecond} from '../modules/flu/PrepareDataset.nf'
include {GrepSequences} from '../modules/flu/PrepareDataset.nf'
include {MergeRefQuerydataset} from '../modules/flu/PrepareDataset.nf'
include {GetDates} from '../modules/flu/PrepareDataset.nf'
include {FillMissingInfosDates} from '../modules/flu/PrepareDataset.nf'
include {NextAlign} from '../modules/flu/PrepareDataset.nf'
include {AddReference} from '../modules/flu/PrepareDataset.nf'

//Create and manipulate tree
include {InferTree} from '../modules/flu/TreeManip.nf'
include {LSD2Analysis} from '../modules/flu/TreeManip.nf'
include {RerootTree} from '../modules/flu/TreeManip.nf'
include {RemoveTips} from '../modules/flu/TreeManip.nf'
include {ResolvePolytomies} from '../modules/flu/TreeManip.nf'


//FLU FILES
params.flu_refs = file("$baseDir/assets/flu")


// DATASET OPTIONS
params.country = "France"
params.sample_min_date = "2019-01-01"
params.sample_max_date = "2024-12-01"
params.nb_max = 3000 //Should be around 3K for Beast but you can add more for ML / Phylodeep. Please not that it doesn't take in account potential reference data ! 
params.expected_sampling_proportion = 0.1

// SAMPLING OPTIONS 
params.use_context = "no" // To add (or not) a reference dataset to the "focus" dataset (i.e, the one you want to focus on for the estimation of epidemiological parameters)
params.random_sampling = "no" // to subsample randomly the dataset without using augur filter


workflow FLU {

    //FLU FILES
    flu_refs = params.flu_refs  

    // DATASET OPTIONS
    sample_min_date = params.sample_min_date
    sample_max_date = params.sample_max_date
    country = params.country  
    nb_max = params.nb_max  
    expected_sampling_proportion = params.expected_sampling_proportion

    // SAMPLING OPTIONS 
    use_context = params.use_context
    random_sampling = params.random_sampling

    // INPUT FILES
    input_folder = params.input_folder

    h1n1pdm_fasta = Channel.fromPath("${input_folder}/*h1n1pdm*.fasta")
    h1n1pdm_metadata = Channel.fromPath("${input_folder}/*h1n1pdm*.xls")
    h1n1pdm_input = h1n1pdm_fasta.combine(h1n1pdm_metadata).map{fasta, metadata -> tuple("h1n1pdm", fasta, metadata)}
    
    h3n2_fasta = Channel.fromPath("${input_folder}/*h3n2*.fasta")
    h3n2_metadata = Channel.fromPath("${input_folder}/*h3n2*.xls")
    h3n2_input = h3n2_fasta.combine(h3n2_metadata).map{fasta, metadata -> tuple("h3n2", fasta, metadata)}
    
    vic_fasta = Channel.fromPath("${input_folder}/*vic*.fasta")
    vic_metadata = Channel.fromPath("${input_folder}/*vic*.xls")
    vic_input = vic_fasta.combine(vic_metadata).map{fasta, metadata -> tuple("vic", fasta, metadata)}
    
    input_files = h1n1pdm_input.mix(h3n2_input, vic_input)

    if (params.mode.equals("create_ref_dataset")) {

	    CREATE_REF_DATASET(input_files, country, nb_max, sample_min_date, sample_max_date, random_sampling, flu_refs)

    }

    else if (params.mode.equals("beast") || params.mode.equals("ml") || params.mode.equals("phylodeep") || params.mode.equals("complete")) {


        //Subsampling and Filtering 

        dataset = RemoveDuplicatesFirst(ParseSequences(ParseMetadata(input_files)))

        if (random_sampling.equals("yes") || random_sampling.equals("Yes") || random_sampling.equals("YES") || random_sampling.equals("Y") || random_sampling.equals("y")) {
            filtered_dataset = FilterRandom(dataset, nb_max)
        }
        else {
            filtered_dataset = FilterNext(dataset, country, sample_min_date, sample_max_date, nb_max)
        }
        
        filtered_dataset = GrepSequences(filtered_dataset)

        if (!use_context.equals("no") && !use_context.equals("No") && !use_context.equals("NO") && !use_context.equals("N") && !use_context.equals("n")) {
            filtered_dataset = MergeRefQuerydataset(filtered_dataset, flu_refs)
            dataset_w_refs = AddReference(filtered_dataset, flu_refs)
        }
        else {
            dataset_w_refs = AddReference(filtered_dataset, flu_refs)
        }
        
        dataset_ready = RemoveDuplicatesSecond(RemoveForbiddenChar(dataset_w_refs))
        dataset_ready_w_dates = FillMissingInfosDates(GetDates(dataset_ready))
        dataset_aln = NextAlign(dataset_ready_w_dates, flu_refs)

        tree_analysis = InferTree(dataset_aln)
        rerooted_treefile = RerootTree(tree_analysis, flu_refs)

        lsd2_analysis = LSD2Analysis(rerooted_treefile)
        tMRCA = lsd2_analysis[1]


        treefile_wo_outliers = RemoveTips(lsd2_analysis[0], flu_refs)
        treefile_resolved = ResolvePolytomies(treefile_wo_outliers)

        if (params.mode.equals("complete")) { // In this mode, we use estimated values from phylodeep as priors for BEAST and ML analyses.

            COMPLETE_ANALYSIS(treefile_resolved, tMRCA, dataset_ready_w_dates, sample_min_date, sample_max_date, expected_sampling_proportion)

        }

        if (params.mode.equals("beast")) {

            BEAST_ANALYSIS(treefile_resolved, tMRCA, dataset_ready_w_dates)

        }
        
        if (params.mode.equals("ml")) {

            ML_ANALYSIS(treefile_resolved, dataset_ready_w_dates, expected_sampling_proportion)

        }
        
        if (params.mode.equals("phylodeep")) {

            PHYLODEEP_ANALYSIS(treefile_resolved, sample_min_date, sample_max_date, expected_sampling_proportion)

        }

    }
}
