
//SUBWORKFLOWS

include { COMPLETE_ANALYSIS } from '../subworkflows/general-virus/complete.nf'
include { BEAST_ANALYSIS } from '../subworkflows/general-virus/beast.nf'
include { ML_ANALYSIS } from '../subworkflows/general-virus/ml.nf'
include { PHYLODEEP_ANALYSIS } from '../subworkflows/general-virus/phylodeep.nf'
include { CREATE_REF_DATASET } from '../subworkflows/general-virus/create_ref_dataset.nf'

//MODULES 

//Prepare dataset
include {PrepareDatasetDateAndCountry} from '../modules/general-virus/PrepareDataset.nf'
include {FilterRandom} from '../modules/general-virus/PrepareDataset.nf'
include {FilterNext} from '../modules/general-virus/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesFirst} from '../modules/general-virus/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesSecond} from '../modules/general-virus/PrepareDataset.nf'
include {RemoveForbiddenCharSeq} from '../modules/general-virus/PrepareDataset.nf'
include {RemoveForbiddenCharMetadata} from '../modules/general-virus/PrepareDataset.nf'
include {GrepSequences} from '../modules/general-virus/PrepareDataset.nf'
include {MergeRefQuerydataset} from '../modules/general-virus/PrepareDataset.nf'
include {AddWuhan} from '../modules/general-virus/PrepareDataset.nf'
include {FillMissingInfosDates} from '../modules/general-virus/PrepareDataset.nf'
include {GetDates} from '../modules/general-virus/PrepareDataset.nf'
include {NextAlign} from '../modules/general-virus/PrepareDataset.nf'
include {MaskAlignment} from '../modules/general-virus/PrepareDataset.nf'

//Create and manipulate tree
include {InferTree} from '../modules/general-virus/TreeManip.nf'
include {LSD2Analysis} from '../modules/general-virus/TreeManip.nf'
include {RerootTree} from '../modules/general-virus/TreeManip.nf'
include {RemoveTips} from '../modules/general-virus/TreeManip.nf'
include {ResolvePolytomies} from '../modules/general-virus/TreeManip.nf'

// Here, most assets and defaults parameters are lacking. It's a general-virus analysis, so the user should give them! I still need to put the list of needed parameters in this code (and in the README!)
//I'm using sars-cov-2 as the template. It should be mixed with RSV (multiple reference possible) and with flu (multiple segments possible)

// DATASET OPTIONS
params.country = "France"
params.sample_min_date = "2019-01-01"
params.sample_max_date = "2024-12-01"
params.nb_max = 3000 //Should be around 3K for Beast but you can add more for ML / Phylodeep. Please note that it doesn't take in account potential reference data ! 
params.expected_sampling_proportion = 0.1

// SAMPLING OPTIONS 
params.use_context = "no" // To add (or not) a reference dataset to the "focus" dataset (i.e, the one you want to focus on for the estimation of epidemiological parameters)
params.random_sampling = "no" // to subsample randomly the dataset without using augur filter

// OTHER OPTIONS THAT ARE NOT APPLICABLE WITH A "GENERAL VIRUS" ANALYSIS (for example, we can't have a reference dataset as an asset, since we don't know which virus is analyzed by the user)

params.nb_cases_file = ""
params.nextalign_ref = ""  
params.outgroup_file = ""
params.fasta_ref = ""
params.metadata_ref = ""
params.mask_begin = ""
params.mask_end = ""  
params.mask_sites_file = ""  

workflow GENERAL_VIRUS {

    // MASK OPTIONS
    mask_begin = params.mask_begin  
    mask_end = params.mask_end  
    mask_sites_file = params.mask_sites_file  

    // REFERENCE FILES
    fasta_ref=Channel.fromPath(params.fasta_ref)
    metadata_ref=Channel.fromPath(params.metadata_ref)

    //general-virus FILES
    nb_cases_file = params.nb_cases_file
    nextalign_ref = params.nextalign_ref  
    outgroup_file = params.outgroup_file

    // DATASET OPTIONS
    sample_min_date = params.sample_min_date
    sample_max_date = params.sample_max_date
    country = params.country  
    nb_max = params.nb_max  
    expected_sampling_proportion = params.expected_sampling_proportion

    // SAMPLING OPTIONS 
    use_context = params.use_context
    random_sampling = params.random_sampling

    // INPUT FILES
    fasta=Channel.fromPath(params.fasta)
    metadata=Channel.fromPath(params.metadata)

    if (params.mode.equals("create_ref_dataset")) {

	    CREATE_REF_DATASET(fasta, metadata, country, nb_max, sample_min_date, sample_max_date, random_sampling, fasta_wuhan, metadata_wuhan)

    }

    else if (params.mode.equals("beast") || params.mode.equals("ml") || params.mode.equals("phylodeep") || params.mode.equals("complete")) {


        //Subsampling and Filtering 

        prepared_dataset = PrepareDatasetDateAndCountry(fasta, metadata, country, sample_min_date, sample_max_date)
        prepared_sequences = prepared_dataset[0]
        prepared_metadata = prepared_dataset[1]

        rmdup_dataset = RemoveDuplicatesFirst(prepared_sequences, prepared_metadata)
        rmdup_sequences = rmdup_dataset[0]
        rmdup_metadata = rmdup_dataset[1]

        if (random_sampling.equals("yes") || random_sampling.equals("Yes") || random_sampling.equals("YES") || random_sampling.equals("Y") || random_sampling.equals("y")) {
            filtered_metadata = FilterRandom(rmdup_metadata, nb_max)
        }
        else {
            filtered_metadata = FilterNext(rmdup_metadata, country, sample_min_date, sample_max_date, nb_max)
        }
        
        filtered_fasta = GrepSequences(rmdup_sequences, filtered_metadata)

        if (!use_context.equals("no") && !use_context.equals("No") && !use_context.equals("NO") && !use_context.equals("N") && !use_context.equals("n")) {
            merged_dataset = MergeRefQuerydataset(filtered_fasta, fasta_ref, filtered_metadata, metadata_ref)
            merged_fasta = merged_dataset[0]
            merged_metadata = merged_dataset[1]
            dataset_w_wuhan = AddWuhan(merged_fasta, fasta_wuhan, merged_metadata, metadata_wuhan)
        }
        else {
            dataset_w_wuhan = AddWuhan(filtered_fasta, fasta_wuhan, filtered_metadata, metadata_wuhan)
        }

        dataset_ready = RemoveDuplicatesSecond(RemoveForbiddenCharSeq(dataset_w_wuhan[0]), RemoveForbiddenCharMetadata(dataset_w_wuhan[1]))
        fasta_ready = dataset_ready[0] //Maybe rename to not mix with previous
        metadata_ready = dataset_ready[1]

        dates_ready = FillMissingInfosDates(GetDates(metadata_ready))

        aln = NextAlign(fasta_ready, nextalign_ref)
        aln_masked = MaskAlignment(aln, mask_sites_file, mask_begin, mask_end)

        iqtreefile = InferTree(aln_masked)

        rerooted_treefile = RerootTree(iqtreefile, outgroup_file)

        lsd2_analysis = LSD2Analysis(rerooted_treefile, dates_ready)

        treefile = lsd2_analysis[0]
        tMRCA = lsd2_analysis[1]


        treefile_wo_outliers = RemoveTips(treefile, outgroup_file)
        treefile_resolved = ResolvePolytomies(treefile_wo_outliers)

        if (params.mode.equals("complete")) { // In this mode, we use estimated values from phylodeep as priors for BEAST and ML analyses.

            COMPLETE_ANALYSIS(treefile_resolved, tMRCA, dataset_ready_w_dates, sample_min_date, sample_max_date, nb_cases_file, expected_sampling_proportion)

        }

        if (params.mode.equals("beast")) {

            BEAST_ANALYSIS(treefile_resolved, tMRCA, dates_ready)

        }
        
        if (params.mode.equals("ml")) {

            ML_ANALYSIS(treefile_resolved, dates_ready, nb_cases_file, expected_sampling_proportion)

        }
        
        if (params.mode.equals("phylodeep")) {

            PHYLODEEP_ANALYSIS(treefile_resolved, sample_min_date, sample_max_date, nb_cases_file, expected_sampling_proportion)

        }

    }

}