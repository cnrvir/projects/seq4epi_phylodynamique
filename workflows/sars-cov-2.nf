
//SUBWORKFLOWS

include { COMPLETE_ANALYSIS } from '../subworkflows/sars-cov-2/complete.nf'
include { BEAST_ANALYSIS } from '../subworkflows/sars-cov-2/beast.nf'
include { ML_ANALYSIS } from '../subworkflows/sars-cov-2/ml.nf'
include { PHYLODEEP_ANALYSIS } from '../subworkflows/sars-cov-2/phylodeep.nf'
include { CREATE_REF_DATASET } from '../subworkflows/sars-cov-2/create_ref_dataset.nf'

//MODULES 

//Prepare dataset
include {PrepareDatasetDateAndCountry} from '../modules/sars-cov-2/PrepareDataset.nf'
include {FilterRandom} from '../modules/sars-cov-2/PrepareDataset.nf'
include {FilterNext} from '../modules/sars-cov-2/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesFirst} from '../modules/sars-cov-2/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesSecond} from '../modules/sars-cov-2/PrepareDataset.nf'
include {RemoveForbiddenCharSeq} from '../modules/sars-cov-2/PrepareDataset.nf'
include {RemoveForbiddenCharMetadata} from '../modules/sars-cov-2/PrepareDataset.nf'
include {GrepSequences} from '../modules/sars-cov-2/PrepareDataset.nf'
include {MergeRefQuerydataset} from '../modules/sars-cov-2/PrepareDataset.nf'
include {AddWuhan} from '../modules/sars-cov-2/PrepareDataset.nf'
include {FillMissingInfosDates} from '../modules/sars-cov-2/PrepareDataset.nf'
include {GetDates} from '../modules/sars-cov-2/PrepareDataset.nf'
include {NextAlign} from '../modules/sars-cov-2/PrepareDataset.nf'
include {MaskAlignment} from '../modules/sars-cov-2/PrepareDataset.nf'

//Create and manipulate tree
include {InferTree} from '../modules/sars-cov-2/TreeManip.nf'
include {LSD2Analysis} from '../modules/sars-cov-2/TreeManip.nf'
include {RerootTree} from '../modules/sars-cov-2/TreeManip.nf'
include {RemoveTips} from '../modules/sars-cov-2/TreeManip.nf'
include {ResolvePolytomies} from '../modules/sars-cov-2/TreeManip.nf'

// MASK OPTIONS
params.mask_begin = 100
params.mask_end = 201
params.mask_sites_file = file("$baseDir/assets/sites_to_mask.txt")

// REFERENCE FILES
params.fasta_ref = file("$baseDir/assets/sars-cov-2/ref_dataset.fasta") // Used for example of reference dataset (if you want to use the --use_context)
params.metadata_ref = file("$baseDir/assets/sars-cov-2/ref_dataset.tsv")

//SARS-COV-2 FILES
params.nb_cases_file = file("$baseDir/assets/cases_france.tsv") // nb of french cases through time. Used to estimate the sampling proportion (if you want to create your own file)
params.fasta_wuhan = file("$baseDir/assets/sars-cov-2/wuhan_sequences.fasta")
params.metadata_wuhan = file("$baseDir/assets/sars-cov-2/wuhan_metadata.tsv")
params.nextalign_ref = file("$baseDir/assets/sars-cov-2/wuhan_for_nextalign.fasta")
params.outgroup_file = file("$baseDir/assets/outgroup_file.txt") // Soon to be depecrated. Is used to remouve the outgroups (both wuhan sequences) after the rerooting of the tree

// DATASET OPTIONS
params.country = "France"
params.sample_min_date = "2019-01-01"
params.sample_max_date = "2024-12-01"
params.nb_max = 3000 //Should be around 3K for Beast but you can add more for ML / Phylodeep. Please not that it doesn't take in account potential reference data ! 
params.expected_sampling_proportion = 0.1

// SAMPLING OPTIONS 
params.use_context = "no" // To add (or not) a reference dataset to the "focus" dataset (i.e, the one you want to focus on for the estimation of epidemiological parameters)
params.random_sampling = "no" // to subsample randomly the dataset without using augur filter


workflow SARS_COV_2 {

    // MASK OPTIONS
    mask_begin = params.mask_begin  
    mask_end = params.mask_end  
    mask_sites_file = params.mask_sites_file  

    // REFERENCE FILES
    fasta_ref=Channel.fromPath(params.fasta_ref)
    metadata_ref=Channel.fromPath(params.metadata_ref)

    //SARS-COV-2 FILES
    nb_cases_file = params.nb_cases_file
    fasta_wuhan = params.fasta_wuhan
    metadata_wuhan = params.metadata_wuhan
    nextalign_ref = params.nextalign_ref  
    outgroup_file = params.outgroup_file

    // DATASET OPTIONS
    sample_min_date = params.sample_min_date
    sample_max_date = params.sample_max_date
    country = params.country  
    nb_max = params.nb_max  
    expected_sampling_proportion = params.expected_sampling_proportion

    // SAMPLING OPTIONS 
    use_context = params.use_context
    random_sampling = params.random_sampling

    // INPUT FILES
    fasta=Channel.fromPath(params.fasta)
    metadata=Channel.fromPath(params.metadata)

    if (params.mode.equals("create_ref_dataset")) {

	    CREATE_REF_DATASET(fasta, metadata, country, nb_max, sample_min_date, sample_max_date, random_sampling, fasta_wuhan, metadata_wuhan)

    }

    else if (params.mode.equals("beast") || params.mode.equals("ml") || params.mode.equals("phylodeep") || params.mode.equals("complete")) {


        //Subsampling and Filtering 

        prepared_dataset = PrepareDatasetDateAndCountry(fasta, metadata, country, sample_min_date, sample_max_date)
        prepared_sequences = prepared_dataset[0]
        prepared_metadata = prepared_dataset[1]

        rmdup_dataset = RemoveDuplicatesFirst(prepared_sequences, prepared_metadata)
        rmdup_sequences = rmdup_dataset[0]
        rmdup_metadata = rmdup_dataset[1]

        if (random_sampling.equals("yes") || random_sampling.equals("Yes") || random_sampling.equals("YES") || random_sampling.equals("Y") || random_sampling.equals("y")) {
            filtered_metadata = FilterRandom(rmdup_metadata, nb_max)
        }
        else {
            filtered_metadata = FilterNext(rmdup_metadata, country, sample_min_date, sample_max_date, nb_max)
        }
        
        filtered_fasta = GrepSequences(rmdup_sequences, filtered_metadata)

        if (!use_context.equals("no") && !use_context.equals("No") && !use_context.equals("NO") && !use_context.equals("N") && !use_context.equals("n")) {
            merged_dataset = MergeRefQuerydataset(filtered_fasta, fasta_ref, filtered_metadata, metadata_ref)
            merged_fasta = merged_dataset[0]
            merged_metadata = merged_dataset[1]
            dataset_w_wuhan = AddWuhan(merged_fasta, fasta_wuhan, merged_metadata, metadata_wuhan)
        }
        else {
            dataset_w_wuhan = AddWuhan(filtered_fasta, fasta_wuhan, filtered_metadata, metadata_wuhan)
        }

        dataset_ready = RemoveDuplicatesSecond(RemoveForbiddenCharSeq(dataset_w_wuhan[0]), RemoveForbiddenCharMetadata(dataset_w_wuhan[1]))
        fasta_ready = dataset_ready[0] //Maybe rename to not mix with previous
        metadata_ready = dataset_ready[1]

        dates_ready = FillMissingInfosDates(GetDates(metadata_ready))

        aln = NextAlign(fasta_ready, nextalign_ref)
        aln_masked = MaskAlignment(aln, mask_sites_file, mask_begin, mask_end)

        iqtreefile = InferTree(aln_masked)

        rerooted_treefile = RerootTree(iqtreefile, outgroup_file)

        lsd2_analysis = LSD2Analysis(rerooted_treefile, dates_ready)

        treefile = lsd2_analysis[0]
        tMRCA = lsd2_analysis[1]


        treefile_wo_outliers = RemoveTips(treefile, outgroup_file)
        treefile_resolved = ResolvePolytomies(treefile_wo_outliers)

        if (params.mode.equals("complete")) { // In this mode, we use estimated values from phylodeep as priors for BEAST and ML analyses.

            COMPLETE_ANALYSIS(treefile_resolved, tMRCA, dates_ready, sample_min_date, sample_max_date, nb_cases_file, expected_sampling_proportion)

        }

        if (params.mode.equals("beast")) {

            BEAST_ANALYSIS(treefile_resolved, tMRCA, dates_ready)

        }
        
        if (params.mode.equals("ml")) {

            ML_ANALYSIS(treefile_resolved, dates_ready, nb_cases_file, expected_sampling_proportion)

        }
        
        if (params.mode.equals("phylodeep")) {

            PHYLODEEP_ANALYSIS(treefile_resolved, sample_min_date, sample_max_date, nb_cases_file, expected_sampling_proportion)

        }

    }

}