
//SUBWORKFLOWS

include { COMPLETE_ANALYSIS } from '../subworkflows/rsv/complete.nf'
include { BEAST_ANALYSIS } from '../subworkflows/rsv/beast.nf'
include { ML_ANALYSIS } from '../subworkflows/rsv/ml.nf'
include { PHYLODEEP_ANALYSIS } from '../subworkflows/rsv/phylodeep.nf'
include { CREATE_REF_DATASET } from '../subworkflows/rsv/create_ref_dataset.nf'

//MODULES 

//Prepare dataset
include {ParseGisaidFile} from '../modules/rsv/PrepareDataset.nf'
include {FilterRandom} from '../modules/rsv/PrepareDataset.nf'
include {FilterNext} from '../modules/rsv/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesFirst} from '../modules/rsv/PrepareDataset.nf'
include {RemoveDuplicates as RemoveDuplicatesSecond} from '../modules/rsv/PrepareDataset.nf'
include {RemoveForbiddenChar} from '../modules/rsv/PrepareDataset.nf'
include {GrepSequences} from '../modules/rsv/PrepareDataset.nf'
include {MergeRefQuerydataset} from '../modules/rsv/PrepareDataset.nf'
include {FillMissingInfosDates} from '../modules/rsv/PrepareDataset.nf'
include {GetDates} from '../modules/rsv/PrepareDataset.nf'
include {NextAlign} from '../modules/rsv/PrepareDataset.nf'
include {AddReference} from '../modules/rsv/PrepareDataset.nf'

//Create and manipulate tree
include {InferTree} from '../modules/rsv/TreeManip.nf'
include {LSD2Analysis} from '../modules/rsv/TreeManip.nf'
include {RerootTree} from '../modules/rsv/TreeManip.nf'
include {RemoveTips} from '../modules/rsv/TreeManip.nf'
include {ResolvePolytomies} from '../modules/rsv/TreeManip.nf'


//RSV FILES
params.region_file = file("$baseDir/assets/rsv_region_file.tsv")
params.rsv_refs = file("$baseDir/assets/rsv")


// DATASET OPTIONS
params.country = "France"
params.sample_min_date = "2019-01-01"
params.sample_max_date = "2024-12-01"
params.nb_max = 3000 //Should be around 3K for Beast but you can add more for ML / Phylodeep. Please not that it doesn't take in account potential reference data ! 

// SAMPLING OPTIONS 
params.use_context = "no" // To add (or not) a reference dataset to the "focus" dataset (i.e, the one you want to focus on for the estimation of epidemiological parameters)
params.random_sampling = "no" // to subsample randomly the dataset without using augur filter

//ANALYSIS OPTIONS 
params.expected_sampling_proportion = 0.1
params.window_size = 1
params.window_step = 0.25

workflow RSV {

    //RSV FILES
    region_file = params.region_file
    rsv_refs = params.rsv_refs  

    // DATASET OPTIONS
    sample_min_date = params.sample_min_date
    sample_max_date = params.sample_max_date
    country = params.country  
    nb_max = params.nb_max  

    // SAMPLING OPTIONS 
    use_context = params.use_context
    random_sampling = params.random_sampling

    // ANALYSIS OPTIONS 
    expected_sampling_proportion = params.expected_sampling_proportion
    window_size = params.window_size
    window_step = params.window_step

    // INPUT FILES
    gisaid_file=Channel.fromPath(params.gisaid_file)

    if (params.mode.equals("create_ref_dataset")) {

	    CREATE_REF_DATASET(gisaid_file, region_file,country,nb_max, sample_min_date, sample_max_date, random_sampling, rsv_refs)

    }

    else if (params.mode.equals("beast") || params.mode.equals("ml") || params.mode.equals("phylodeep") || params.mode.equals("complete")) {


        //Subsampling and Filtering 

        parsed_dataset = ParseGisaidFile(gisaid_file, region_file)
        rsva_dataset = parsed_dataset[0]
        rsvb_dataset = parsed_dataset[1]

        dataset = RemoveDuplicatesFirst(rsva_dataset.mix(rsvb_dataset))

        if (random_sampling.equals("yes") || random_sampling.equals("Yes") || random_sampling.equals("YES") || random_sampling.equals("Y") || random_sampling.equals("y")) {
            filtered_dataset = FilterRandom(dataset, nb_max)
        }
        else {
            filtered_dataset = FilterNext(dataset, country, sample_min_date, sample_max_date, nb_max)
        }
        
        filtered_dataset = GrepSequences(filtered_dataset)

        if (!use_context.equals("no") && !use_context.equals("No") && !use_context.equals("NO") && !use_context.equals("N") && !use_context.equals("n")) {
            filtered_dataset = MergeRefQuerydataset(filtered_dataset, rsv_refs)
            dataset_w_refs = AddReference(filtered_dataset, rsv_refs)
        }
        else {
            dataset_w_refs = AddReference(filtered_dataset, rsv_refs)
        }
        
        dataset_ready = RemoveDuplicatesSecond(RemoveForbiddenChar(dataset_w_refs))
        dataset_ready_w_dates = FillMissingInfosDates(GetDates(dataset_ready))
        dataset_aln = NextAlign(dataset_ready_w_dates, rsv_refs)

        tree_analysis = InferTree(dataset_aln)
        rerooted_treefile = RerootTree(tree_analysis, rsv_refs)

        lsd2_analysis = LSD2Analysis(rerooted_treefile)
        tMRCA = lsd2_analysis[1]


        treefile_wo_outliers = RemoveTips(lsd2_analysis[0], rsv_refs)
        treefile_resolved = ResolvePolytomies(treefile_wo_outliers)

        if (params.mode.equals("complete")) { // In this mode, we use estimated values from phylodeep as priors for BEAST and ML analyses.

            COMPLETE_ANALYSIS(treefile_resolved, tMRCA, dataset_ready_w_dates, sample_min_date, sample_max_date, expected_sampling_proportion)

        }

        if (params.mode.equals("beast")) {

            BEAST_ANALYSIS(treefile_resolved, tMRCA, dataset_ready_w_dates)

        }
        
        if (params.mode.equals("ml")) {

            ML_ANALYSIS(treefile_resolved, dataset_ready_w_dates, expected_sampling_proportion)

        }
        
        if (params.mode.equals("phylodeep")) {

            PHYLODEEP_ANALYSIS(treefile_resolved, sample_min_date, sample_max_date, expected_sampling_proportion)

        }

    }
}
