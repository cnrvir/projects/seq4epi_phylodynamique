#!/usr/bin/env python3
import argparse
from argparse import RawTextHelpFormatter

import sys
import os.path

from Bio import SeqIO

import metadataFilter
from metadataFilter import readCSV

import time
from datetime import datetime as dt

from configparser import ConfigParser


def print_header(output_xml):
	with open(output_xml, 'w') as f:
		f.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?>')
		f.write('\n<beast beautitemplate="Standard" beautistatus=""')
		f.write('\n\tnamespace="beast.core\n\t:beast.evolution.alignment\n\t:beast.evolution.tree.coalescent\n\t:beast.core.util\n\t:beast.evolution.nuc\n\t:beast.evolution.operators\n\t:beast.evolution.sitemodel\n\t:beast.evolution.substitutionmodel\n\t:beast.evolution.likelihood"')
		f.write('\n\tversion="2.6">')
		f.write('\n<map name="Uniform" >beast.math.distributions.Uniform</map>')
		f.write('\n<map name="Exponential" >beast.math.distributions.Exponential</map>')
		f.write('\n<map name="LogNormal" >beast.math.distributions.LogNormalDistributionModel</map>')
		f.write('\n<map name="Normal" >beast.math.distributions.Normal</map>')
		f.write('\n<map name="Beta" >beast.math.distributions.Beta</map>')
		f.write('\n<map name="Gamma" >beast.math.distributions.Gamma</map>')
		f.write('\n<map name="LaplaceDistribution" >beast.math.distributions.LaplaceDistribution</map>')
		f.write('\n<map name="prior" >beast.math.distributions.Prior</map>')
		f.write('\n<map name="InverseGamma" >beast.math.distributions.InverseGamma</map>')
		f.write('\n<map name="OneOnX" >beast.math.distributions.OneOnX</map>')
	f.close()


def print_fixed_tree(input_Newick_Tree,output_xml):
	# fichier avec l'arbre en newick comme entrée
	f=open(input_Newick_Tree, "r")
	tree=f.readline()
	f.close()
	if tree[-1]=='\n':
		tree=tree[:-1]
	tmp='\n<tree id="Tree" spec="beast.util.TreeParser" IsLabelledNewick="true" adjustTipHeights="false" newick="'+tree+'"/>'
	with open(output_xml, 'a') as f:
		f.write('\n<!-- Fixed tree -->')
		f.write(tmp)
	f.close()

def print_run(output_xml,chain_length,every,output_log,dimension):
	tmp_chain = '\n<run id="mcmc" spec="MCMC" chainLength="'+chain_length+'">'
	tmp_store_every = '\n\t<state id="state" spec="State" storeEvery="'+every+'">'
	tmp_param_bpopsize = '\n\t\t<parameter id="bPopSizes" spec="parameter.RealParameter" dimension="'+str(dimension)+'" lower="0.0" name="stateNode">380.0</parameter>'
	tmp_param_bgroupsize = '\n\t\t<stateNode id="bGroupSizes" spec="parameter.IntegerParameter" dimension="'+str(dimension)+'">1</stateNode>'
	tmp_trace_log = '\n\t<logger id="tracelog" spec="Logger" fileName="'+output_log+'" logEvery="'+every+'" model="@posterior" sanitiseHeaders="true" sort="smart">'
	tmp_screen_log = '\n\t<logger id="screenlog" spec="Logger" logEvery="'+every+'">'
	with open(output_xml, 'a') as f:
		f.write(tmp_chain)
		f.write(tmp_store_every)
		f.write(tmp_param_bpopsize)
		f.write(tmp_param_bgroupsize)
		f.write('\n\t</state>')
		f.write('\n\t<distribution id="posterior" spec="util.CompoundDistribution">')
		f.write('\n\t\t<distribution id="prior" spec="util.CompoundDistribution">')
		f.write('\n\t\t\t<distribution id="BayesianSkyline" spec="BayesianSkyline" groupSizes="@bGroupSizes" popSizes="@bPopSizes">')
		f.write('\n\t\t\t\t<treeIntervals id="BSPTreeIntervals" spec="TreeIntervals" tree="@Tree"/>')
		f.write('\n\t\t\t</distribution>')
		f.write('\n\t\t\t<distribution id="MarkovChainedPopSizes" spec="beast.math.distributions.MarkovChainDistribution" jeffreys="true" parameter="@bPopSizes"/>')
		f.write('\n\t\t</distribution>')
		f.write('\n\t\t<distribution id="likelihood" spec="util.CompoundDistribution" useThreads="true">')
		f.write('\n\t\t</distribution>')
		f.write('\n\t</distribution>')
		f.write('\n\t<operator id="popSizesScaler" spec="ScaleOperator" parameter="@bPopSizes" scaleFactor="0.75" weight="15.0"/>')
		f.write('\n\t<operator id="groupSizesDelta" spec="DeltaExchangeOperator" integer="true" weight="6.0">')
		f.write('\n\t\t<intparameter idref="bGroupSizes"/>')
		f.write('\n\t</operator>')
		f.write(tmp_trace_log)
		f.write('\n\t\t<log idref="posterior"/>')
		f.write('\n\t\t<log idref="likelihood"/>')
		f.write('\n\t\t<log idref="prior"/>')
		f.write('\n\t\t<log id="TreeHeight" spec="beast.evolution.tree.TreeHeightLogger" tree="@Tree"/>')
		f.write('\n\t\t<log idref="bPopSizes"/>')
		f.write('\n\t\t<log idref="bGroupSizes"/>')
		f.write('\n\t</logger>')
		f.write(tmp_screen_log)
		f.write('\n\t\t<log idref="posterior"/>')
		f.write('\n\t\t<log idref="likelihood"/>')
		f.write('\n\t\t<log idref="prior"/>')
		f.write('\n\t</logger>')
		f.write('\n</run>')
		f.write('\n</beast>')
	f.close()

def print_XML_tree_fixed(dico):
	output_prefix = dico["output"]["prefix"]
	output_xml = output_prefix + '.xml'
	output_log = output_prefix + ".log"
	chain_length=dico['mcmc']["chain_length"]
	store_every=dico['mcmc']['store_every']
	input_tree = dico['input_files']["tree"]
	dimension = dico['Ne']['dimension']
	tmp = "Writing XML file for Bayesian Coalescent Skyline analysis : " + output_xml
	print(tmp)
	print_header(output_xml = output_xml)
	print_fixed_tree(input_Newick_Tree = input_tree, output_xml = output_xml)
	print_run(output_xml = output_xml, chain_length = chain_length, every = store_every,output_log = output_log,dimension= dimension)


def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        # return open(arg, 'r')  # return an open file handle
        return arg

def show_config_example():
	tmp='[output]\n\
	prefix=delta_covid\n\n\
	[Ne]\n\
	dimension=4\n\
	[input_files]\n\
	tree="tree.newick"\n\
	[mcmc]\n\
	chain_length=100000\n\
	store_every=10000\n\
	log_every=10000\n'
	return tmp


def config_to_dico(config_file):
	print("Parsing config file...")
	parser = ConfigParser()
	parser.read(config_file)
	dico = {'Ne': {'dimension': 4},
			'mcmc': {'store_every': 100000, 'log_every': 100000, 'chain_length': 10000000}
			}
	dico["output"] = {}
	dico["input_files"] = {}
	dico["input_files"]['tree'] = None
	dico["input_files"]['alignment'] = None
	## TO DO ADD CLOCK RATE
	sections = parser.sections()
	for section in sections:
		subsections = list(dict(parser.items(section)).keys())
		for subsection in subsections:
			dico[section][subsection]=dict(parser.items(section))[subsection]
	return dico



def main(dico):
	if dico['input_files']['tree']:
		print_XML_tree_fixed(dico)
	else:
		pass

if __name__ == "__main__":

	parser=argparse.ArgumentParser(description='Generates an XML file to run Beast2 BDSKY analysis.', formatter_class=RawTextHelpFormatter)
	parser.add_argument("--config", dest="config", required=True, help="input config file with Beast2 prior parameters", metavar="FILE")
	parser.add_argument('--example', help=show_config_example(), metavar=None)
	args=parser.parse_args()

	if args.config:
		args_dico = config_to_dico(config_file = args.config)
		main(args_dico)
