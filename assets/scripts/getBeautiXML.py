#!/usr/bin/env python3

import argparse
from argparse import RawTextHelpFormatter

import sys
import os.path

from Bio import SeqIO

import metadataFilter
from metadataFilter import readCSV

import time
from datetime import datetime as dt

from configparser import ConfigParser

def toYearFraction(date):
	def sinceEpoch(date): # returns seconds since epoch
		return time.mktime(date.timetuple())
	s = sinceEpoch
	year = date.year
	startOfThisYear = dt(year=year, month=1, day=1)
	startOfNextYear = dt(year=year+1, month=1, day=1)
	yearElapsed = s(date) - s(startOfThisYear)
	yearDuration = s(startOfNextYear) - s(startOfThisYear)
	fraction = yearElapsed/yearDuration
	return date.year + fraction

def substract_alphabetic(word):
	only_alpha = ""
	for char in word:
		if char.isalpha():
			only_alpha += char
	return only_alpha

def print_header(output_xml):
	with open(output_xml, 'w') as f:
		f.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?>')
		f.write('\n<beast beautitemplate="Standard" beautistatus=""')
		f.write('\n\tnamespace="beast.core\n\t:beast.evolution.alignment\n\t:beast.evolution.tree.coalescent\n\t:beast.core.util\n\t:beast.core.parameter\n\t:beast.evolution.nuc\n\t:beast.evolution.operators\n\t:beast.evolution.sitemodel\n\t:beast.evolution.substitutionmodel\n\t:beast.evolution.likelihood"')
		f.write('\n\trequired="BDSKY v1.4.8" version="2.6">')
		f.write('\n<map name="Uniform" >beast.math.distributions.Uniform</map>')
		f.write('\n<map name="Exponential" >beast.math.distributions.Exponential</map>')
		f.write('\n<map name="LogNormal" >beast.math.distributions.LogNormalDistributionModel</map>')
		f.write('\n<map name="Normal" >beast.math.distributions.Normal</map>')
		f.write('\n<map name="Beta" >beast.math.distributions.Beta</map>')
		f.write('\n<map name="Gamma" >beast.math.distributions.Gamma</map>')
		f.write('\n<map name="LaplaceDistribution" >beast.math.distributions.LaplaceDistribution</map>')
		f.write('\n<map name="prior" >beast.math.distributions.Prior</map>')
		f.write('\n<map name="InverseGamma" >beast.math.distributions.InverseGamma</map>')
		f.write('\n<map name="OneOnX" >beast.math.distributions.OneOnX</map>')
		f.write('\n<function spec="beast.core.util.Slice" id="samplingProportionSlice" arg="@samplingProportion" index="1" count="1"/>')
	f.close()

def print_seq_data(input_fasta,output_xml):
	with open(output_xml, 'a') as f:
		f.write('\n<data id="Covid" spec="Alignment" name="alignment">\n')
		for seq_record in SeqIO.parse(input_fasta, format = "fasta"):
			tmp='\n\t<sequence id="seq_'+seq_record.id+'" spec="Sequence" taxon="'+seq_record.id+'" totalcount="4" value="'+str(seq_record.seq)+'"/>'
			f.write(tmp)
		f.write('\n</data>')
	f.close()


def print_fixed_tree(input_Newick_Tree,output_xml):
	f=open(input_Newick_Tree, "r")
	tree=f.readline()
	f.close()
	if tree[-1]=='\n':
		tree=tree[:-1]
	tmp='\n<tree id="Tree" spec="beast.util.TreeParser" IsLabelledNewick="true" adjustTipHeights="false" newick="'+tree+'"/>'
	with open(output_xml, 'a') as f:
		f.write('\n<!-- Fixed tree -->')
		f.write(tmp)
	f.close()

def print_date_trait_tree(dates_file, output_xml):
	dates = readCSV(csv_file = dates_file, sep="\t")
	dd = dates['date'].tolist()
	nn = dates['strain'].tolist()
	tmp = ""
	for d,n in zip(dd,nn):
		tmp = tmp + '\n\t\t\t\t' + n + '=' + d
	with open(output_xml, 'a') as f:
		f.write('\n\t\t<tree id="Tree" name="stateNode">')
		f.write('\n\t\t\t<trait id="dateTrait" spec="beast.evolution.tree.TraitSet" traitname="date" dateFormat="yyyy-M-dd">')
		f.write(tmp)
		f.write('\n\t\t\t\t<taxa id="TaxonSet.Covid" spec="TaxonSet">')
		f.write('\n\t\t\t\t\t<alignment idref="Covid"/>')
		f.write('\n\t\t\t\t</taxa>')
		f.write('\n\t\t\t</trait>')
		f.write('\n\t\t\t<taxonset idref="TaxonSet.Covid"/>')
		f.write('\n\t\t</tree>')
	f.close()


def print_evolution_model_parameters_state(output_xml, clock_rate_value = None, estimate = False):
	if estimate:
		tmp = '\n\t\t<parameter id="clockRate" name="stateNode">1.0</parameter>'
	else:
		tmp = '\n\t\t<parameter id="clockRate" name="stateNode" estimate="false">'+clock_rate_value+'</parameter>'
	with open(output_xml, 'a') as f:
		f.write('\n\t\t<parameter id="freqParameter.s" spec="parameter.RealParameter" dimension="4" lower="0.0" name="stateNode" upper="1.0">0.25</parameter>')
		f.write('\n\t\t<parameter id="rateAC.s" spec="parameter.RealParameter" lower="0.0" name="stateNode">1.0</parameter>')
		f.write('\n\t\t<parameter id="rateAG.s" spec="parameter.RealParameter" lower="0.0" name="stateNode">1.0</parameter>')
		f.write('\n\t\t<parameter id="rateAT.s" spec="parameter.RealParameter" lower="0.0" name="stateNode">1.0</parameter>')
		f.write('\n\t\t<parameter id="rateCG.s" spec="parameter.RealParameter" lower="0.0" name="stateNode">1.0</parameter>')
		f.write('\n\t\t<parameter id="rateGT.s" spec="parameter.RealParameter" lower="0.0" name="stateNode">1.0</parameter>')
		f.write(tmp)
	f.close()

def check_dico(dico):
	"""Check if lower < start < upper + origin if start and upper > height"""
	parameters = {"origin" : "origin", "sampling_proportion":"samplingProportion", "become_uninfectious_rate":"becomeUninfectiousRate", "R0":"reproductiveNumber"}
	for bdsky_name, dico_name in zip(parameters.values(),parameters.keys()):
		if "fixed" not in dico[dico_name].keys():
			lower = dico[dico_name]["lower"]
			upper = dico[dico_name]["upper"]
			if lower >= upper:
				dico[dico_name]["lower"] = upper
				dico[dico_name]["upper"] = lower
			if "start" not in dico[dico_name].keys():
				start = random.randint(int(lower), int(upper))
				dico[dico_name]["start"] = start
			else:
				start = dico[dico_name]["start"]
			if "height" in dico[dico_name].keys():
				if start <= dico[dico_name]["height"]:
					start = float(dico[dico_name]["height"]) + 0.1
					dico[dico_name]["start"] = str(start)
				if float(upper) < float(dico[dico_name]["height"]):
					upper = float(dico[dico_name]["height"]) + float(dico[dico_name]["height"]) / 2
					dico[dico_name]["upper"] = upper
	return dico

def print_bdsky_parameters_state(dico, output_xml):
	parameters = {"origin" : "origin", "sampling_proportion":"samplingProportion", "become_uninfectious_rate":"becomeUninfectiousRate", "R0":"reproductiveNumber"}
	with open(output_xml, 'a') as f:
		for bdsky_name, dico_name in zip(parameters.values(),parameters.keys()):
			if "fixed" in dico[dico_name].keys():
				value = dico[dico_name]["fixed"]
				tmp = '\n\t\t<parameter id="' + bdsky_name + '" name="stateNode" estimate="false">'+str(value)+'</parameter>'
			else:
				lower = dico[dico_name]["lower"]
				upper = dico[dico_name]["upper"]
				if "start" not in dico[dico_name].keys():
					start = random.randint(int(lower), int(upper))
				else:
					start = dico[dico_name]["start"]
				if "height" in dico[dico_name].keys():
					if start <= dico[dico_name]["height"]:
						start = float(dico[dico_name]["height"]) + 0.1
						start = str(start)
					if float(upper) < float(dico[dico_name]["height"]):
						upper = float(dico[dico_name]["height"]) + 2
				if "dimension" in dico[dico_name].keys():
					if dico_name == "sampling_proportion":
						dimension = dico[dico_name]["dimension"]
						start = '0' + dimension*(" "+str(start))
						tmp = '\n\t\t<parameter id="' + bdsky_name + '" lower="' + str(lower) + '" name="stateNode" upper="'+ str(upper) + '">'+str(start)+'</parameter>'
					else:
						dimension = dico[dico_name]["dimension"]
						tmp = '\n\t\t<parameter id="'+bdsky_name+'" dimension="'+str(dimension)+'" lower="'+str(lower)+'" name="stateNode" upper="'+str(upper)+'">'+str(start)+'</parameter>'
				else:
					tmp = '\n\t\t<parameter id="' + bdsky_name + '" lower="' + str(lower) + '" name="stateNode" upper="'+ str(upper) + '">'+str(start)+'</parameter>'
			f.write(tmp)
	f.close()


def print_initial_tree(output_xml):
	""" Required when estimating the tree """
	with open(output_xml, 'a') as f:
		f.write('\n\t<!-- Initial tree -->')
		f.write('\n\t<init id="RandomTree" spec="beast.evolution.tree.RandomTree" estimate="false" initial="@Tree" taxa="@Covid">')
		f.write('\n\t\t<populationModel id="ConstantPopulation0" spec="ConstantPopulation" popSize="1.0"/>')
		f.write('\n\t</init>')
	f.close()

def print_bdsky_treeprior_distribution(output_xml, dates_file, birth_rate_change_dates=None):
	""" In case we want to specify dates as birth rates changing time points : input of birth_rate_change_dates must e a list of dates as %Y-%m-%d"""
	dates = readCSV(csv_file = dates_file, sep="\t")
	max_date = dt.strptime(dates["date"].max(), '%Y-%m-%d')
	min_date = dt.strptime(dates["date"].min(), '%Y-%m-%d')
	max_date_float = round(toYearFraction(max_date),3)
	min_date_float = round(toYearFraction(min_date),3)
	first_sampling_changing_time = round(round(max_date_float-min_date_float,3) + 5 , 3)
	tmp = '\n\t\t\t\t<samplingRateChangeTimes spec="RealParameter" value="0 '+str(first_sampling_changing_time)+'"/>'
	if birth_rate_change_dates:
		birth_rate_change_dates = birth_rate_change_dates.replace('"','').split(",")
		times = []
		for changing_time in birth_rate_change_dates:
			dtime = dt.strptime(str(changing_time), '%Y-%m-%d')
			time_float = round(toYearFraction(dtime),3)
			tmp2 = round(max_date_float - time_float, 3)
			times.append(str(tmp2))
		times.sort()
		tmp1 = "0 " + " ".join(times)
		tmp = tmp + '\n\t\t\t\t<birthRateChangeTimes spec="RealParameter" value="'+tmp1+'"/>'
	tmp = tmp + '\n\t\t\t\t<reverseTimeArrays spec="beast.core.parameter.BooleanParameter" value="true false true false false"/>'
	with open(output_xml, 'a') as f:
		f.write('\n\t\t\t\t<!-- Tree likelihood (BDSKY) -->')
		f.write('\n\t\t\t\t<distribution id="BirthDeathSkySerial"')
		f.write('\n\t\t\t\t\tspec="beast.evolution.speciation.BirthDeathSkylineModel"')
		f.write('\n\t\t\t\t\ttree="@Tree"')
		f.write('\n\t\t\t\t\treproductiveNumber="@reproductiveNumber"')
		f.write('\n\t\t\t\t\tbecomeUninfectiousRate="@becomeUninfectiousRate"')
		f.write('\n\t\t\t\t\tsamplingProportion="@samplingProportion"')
		f.write('\n\t\t\t\t\torigin="@origin">')
		f.write(tmp)
		f.write('\n\t\t\t\t</distribution>')
	f.close()


def print_bdsky_parameters_prior(dico, output_xml):
	parameters = {"origin" : "origin", "sampling_proportion":"samplingProportion", "become_uninfectious_rate":"becomeUninfectiousRate", "R0":"reproductiveNumber"}
	with open(output_xml, 'a') as f:
		for bdsky_name, dico_name in zip(parameters.values(),parameters.keys()):
			if "distribution" in dico[dico_name].keys() and "fixed" not in dico[dico_name].keys():
				if bdsky_name == "samplingProportion":
					tmp = '\n\t\t\t\t<prior id="'+bdsky_name+'Prior'+'" name="distribution" x="@samplingProportionSlice">'
				else:
					tmp = '\n\t\t\t\t<prior id="'+bdsky_name+'Prior'+'" name="distribution" x="@'+bdsky_name+'">'
				f.write(tmp)
				distribution = dico[dico_name]["distribution"]
				distribution = distribution.replace(" ","")
				shape = distribution.split('(')[0]
				shape = shape.lower()
				parameters = distribution.split('(')[1].replace(")","").split(",")
				if shape in ['lognorm', 'lognormal']:
					for param in parameters:
						if substract_alphabetic(param).lower() in ["m", "mean"]:
							mean = param.split("=")[1]
						elif substract_alphabetic(param).lower() in ["s", "sd", "stdev", "stdv"]:
							stdev = param.split("=")[1]
					if "mean_in_real_space" in dico[dico_name].keys():
						tmp = '\n\t\t\t\t\t<LogNormal meanInRealSpace="true" name="distr" M="'+mean+'" S="'+stdev+'" offset="0.0"/>'
					else:
						tmp = '\n\t\t\t\t\t<LogNormal name="distr" M="'+str(mean)+'" S="'+str(stdev)+'" offset="0.0"/>'
					f.write(tmp)
				elif shape in ['uniform', "unif"]:
					for param in parameters:
						if substract_alphabetic(param).lower() in ["upper", "up", "max"]:
							upper = param.split("=")[1]
						elif substract_alphabetic(param).lower() in ["lower", "low", "min"]:
							lower = param.split("=")[1]
					tmp = '\n\t\t\t\t\t'+'<Uniform id="Uniform.2" name="distr" lower="'+str(lower)+'" upper="'+str(upper)+'"/>'
					f.write(tmp)
				elif shape in ['beta']:
					for param in parameters:
						if substract_alphabetic(param).lower() in ["beta", "b"]:
							beta = param.split("=")[1]
						elif substract_alphabetic(param).lower() in ["alpha", "a"]:
							alpha = param.split("=")[1]
					tmp = '\n\t\t\t\t\t<Beta name="distr" alpha="'+str(alpha)+'" beta="'+str(beta)+'"/>'
					f.write(tmp)
				elif shape in ['gamma']:
					for param in parameters:
						if substract_alphabetic(param).lower() in ["beta", "b"]:
							beta = param.split("=")[1]
						elif substract_alphabetic(param).lower() in ["alpha", "a"]:
							alpha = param.split("=")[1]
					tmp = '\n\t\t\t\t\t<Gamma name="distr" alpha="'+str(alpha)+'" beta="'+str(beta)+'"/>'
					f.write(tmp)
			if "distribution" in dico[dico_name].keys() and "fixed" not in dico[dico_name].keys():
				f.write('\n\t\t\t\t</prior>')
	f.close()

def clock_site_model_priors(output_xml, estimate_clock=True):
	with open(output_xml, 'a') as f:
		f.write('\n\t\t<!-- Site model priors -->')
		f.write('\n\t\t<prior id="RateACPrior.s" name="distribution" x="@rateAC.s">')
		f.write('\n\t\t\t<Gamma name="distr" alpha="0.05" beta="10.0"/>')
		f.write('\n\t\t</prior>')
		f.write('\n\t\t<prior id="RateAGPrior.s" name="distribution" x="@rateAG.s">')
		f.write('\n\t\t\t<Gamma name="distr" alpha="0.05" beta="20.0"/>')
		f.write('\n\t\t</prior>')
		f.write('\n\t\t<prior id="RateATPrior.s" name="distribution" x="@rateAT.s">')
		f.write('\n\t\t\t<Gamma name="distr" alpha="0.05" beta="10.0"/>')
		f.write('\n\t\t</prior>')
		f.write('\n\t\t<prior id="RateCGPrior.s" name="distribution" x="@rateCG.s">')
		f.write('\n\t\t\t<Gamma name="distr" alpha="0.05" beta="10.0"/>')
		f.write('\n\t\t</prior>')
		f.write('\n\t\t<prior id="RateGTPrior.s" name="distribution" x="@rateGT.s">')
		f.write('\n\t\t\t<Gamma name="distr" alpha="0.05" beta="10.0"/>')
		f.write('\n\t\t</prior>')
		if estimate_clock:
			f.write('\n\t\t\t<prior id="ClockPrior" name="distribution" x="@clockRate">')
			f.write('\n\t\t\t\t<Uniform id="Uniform.0" name="distr" upper="Infinity"/>')
			f.write('\n\t\t\t</prior>')
	f.close()

def print_alignment_likelihood(output_xml):
	with open(output_xml, 'a') as f:
		f.write('\n\t\t<distribution id="treeLikelihood.Covid" spec="ThreadedTreeLikelihood" data="@Covid" tree="@Tree">')
		f.write('\n\t\t<siteModel id="SiteModel" spec="SiteModel">')
		f.write('<\n\t\tparameter id="mutationRate" estimate="false" name="mutationRate">1.0</parameter>')
		f.write('\n\t\t<parameter id="gammaShape" estimate="false" name="shape">1.0</parameter>')
		f.write('\n\t\t<parameter id="proportionInvariant" estimate="false" lower="0.0" name="proportionInvariant" upper="1.0">0.0</parameter>')
		f.write('\n\t\t<substModel id="gtr.s" spec="GTR" rateAC="@rateAC.s" rateAG="@rateAG.s" rateAT="@rateAT.s" rateCG="@rateCG.s" rateGT="@rateGT.s">')
		f.write('\n\t\t<parameter id="rateCT.s" spec="parameter.RealParameter" estimate="false" lower="0.0" name="rateCT">1.0</parameter>')
		f.write('\n\t\t<frequencies id="estimatedFreqs.s" spec="Frequencies" frequencies="@freqParameter.s"/>')
		f.write('\n\t\t</substModel>')
		f.write('\n\t\t</siteModel>')
		f.write('\n\t\t<branchRateModel id="StrictClock" spec="beast.evolution.branchratemodel.StrictClockModel" clock.rate="@clockRate.c"/>')
		f.write('\n\t\t</distribution>')
	f.close()

def get_fixed_parameter_name(dico):
	fixed_param = []
	for key, value in dico.items():
		if "fixed" in dico[key].keys():
			fixed_param.append(key)
	return fixed_param

def print_operators(dico, output_xml, estimate_clock=True, is_tree_fixed=False):
	with open(output_xml, 'a') as f:
		if not is_tree_fixed:
			f.write('\n<!-- Site model operators -->')
			f.write('\n<operator id="RateACScaler.s" spec="ScaleOperator" parameter="@rateAC.s" scaleFactor="0.5" weight="0.1"/>')
			f.write('\n<operator id="RateAGScaler.s" spec="ScaleOperator" parameter="@rateAG.s" scaleFactor="0.5" weight="0.1"/>')
			f.write('\n<operator id="RateATScaler.s" spec="ScaleOperator" parameter="@rateAT.s" scaleFactor="0.5" weight="0.1"/>')
			f.write('\n<operator id="RateCGScaler.s" spec="ScaleOperator" parameter="@rateCG.s" scaleFactor="0.5" weight="0.1"/>')
			f.write('\n<operator id="RateGTScaler.s" spec="ScaleOperator" parameter="@rateGT.s" scaleFactor="0.5" weight="0.1"/>')
			f.write('\n<operator id="FrequenciesExchanger.s" spec="DeltaExchangeOperator" delta="0.01" weight="0.1">')
			f.write('\n\t<parameter idref="freqParameter.s"/>')
			f.write('\n</operator>')
			f.write('\n<!-- Tree operators >')
			f.write('\n<operator id="BDSKY_serialtreeScaler" spec="ScaleOperator" scaleFactor="0.5" tree="@Tree" weight="3.0"/>')
			f.write('\n<operator id="BDSKY_serialtreeRootScaler" spec="ScaleOperator" rootOnly="true" scaleFactor="0.5" tree="@Tree" weight="3.0"/>')
			f.write('\n<operator id="BDSKY_serialUniformOperator" spec="Uniform" tree="@Tree" weight="30.0"/>')
			f.write('\n<operator id="BDSKY_serialSubtreeSlide" spec="SubtreeSlide" tree="@Tree" weight="15.0"/>')
			f.write('\n<operator id="BDSKY_serialnarrow" spec="Exchange" tree="@Tree" weight="15.0"/>')
			f.write('\n<operator id="BDSKY_serialwide" spec="Exchange" isNarrow="false" tree="@Tree" weight="3.0"/>')
			f.write('\n<operator id="BDSKY_serialWilsonBalding" spec="WilsonBalding" tree="@Tree" weight="3.0"/-->')
			if estimate_clock:
				f.write('\n<!-- Clock model operators >')
				f.write('\n<operator id="StrictClockRateScaler" spec="ScaleOperator" parameter="@clockRate" scaleFactor="0.75" weight="3.0"/>')
				f.write('\n<operator id="strictClockUpDownOperator" spec="UpDownOperator" scaleFactor="0.75" weight="3.0">')
				f.write('\n\t<up idref="clockRate"/>')
				f.write('\n\t<down idref="Tree"/>')
				f.write('\n</operator-->')
		f.write('\n\t<!-- BDSKY operators -->')
		fixed_params = get_fixed_parameter_name(dico)
		if "sampling_proportion" not in fixed_params:
			f.write('\n\t<operator id="samplingScaler" spec="ScaleOperator" parameter="@samplingProportion" scaleFactor="0.75" weight="2.0"/>')
		if "origin" not in fixed_params:
			f.write('\n\t<operator id="origScaler" spec="ScaleOperator" parameter="@origin" scaleFactor="0.75" weight="1.0"/>')
		if "R0" not in fixed_params:
			f.write('\n\t<operator id="reproductiveNumberScaler" spec="ScaleOperator" parameter="@reproductiveNumber" scaleFactor="0.75" weight="10.0"/>')
		if "become_uninfectious_rate" not in fixed_params:
			f.write('\n\t<operator id="becomeUninfectiousRateScaler" spec="ScaleOperator" parameter="@becomeUninfectiousRate" scaleFactor="0.75" weight="2.0"/>')
		ok = "R0" not in fixed_params and "become_uninfectious_rate" not in fixed_params
		if ok:
			f.write('\n\t<operator id="updownBD" spec="UpDownOperator" scaleFactor="0.75" weight="2.0">')
			f.write('\n\t\t<up idref="reproductiveNumber"/>')
			f.write('\n\t\t<down idref="becomeUninfectiousRate"/>')
			f.write('\n\t</operator>')
	f.close()


def print_loggers(dico, output_xml, estimate_clock=True, is_tree_fixed=False, output_log = "", tree_log = "", trace_log_every = 10000, screen_log_every = 10000, tree_log_every = 10000):
	fixed_params = get_fixed_parameter_name(dico)
	with open(output_xml, 'a') as f:
		f.write('\n\t<logger id="tracelog" fileName="'+output_log+'" logEvery="'+str(trace_log_every)+'" model="@posterior" sanitiseHeaders="true" sort="smart">')
		f.write('\n\t\t<log idref="posterior"/>')
		f.write('\n\t\t<log idref="likelihood"/>')
		f.write('\n\t\t<log idref="prior"/>')
		f.write('\n\t\t<log idref="BirthDeathSkySerial"/>')
		f.write('\n\t\t<log id="TreeHeight" spec="beast.evolution.tree.TreeStatLogger" tree="@Tree" logHeight="true"/>')
		if "origin" not in fixed_params:
			f.write('\n\t\t<log idref="origin"/>')
		if "become_uninfectious_rate" not in fixed_params:
			f.write('\n\t\t<log idref="becomeUninfectiousRate"/>')
		if "R0" not in fixed_params:
			f.write('\n\t\t<log idref="reproductiveNumber"/>')
		if "sampling_proportion" not in fixed_params:
			f.write('\n\t\t<log idref="samplingProportion"/>')
		if not is_tree_fixed:
			f.write('\n\t\t<!--log idref="treeLikelihood.Covid"/-->')
			f.write('\n\t\t<log idref="rateAC.s"/>')
			f.write('\n\t\t<log idref="rateAG.s"/>')
			f.write('\n\t\t<log idref="rateAT.s"/>')
			f.write('\n\t\t<log idref="rateCG.s"/>')
			f.write('\n\t\t<log idref="rateGT.s"/>')
			f.write('\n\t\t<log idref="freqParameter.s"/>')
			if estimate_clock:
				f.write('\n\t<!--log idref="clockRate"/-->')
		f.write('\n\t</logger>')
		f.write('\n\t<logger id="screenlog" logEvery="'+str(screen_log_every)+'">')
		f.write('\n\t\t<log idref="posterior"/>')
		f.write('\n\t\t<log id="ESS.0" spec="util.ESS" arg="@posterior"/>')
		f.write('\n\t\t<log idref="likelihood"/>')
		f.write('\n\t\t<log idref="prior"/>')
		f.write('\n\t</logger>')
		if not is_tree_fixed:
			f.write('\n\t\t<logger id="treelog" fileName="'+str(tree_log)+'" logEvery="'+str(tree_log_every)+'" mode="tree">')
			f.write('\n\t\t\t<log id="TreeWithMetaDataLogger" spec="beast.evolution.tree.TreeWithMetaDataLogger" tree="@Tree"/>')
			f.write('\n\t\t</logger>')
	f.close()

def print_XML_tree_fixed(dico):
	output_prefix = dico["output"]["prefix"]
	output_xml = output_prefix + '.xml'
	output_log = output_prefix + ".log"
	tree_log = output_prefix + ".tree"
	input_tree = dico['input_files']["tree"]
	input_dates = dico['input_files']["dates"]
	chain_length=dico['mcmc']["chain_length"]
	store_every=dico['mcmc']['store_every']
	tmp = "Writing XML file : " + output_xml
	print(tmp)
	print_header(output_xml)
	print_fixed_tree(input_Newick_Tree = input_tree, output_xml = output_xml)
	with open(output_xml, 'a') as f:
		f.write('\n<run id="mcmc" spec="MCMC" chainLength="'+str(chain_length)+'">')
		f.write('\n\t<state id="state" storeEvery="'+str(store_every)+'">')
	f.close()
	print_bdsky_parameters_state(dico, output_xml)
	with open(output_xml, 'a') as f:
		f.write('\n\t</state>')
	f.close()
	with open(output_xml, 'a') as f:
		f.write('\n\t<distribution id="posterior" spec="util.CompoundDistribution">')
		f.write('\n\t\t<distribution id="prior" spec="util.CompoundDistribution">')
	f.close()
	print_bdsky_parameters_prior(dico, output_xml)
	with open(output_xml, 'a') as f:
		f.write('\n\t\t</distribution>')
		f.write('\n\t\t<distribution id="likelihood" spec="util.CompoundDistribution" useThreads="true">')
	f.close()
	print_bdsky_treeprior_distribution(output_xml = output_xml, dates_file = input_dates, birth_rate_change_dates=dico["R0"]["changing_dates"])
	with open(output_xml, 'a') as f:
		f.write('\n\t\t</distribution>')
		f.write('\n\t</distribution>')
	f.close()
	print_operators(dico = dico, output_xml = output_xml, estimate_clock=False, is_tree_fixed=True)
	print_loggers(dico = dico, output_xml = output_xml, estimate_clock=False, is_tree_fixed=True, output_log = output_log, tree_log=None, trace_log_every = store_every, screen_log_every = store_every, tree_log_every = store_every)
	with open(output_xml, 'a') as f:
		f.write('\n</run>')
		f.write('\n</beast>')
		f.close()
	print("Done.")


def config_to_dico(config_file):
	print("Parsing config file...")
	parser = ConfigParser()
	parser.read(config_file)
	dico = {'R0': {'upper': 10, 'dimension': 2, 'distribution': 'LogNormal(M=0,S=1)', 'start': 1.0, 'lower': 0.0, 'changing_dates': None},'origin': {'start': 10, 'upper': 'Infinity', 'lower': 0},
			'become_uninfectious_rate': {'start': 2.0, 'upper': 'Infinity', 'lower': 1.0, 'distribution': 'LogNormal(M=0,S=1)'},
			'sampling_proportion': {'start': 0.01, 'upper': 1.0, 'lower': 0.0, 'distribution': 'Beta(alpha=1,beta=1)', 'dimension' : 1},
			'mcmc': {'store_every': 100000, 'log_every': 100000, 'chain_length': 10000000},
			'origin': {'upper': 500.0, 'lower': 0.1, 'start': 0.5, 'distribution': "Uniform(upper=500.0,lower=0.1)"}}
	dico["output"] = {}
	dico["input_files"] = {}
	dico["input_files"]['tree'] = None
	dico["input_files"]['alignment'] = None
	## TO DO ADD CLOCK RATE
	sections = parser.sections()
	for section in sections:
		subsections = list(dict(parser.items(section)).keys())
		for subsection in subsections:
			dico[section][subsection]=dict(parser.items(section))[subsection]
	return dico


def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        # return open(arg, 'r')  # return an open file handle
        return arg

def show_config_example():
	tmp='[output]\n\
prefix=delta_covid\n\n\
[R0]\n\
distribution=LogNorm(M=1.0,S=1.2)\n\
start=1.5\n\
upper=5\n\
lower=0.5\n\
dimension=5\n\
changing_dates="2021-01-01,2021-02-01,2021-03-01,2021-05-01"\n\n\
[sampling_proportion]\n\
upper=0.25\n\
lower=0.0\n\n\
[become_uninfectious_rate]\n\
upper=100\n\
lower=20\n\
distribution=Uniform(upper=100,lower=20)\n\
start=50\n\n\
[origin]\n\
upper=1.5\n\
lower=0.5\n\
start=0.2\n\n\
[input_files]\n\
tree="tree.newick"\n\
alignment="sequences.fasta"\n\
dates="dates.txt"\n\n\
[mcmc]\n\
chain_length=100000\n\
store_every=10000\n\
log_every=1000\n'
	return tmp


def main(dico):
	if dico['input_files']['tree']:
		dico = check_dico(dico)
		print_XML_tree_fixed(dico)
	else:
		pass

if __name__ == "__main__":

	parser=argparse.ArgumentParser(description='Generates an XML file to run Beast2 BDSKY analysis.', formatter_class=RawTextHelpFormatter)
	parser.add_argument("--config", dest="config", required=True, help="input config file with Beast2 prior parameters", metavar="FILE")
	parser.add_argument('--example', help=show_config_example(), metavar=None)
	args=parser.parse_args()

	if args.config:
		args_dico = config_to_dico(config_file = args.config)
		main(args_dico)
