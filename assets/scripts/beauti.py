import os
import subprocess

def get_bdsky_xml_config_file(config, temp_file, treefile, datesfile, treeheight, dimensionfile):
	f = open(treeheight, "r")
	height_value = f.read().splitlines()
	f.close
	height_value = height_value[0]
	f = open(dimensionfile, 'r')
	dimension_value = f.read().splitlines()
	f.close
	dimension_value = str(dimension_value[0])

	xml_keys = ['R0', 'sampling_proportion', 'become_uninfectious_rate', 'mcmc', 'origin']
	tmp=''
	tmp = tmp + '\n' + '[input_files]' + '\n'
	tmp = tmp + 'tree=' + treefile + "\n"
	tmp = tmp + 'dates=' + datesfile + "\n"

	for key in config.keys():
		if key in xml_keys:
			tmp = tmp + '\n' + '[' + key + ']' + '\n'
			for subkey,value in zip(config[key].keys(),config[key].values()):
				tmp = tmp + subkey + '=' + str(value) + "\n"
			if key == "origin":
				tmp = tmp + "height" + '=' + height_value + "\n"
			if key == "R0" : 
				tmp = tmp + "dimension=" + dimension_value + "\n"
		elif key in ["output"]:
			tmp = tmp + '\n' + '[' + key + ']' + '\n'
			tmp = tmp + 'prefix=' + config[key] + "_bdsky" + "\n"
	with open(temp_file, 'w') as f:
		count = f.write(tmp)
	f.close()

def get_coalsky_xml_config_file(config, temp_file, treefile, dimensionfile):
	
	f = open(dimensionfile, 'r')
	dimension_value = f.read().splitlines()
	f.close
	dimension_value = str(dimension_value[0])

	xml_keys = ['mcmc', 'Ne']
	tmp=''
	tmp = tmp + '\n' + '[input_files]' + '\n'
	tmp = tmp + 'tree=' + treefile + "\n"

	for key in config.keys():
		if key in xml_keys:
			tmp = tmp + '\n' + '[' + key + ']' + '\n'
			for subkey,value in zip(config[key].keys(),config[key].values()):
				tmp = tmp + subkey + '=' + str(value) + "\n"
				
			if key == "Ne" : 
				tmp = tmp + "dimension=" + dimension_value + "\n"

		elif key in ["output"]:
			tmp = tmp + '\n' + '[' + key + ']' + '\n'
			tmp = tmp + 'prefix=' + config[key] + "_coalsky" + "\n"
	with open(temp_file, 'w') as f:
		count = f.write(tmp)
	f.close()


def beauti():
	
    get_bdsky_xml_config_file(config = snakemake.config, temp_file = snakemake.output.bdsky_config, treefile = snakemake.input.tree, datesfile = snakemake.input.dates, treeheight = snakemake.input.maxheight, dimensionfile = snakemake.input.dimensionfile)
    get_coalsky_xml_config_file(config = snakemake.config, temp_file = snakemake.output.coalsky_config, treefile = snakemake.input.tree, dimensionfile = snakemake.input.dimensionfile)
    cmd = 'python3 scripts/getBeautiXML.py --config {conf};'.format(conf=snakemake.output.bdsky_config)
    count = subprocess.call(cmd, shell=True)
    cmd = 'python3 scripts/getCoalSkyline.py --config {conf};'.format(conf=snakemake.output.coalsky_config)
    count = subprocess.call(cmd, shell=True)
    cmd = "mv *.xml results/.;"
    count = subprocess.call(cmd, shell=True)


if __name__ == "__main__":
    beauti()
