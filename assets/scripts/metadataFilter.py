import json
import pandas
import datetime
import csv
import numpy as np


def validate_date(date_text):
    try:
        datetime.datetime.strptime(str(date_text), '%Y-%m-%d')
        res = True
    except ValueError:
        #raise ValueError("Incorrect data format, should be YYYY-MM-DD")
        res = False
    return res

def readJSON(fJson):
    print("Reading JSON file ", fJson,"...")
    with open(fJson) as json_file:
        json_data = json.load(json_file)
    return json_data

def readCSV(csv_file, sep="\t"):
	'''Returns metadata read from input csv or tsv file'''
	df = pandas.read_csv(csv_file, sep=sep)
	return df

def modify_df_columns(df, metadata_columns):
    '''
    Modify name of columns of data frame.
    df : dataframe
    metadata_columns : 'default_column=your_column'; ex :'date=sampling_date,pango_lineage=lineage,strain=id'
    df.rename(columns={"old_column_name" : "new_column_name"})
    '''
    liste=metadata_columns.split(",")
    to_change={sub[1]:sub[0] for sub in [l.split("=") for l in liste]}
    df=df.rename(columns=to_change)
    return df

def write_CSV(metadata, output, sep="\t"):
    '''Prints out metadata into csv or tsv output file'''
    f = open(output, 'w')
    metadata.to_csv(output, sep=sep, index=False, header=True)
    f.close()

def replace_id_pattern(metadata, pattern = "_", replace = "-"):
    df_updated = metadata['strain'].replace(to_replace = pattern, value=replace, regex = True)
    metadata['strain'] = df_updated
    return metadata

def filter_by_date_format(metadata):
	""" Filter given date format : dont keep rows with date not in format yyyy-mm-dd"""
	metadata = metadata[metadata['date'].apply(validate_date)]
	print("Dropping non valid date (format must be yyyy-mm-dd)...")
	print(len(metadata), " rows left.")
	return metadata

def filter_by_host(metadata, host = "Human"):
	metadata = metadata[metadata['host'].isin([host])]
	print("Keeping only data from ", host, " host type.")
	print(len(metadata), " rows left")
	return metadata

def filter_by_region(metadata, region):
    """
	Filter data given a required region or multiple region.
	Input region (STRING) ; examples : "Europe" or "Europe,Asia"
	Returns the filtered metatada.
	"""
    print("Filtering data from ",region,"...")
    region = [item.strip() for item in region.split(",")]
    metadata = metadata[metadata['region'].isin(region)]
    print(len(metadata), " rows left.")
    return metadata

def filter_by_country(metadata, country):
    """
	Filter data given a required country or multiple countries.
	Input country (STRING) ; examples : "France" or "Germany,France,United Kngdom"
	Countries must be comma separated. No worries if spaces are added before/after the comma.
	Returns the filtered metatada.
	"""
    print("Filtering data from ",country,"...")
    country = [item.strip() for item in country.split(",")]
    metadata = metadata[metadata['country'].isin(country)]
    print(len(metadata), " rows left.")
    return metadata

def filter_by_division(metadata, division):
    """ Filter data given a required division or multiple divisions (comma separated). """
    print("Filtering data from ",division,"...")
    division = [item.strip() for item in division.split(",")]
    metadata = metadata[metadata['division'].isin(division)]
    print(len(metadata), " rows left.")
    return metadata

def filter_by_dates(metadata, min_date = None, max_date = None):
    """ Filter data given a maximum or minimum date.
    Dates must be given in a yyyy-mm-dd format. """
    txt = "Filtering"
    if min_date:
        txt = " ".join([txt," ".join(["from",min_date])])
        metadata = metadata[metadata['date'] >= min_date]
    if max_date:
        txt = " ".join([txt," ".join(["to",max_date])])
        metadata = metadata[metadata['date'] <= max_date]
    print(txt)
    print(len(metadata), " rows left.")
    return metadata

def filter_by_length(metadata, min_length = 27000):
    """ Filter data given the sequence length. By default, minimum length is 27000. """
    metadata = metadata[metadata['length'] >= int(min_length)]
    print("Dropping data with sequence length smaller than 27000 pb.")
    print(len(metadata)," rows left.")
    return metadata

def filter_by_lineage(metadata, lineage):
    """
    Filter data given a pangolin lineage.
    Checks if given lineage is in the metadata. If not, the filter is not taken into account
    Input examples : B.1.1.7 or B.1.1.7,P.1.
    """
    ## First : check if column lineage exists
    if not any(item in list(metadata.columns) for item in ["pango_lineage","pangolin_lineage"]):
        print("WARNING : There is no column about the lineage in the metadata given.")
    else:
        lineage_column = [item for item in list(metadata.columns) if "lineage" in item][0]
        ## Then : check if lineage is in metedata
        lineages = list(metadata[lineage_column].unique())
        lineage = [item.strip() for item in lineage.split(",")]
        bools = [item in lineages for item in lineage]
        wrong_lineage = [i for (i, v) in zip(lineage, bools) if not v]
        lineage = [i for (i, v) in zip(lineage, bools) if v]
        if len(wrong_lineage):
            print("WARNING : There is no data for lineage(s)"," and ".join(wrong_lineage), "in the metadata. Not taking into account that filter.")
        elif len(lineage):
            print("Filtering data from"," and ".join(lineage),"...")
            metadata = metadata[metadata[lineage_column].isin(lineage)]
            print(len(metadata), " rows left.")
    return metadata

def remove_by_ID(metadata, id):
    """ Remove metadata given list of IDs. """
    if len(id):
        metadata = metadata[-metadata['strain'].isin(id)]
    return metadata

def keep_by_ID(metadata, id):
    """ Keep only metadata given list of IDs. """
    if len(id):
        metadata = metadata[metadata['strain'].isin(id)]
    return metadata

def filter_by_json(json_data, metadata):
    """
    This function filters the metadata given the instructions given in the json file.
    Types of instructions and actions :
			- replace a value from a column with another value
			- remove rows with a certain value for a given column
			- keep only rows with a given value for a given column
	Returns the filtered metedata as a df pandas object.
	"""
    print("\n Start filtering with JSON filter... \n")
    object_keys = [key for key, value in json_data.items()]
    ### Start with replace action
    replace_objects = [value for key, value in json_data.items() if 'Replace' in key]
    key_replace_object = [key for key, value in json_data.items() if 'Replace' in key]
    i = 0 #to keep track of keys and help debug for user
    for replace in replace_objects: #for each replace object
        print("Action ",  key_replace_object[i] , " : ")
        if 'Replace_with' in list(replace.keys()) and 'To_replace' in list(replace.keys()):
            bool_vector = [True for i in range(len(metadata.iloc[:, 0]))] #we first define the vector which will indicate on which row we will work
            for condition in replace["To_replace"]:  #for each condition in the To_replace
                if "column" in list(condition.keys()) :
                    if condition['column'] in list(metadata.columns):
                        if "value" in list(condition.keys()) :
                            new_bool = list(metadata[condition['column']].isin([condition['value']]).fillna(False))
                            bool_vector = [bool_vector[i] and new_bool[i] for i in range(len(metadata.iloc[:, 0]))]
                        elif "contains" in list(condition.keys()) :
                            new_bool = list(metadata[condition['column']].str.contains(condition['contains']).fillna(False))
                            bool_vector = [bool_vector[i] and new_bool[i] for i in range(len(metadata.iloc[:, 0]))]
                        else :
                            #error not value or contains (illegale key word ... for action ... legal key words are "value" and "contains")
                            print("ERROR : one of the key words is not valid (invalid key work for action " ,  key_replace_object[i]  , " in To_replace part,  valid key words are \"value\" and \"contains\" ) \n")
                    else :
                        print("ERROR : ",condition['column']," is not a valid column name (invalid column name for action " ,  key_replace_object[i]  , " in To_replace part, check colnames in your tsv file for valid column names) \n")
                else :
                    print("ERROR : one of the key words is an invalid (first key word should be \"column\" for action " ,  key_replace_object[i]  , " in To_replace part, check documentation and exemple if needed) \n")
            print(sum(bool_vector), "rows are targeted by replacement \n")
            #To_replace part
            for replacers in replace["Replace_with"]: #for each line in Replace with
                if "column" in list(replacers.keys()):
                    if replacers['column'] in list(metadata.columns):
                        if "value" in list(replacers.keys()):
                            metadata.loc[bool_vector, replacers['column']] = replacers['value'] #replacement action
                        else :
                            #error not value or contains (illegale key word ... for action ... legal key words are "value" and "contains")
                            print("ERROR : invalid key word for action " ,  key_replace_object[i]  , " in Replace_with part, valid key word is \"value\" \n")
                    else :
                        print("ERROR : " ,replacers['column'], " not a valid column name (invalid column name for action " ,  key_replace_object[i]  , " in Replace_with part, check colnames in your tsv file for valid column names) \n")
                else :
                    print("ERROR : invalid key word (first key word should be \"column\" for action " ,  key_replace_object[i]  , "  in Replace_with part, check documentation and exemple if needed. \n")
        else :
            print("ERROR : invalid key words in action" ,  key_replace_object[i]  , " the 2 sub_object should be named \"To_replace\" and \"Replace_with\" ) \n")
        object_keys.remove(key_replace_object[i])
        i = i+1
    ### Remove part
    remove_objects = [value for key, value in json_data.items() if 'Remove' in key]
    key_remove_object = [key for key, value in json_data.items() if 'Remove' in key]
    i = 0
    for remove in remove_objects: #for each Remove object
        print("Action ",  key_remove_object[i] , " : ")
        valid = True
        bool_vector = [True for i in range(len(metadata.iloc[:, 0]))] #we first define the vector which will indicate on which row we will work
        for condition in remove:  #for each condition line
            if "column" in list(condition.keys()):
                if condition['column'] in list(metadata.columns):
                    if "value" in list(condition.keys()):
                        new_bool = metadata[condition['column']].isin([condition['value']]).fillna(False).tolist()
                        bool_vector = [bool_vector[j] and new_bool[j] for j in range(len(metadata.iloc[:, 0]))]
                        print(sum(bool_vector), "rows selected")
                    elif "contains" in list(condition.keys()):
                        new_bool = metadata[condition['column']].str.contains(condition['contains']).fillna(False).tolist()
                        bool_vector = [bool_vector[j] and new_bool[j] for j in range(len(metadata.iloc[:, 0]))]
                        print(sum(bool_vector), "rows selected")
                    else :
                        #error not value or contains (illegale key word ... for action ... legal key words are "value" and "contains")
                        print("ERROR : invalid key work for action " ,  key_remove_object[i]  , " in To_replace part,  valid key words are \"value\" and \"contains\"  \n")
                        valid = False
                else :
                    print("ERROR : ",condition['column']," is not a valid column name (invalid column name for action " ,  key_remove_object[i]  , " in To_replace part, check colnames in your tsv file for valid column names) \n")
                    valid = False
            else :
                print("ERROR : invalid key word (first key word should be \"column\" for action " ,  key_remove_object[i]  , " in To_replace part, check documentation and exemple if needed) \n")
                valid = False
        if valid is True :
            metadata = metadata[~np.array(bool_vector)] #We remove selected rows
            print(sum(bool_vector), "rows removed \n")
        object_keys.remove(key_remove_object[i])
        i = i+1
    ## Keep_only part
    keep_objects = [value for key, value in json_data.items() if 'Keep_only' in key]
    key_keep_object = [key for key, value in json_data.items() if 'Keep_only' in key]
    i = 0
    for keep in keep_objects: #for each object
        print("Action ",  key_keep_object[i] , " : ")
        valid = True
        nb_rows = len(metadata.iloc[:, 0])
        bool_vector = [True for i in range(nb_rows)] #we first define the vector which will indicate on which row we will work
        for condition in keep:  #for each condition line
            if "column" in list(condition.keys()):
                if condition['column'] in list(metadata.columns):
                    if "value" in list(condition.keys()):
                        new_bool = metadata[condition['column']].isin([condition['value']]).fillna(False).tolist()
                        bool_vector = [bool_vector[i] and new_bool[i] for i in range(nb_rows)]
                        print(sum(bool_vector), "rows selected")
                    elif "contains" in list(condition.keys()):
                        new_bool = metadata[condition['column']].str.contains(condition['contains']).fillna(False).tolist()
                        bool_vector = [bool_vector[i] and new_bool[i] for i in range(nb_rows)]
                        print(sum(bool_vector), "rows selected")
                    else :
						# error not value or contains (illegale key word ... for action ... legal key words are "value" and "contains")
                        print("ERROR : invalid key word for action " ,  key_keep_object[i]  , " in To_replace part,  valid key words are \"value\" and \"contains\"  \n")
                        valid = False
                else :
                    print("ERROR : ",condition['column']," is not a valid column name (invalid column name for action " ,  key_keep_object[i]  , " in To_replace part, check colnames in your tsv file for valid column names) \n")
                    valid = False
            else :
                print("ERROR : invalid key word (first key word should be \"column\" for action " ,  key_keep_object[i]  , " in To_replace part, check documentation and exemple if needed) \n")
                valid = False
        if valid is True :
            metadata = metadata[np.array(bool_vector)] #We keep selected rows
            print(sum(bool_vector), "rows kept \n")
        object_keys.remove(key_keep_object[i])
        i = i+1
    if len(object_keys) > 0 :
        print("WARNING : Be carful, some actions have been ignored because their synthaxe is wrong, action names must contain one of these three words \"Remove\", \"Keep_only\", \"Replace\"")
        print("Action(s) which haven't been performed is/are : ",  object_keys, "\n")
    print("\n Filtering with JSON filter done. \n")
    return metadata

def getMonths(date_text):
    datee = datetime.datetime.strptime(date_text, "%Y-%m-%d")
    res = datee.month
    if datee.year > 2020 :
        diff = datee.year - 2020
        res = res + (12 * diff)
    return res

def subsample_nb_data(metadata, nb):
    """ Subsample from metadata nb IDs randomly.
    Input : Metadata with column strain for ID
    Ouput : List of randomly picked IDs
    """
    tmp = metadata
    if nb < 1:
        nb = round(nb * len(metadata))
    if nb < len(metadata):
        tmp = metadata.sample(int(nb))
    return tmp['strain'].tolist()

def subsample_metadata(metadata, arg, nb):
    """ Subsample data geographically.
    Input arguments :
        - arg should be such as per-month-division or per-month-region-country-division.
        - nb should be a number.
    Output :
        Subsampled metadata
    Example :
        with arg = per-month-division and nb = 10
        -> subsample randomly 10 data per month per division
    """
    subsample_ids = []
    key_words = arg.split("-")
    geographics = ["region", "country", "division"]
    if "month" in key_words:
        dates = metadata['date']
        months = dates.apply(getMonths)
        mois_num = sorted(np.unique(months))
        if any(x in geographics for x in key_words):
            for month in mois_num:
                data_month = metadata[months.isin([month])]
                sample_ids = geographic_subsample(metadata = data_month, arg = arg , nb = nb)
                subsample_ids.extend(sample_ids)
        else:
            for month in mois_num:
                data_month = metadata[months.isin([month])]
                sample_ids = subsample_nb_data(metadata = data_month, nb = nb)
                subsample_ids.extend(sample_ids)
    elif any(x in geographics for x in key_words):
        subsample_ids = geographic_subsample(metadata = metadata, arg = arg , nb = nb)
    subsample_metadata = metadata[metadata['strain'].isin(subsample_ids)]
    return subsample_metadata

def geographic_subsample(metadata, arg, nb):
    """ Subsample data geographically.
    Input arguments :
        - arg should be such as per-month-division or per-region-country-division.
        - nb : if > 1 is the number ; if < 1 is a proportion
    Output :
        list of IDs
    Example :
        with arg = per-month-division and nb = 10
        -> subsample randomly 10 data per division
        with arg = per-month-division and nb = 0.1
        -> subsample randomly 10% of the data per division
    """
    subsample_ids = []
    key_words = arg.split("-")
    if "region" in key_words:
        regions = metadata["region"].unique().tolist()
        for region in regions:
            metadata_region = metadata[metadata['region'].isin([region])]
            if "country" in key_words:
                countries = metadata_region["country"].unique().tolist()
                for country in countries:
                    metadata_country = metadata_region[metadata_region['country'].isin([country])]
                    if "division" in key_words:
                        divisions = metadata_country["division"].unique().tolist()
                        for division in divisions:
                            metadata_division = metadata_country[metadata_country['division'].isin([division])]
                            tmp_ids = subsample_nb_data(metadata = metadata_division, nb = nb)
                            subsample_ids.extend(tmp_ids)
                    else:
                        tmp_ids = subsample_nb_data(metadata = metadata_country, nb = nb)
                        subsample_ids.extend(tmp_ids)
            elif "division" in key_words:
                divisions = metadata_region["division"].unique().tolist()
                for division in divisions:
                    metadata_division = metadata_region[metadata_region['division'].isin([division])]
                    tmp_ids = subsample_nb_data(metadata = metadata_division, nb = nb)
                    subsample_ids.extend(tmp_ids)
            else:
                tmp_ids = subsample_nb_data(metadata = metadata_region, nb = nb)
                subsample_ids.extend(tmp_ids)
    elif "country" in key_words:
        countries = metadata["country"].unique().tolist()
        for country in countries:
            metadata_country = metadata[metadata['country'].isin([country])]
            if "division" in key_words:
                divisions = metadata_country["division"].unique().tolist()
                for division in divisions:
                    metadata_division = metadata_country[metadata_country['division'].isin([division])]
                    tmp_ids = subsample_nb_data(metadata = metadata_division, nb = nb)
                    subsample_ids.extend(tmp_ids)
            else:
                tmp_ids = subsample_nb_data(metadata = metadata_country, nb = nb)
                subsample_ids.extend(tmp_ids)
    elif "division" in key_words:
        divisions = metadata["division"].unique().tolist()
        for division in divisions:
            metadata_division = metadata[metadata['division'].isin([division])]
            tmp_ids = subsample_nb_data(metadata = metadata_division, nb = nb)
            subsample_ids.extend(tmp_ids)
    return subsample_ids

def replace_id_pattern(metadata, pattern = "_", replace = "-"):
    df_updated = metadata['strain'].replace(to_replace = pattern, value=replace, regex = True)
    metadata['strain'] = df_updated
    return metadata

def convert_metadata(metadata):
    ids = metadata["Virus name"].str.replace("hCoV-19/","")
    dates = metadata["Collection date"]
    locations = metadata["Location"].str.split("/", expand=True)
    regions = locations[0].str.strip()
    countries = locations[1].str.strip()
    divisions = locations[2].str.strip()
    hosts = metadata["Host"]
    sex = metadata["Gender"]
    age = metadata["Patient age"]
    lineages = metadata["Lineage"]
    clade = metadata["Clade"]
    new_data = {'strain': ids.tolist(),
                'date': dates.tolist(),
                'region': regions.tolist(),
                'country': countries.tolist(),
                'division': divisions.tolist(),
                'host': hosts.tolist(),
                'sex': sex.tolist(),
                'age': age.tolist(),
                'pango_lineage': lineages.tolist(),
                'GISAID_clade': clade.tolist()}
    new_df = pandas.DataFrame(new_data)
    return new_df
